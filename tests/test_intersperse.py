"""Unit tests for the intersperse moudle."""

from muppet.intersperse import intersperse


def test_intersperse():
    assert list(intersperse(1, [2, 3, 4])) == [2, 1, 3, 1, 4]


def test_intersperse_empty():
    assert list(intersperse(1, [])) == []
