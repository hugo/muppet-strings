from muppet.puppet.format.html import HTMLFormatter
from muppet.puppet.ast import PuppetLiteral

fmt = HTMLFormatter()


def test_literal():
    assert '<span class="literal">true</span>' == fmt._puppet_literal(PuppetLiteral('true'))
