from muppet.util import (
    group_by,
    concatenate,
    string_around,
    len_before
)


def test_group_by():
    groups = group_by(lambda x: x % 3, range(10))
    assert len(groups) == 3
    assert groups[0] == [0, 3, 6, 9]
    assert groups[1] == [1, 4, 7]
    assert groups[2] == [2, 5, 8]


def test_concatenate():
    assert concatenate([[1, 2], [3, 4]]) == [1, 2, 3, 4]
    assert concatenate([[1, [2]], [3, 4]]) == [1, [2], 3, 4]


def test_string_around():
    assert "5" == string_around("0123456789", 5, 0, 0)
    assert "456" == string_around("0123456789", 5, 1, 1)
    assert "3456" == string_around("0123456789", 5, 2, 1)
    assert "4567" == string_around("0123456789", 5, 1, 2)
    assert "0123456789" == string_around("0123456789", 5, 100, 100)


def test_len_before():
    assert 5 == len_before("0123456789", 5, 100)
