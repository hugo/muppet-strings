"""Unit tests for lookup."""

from muppet.lookup import lookup, Ref


def test_simple_lookup():
    assert lookup(str).get('split').value() == str.split
    assert lookup({'a': 'b'}).ref('a').value() == 'b'
    assert lookup("Hello").idx(1).value() == 'e'


def test_simple_failing_lookups():
    assert lookup(str).get('missing').value() is None
    assert lookup(str).get('missing').get('x').value() is None
    assert lookup(str).get('missing').ref('x').value() is None
    assert lookup(str).get('missing').idx(0).value() is None
    assert lookup(str).get('missing').find('Anything can go here').value() is None


def test_expressions():
    # Missing field
    assert not Ref('field').run({})
    # Present field
    assert Ref('field').run({'field': 'anything'})
    # Equality on missing field
    assert not (Ref('field') == 'anything').run({'not': 'else'})
    # Equality on present field with different value
    assert not (Ref('field') == 'anything').run({'field': 'else'})
    # Equality on present field with expected value
    assert (Ref('field') == 'anything').run({'field': 'anything'})


def test_find():
    assert lookup([{'something': 'else'}, {'key': 'value'}]) \
        .find(Ref('key')) \
        .value() == {'key': 'value'}

    assert lookup([{'something': 'else'}, {'key': 'value'}, {'key': '2'}]) \
        .find(Ref('key') == '2') \
        .ref('key') \
        .value() == '2'

    assert lookup([{'something': 'else'}, {'key': 'value'}]) \
        .find(Ref('key') == '2') \
        .ref('key') \
        .value() is None
