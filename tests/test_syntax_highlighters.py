from muppet.syntax_highlight import pygments, andre_simon, plain


def test_pygments():
    """
    <!-- Generated through pygments, as python -->
    <div class="highlight-pygments"><table class="highlight-pygmentstable"><tr><td class="linenos"><div class="linenodiv"><pre><span class="normal"><a href="#line-1">1</a></span></pre></div></td><td class="code"><div><pre><span></span><a id="line-1" name="line-1"></a><span class="n">f</span><span class="p">(</span><span class="n">x</span><span class="o">+</span><span class="mi">1</span><span class="p">)</span>
    </pre></div></td></tr></table></div>

    """ == pygments.highlight("f(x+1)", "python")


def test_andre_simon():
    assert """
    <!-- Generated through highlight(1), as language python -->
    <div class="highlight-andre-simon"><pre><span class="lin" id="line_1">    1 </span><span class="kwd">f</span><span class="opt">(</span>x<span class="opt">+</span><span class="num">1</span><span class="opt">)</span>
</pre></div>
    """ == andre_simon.highlight("f(x+1)", "python")


def test_plain():
    assert """
    <!-- "Genererated" as plain output -->
    <div class"highlight-plain"><pre><code class="python">f(x+1)</code></pre></div>
    """ == plain.highlight("f(x+1)", "python")
