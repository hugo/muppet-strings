"""
Tests for Parser combinator re-parsers.

Many of these have first a test_thingy, and then test_thingy_nested.

This is due to to trailing data being ignored. For example


.. code-block:: puppet

    Exec <| title=='apt_update' |> {
      refreshonly => false,
    }

Could be parsed as ``Exec <| title=='apt_update' |>`` with the
remaining string ``{ refreshonly => false, }``, while

.. code-block:: puppet

    if $_update['frequency'] == 'always' {
        Exec <| title=='apt_update' |> {
            refreshonly => false,
        }
    }

MUST parse the entire collect block, since there is otherwise a syntax
error with the if block.
"""

import pytest
from muppet.puppet.format.parser import ParserFormatter
from muppet.puppet.ast import (
    build_ast,
    HashEntry,
    PuppetAST,
    PuppetAccess,
    PuppetBinaryOperator,
    PuppetCallMethod,
    PuppetConcat,
    PuppetHash,
    PuppetInvoke,
    PuppetLiteralHeredoc,
    PuppetNumber,
    PuppetQn,
    PuppetString,
    PuppetVar,
)
from muppet.puppet.parser import puppet_parser
from muppet.parser_combinator import ParserCombinator, MatchCompound
from pprint import pprint
from typing import Any, Optional


def ws(x):
    return MatchCompound(type='ws', matched=x)


def parse_string(s: str, *,
                 ast: Optional[PuppetAST] = None,
                 matched: Optional[list[Any]] = None):
    """
    Parse and validate the given puppet fragment.

    At it's core this procedure simply:
    - Parses the puppet fragment into a well-behaved JSON structure
      (note that this step is actually two steps, see documentation)
    - Builds a Python AST from it
    - Prints that AST to stdout
    - Builds a parser combinator from that AST
    - Prints that parser combinator to stdout
      (TODO add rainbow parenthesis if module is available)
    - Attempt to parse the original string with the generated parser
      combinator
    - Prints that result.

    After the generation and printing of different data, there's an
    option to compare it to a known correct value. This comparison is
    done inside an `assert`, meaning that it works well with the
    running test.

    No comparison option exist for the parser object, since it may
    contain functions, and the only function comparison is if it's the
    exact same object, making it useless here.

    :param s:
        A self contained puppet fragment.
    :param ast:
        AST to compare the output of `build_ast` to.
    :param matched:
        List of match objects (or whatever the parser combinator
        returned) to compare agains.
    """
    generated_ast = build_ast(puppet_parser(s))
    pprint(generated_ast)
    if ast:
        assert generated_ast == ast

    parser = ParserFormatter().serialize(generated_ast)

    print()
    pprint(parser)
    match_objects = ParserCombinator(s, "s").get(parser)

    print()
    pprint(match_objects)
    if matched:
        assert matched == match_objects


def test_parse_else_if():
    parse_string("""
    if x {
      1
    } else {
      if y {
        2
      } else {
        3
      }
    }
    """)
    # [`ws(['\n    '])`, `keyword(['if'])`, `ws([' '])`,
    # `qn([`ws([])`, 'x'])`, `ws([' '])`, '{', `ws(['\n      '])`,
    # `ws([])`, `ws([])`, '1', `ws(['\n    '])`, '}', `ws([' '])`,
    # `keyword(['else'])`, `ws([' '])`, '{', `ws(['\n      '])`,
    # `ws([])`, `keyword(['if'])`, `ws([' '])`, `qn([`ws([])`, 'y'])`,
    # `ws([' '])`, '{', `ws(['\n        '])`, `ws([])`, `ws([])`, '2',
    # `ws(['\n      '])`, '}', `ws([' '])`, `keyword(['else'])`,
    # `ws([' '])`, '{', `ws(['\n        '])`, `ws([])`, `ws([])`, '3',
    # `ws(['\n      '])`, '}', `ws(['\n    '])`, '}']


def test_parse_elsif():
    parse_string("""
    if x {
      1
    } elsif y {
      2
    } else {
      3
    }
    """)
    # [`ws(['\n    '])`, `keyword(['if'])`, `ws([' '])`,
    # `qn([`ws([])`, 'x'])`, `ws([' '])`, '{', `ws(['\n      '])`,
    # `ws([])`, `ws([])`, '1', `ws(['\n    '])`, '}', `ws([' '])`,
    # `keyword(['elsif'])`, `ws([' '])`, `qn([`ws([])`, 'y'])`, `ws(['
    # '])`, '{', `ws(['\n      '])`, `ws([])`, `ws([])`, '2', `ws(['\n
    # '])`, '}', `ws([' '])`, `keyword(['else'])`, `ws([' '])`, '{',
    # `ws(['\n      '])`, `ws([])`, `ws([])`, '3', `ws(['\n    '])`,
    # '}']


def test_chained():
    parse_string("x.filter().join()")


def test_lambda():
    parse_string("x.filter |$x, $y| { $x + 1 }")


def test_chained_and_lambda():
    parse_string("x.filter |$x, $y| { $x + 1 }.join()")


def test_invoke():
    parse_string("""
    assert_type(
      Enum['always','daily','weekly','reluctantly'],
      $update['frequency'],
    )
    """)


def test_invoke_inside_if():
    # This previously failed due to invoke not handling trailing
    # commas. The above example (test_invoke) apparently worked,
    # because reasons.
    parse_string("""
    if $update['frequency'] {
      assert_type(
        Enum['always','daily','weekly','reluctantly'],
        $update['frequency'],
      )
    }
    """)


def test_funcall_with_block():
    parse_string("""
    assert_type(A::B, $name) |$_expected, $actual | {
      fail("msg")
    }
    """)


def test_funcall_with_block_inner():
    parse_string("""
    if $facts['package_provider'] == 'pkg' and $version =~ Undef {
      assert_type(A::B, $name) |$_expected, $actual | {
        fail("msg")
      }
    }
    """)


def test_string_interpolation_1():
    # This one is parsed as 'qn'
    parse_string("""
    "${x}"
    """)


def test_string_interpolation_2():
    # This one is parsed as 'var', but also looks like one
    parse_string("""
    "${$x}"
    """)


def test_string_interpolation_3():
    # qn
    parse_string("""
    "${x + 1}"
    """)


def test_string_interpolation_4():
    # var
    parse_string("""
    "${x + 1}"
    """)


def test_string_interpolation_access():
    # var, but looks like qn
    parse_string("""
    "${x['y']}"
    """)


def test_string_interpolation_deep_access():
    # var, but looks like qn
    parse_string("""
    "${x['y']['z']}"
    """)


def test_string_interpolation_call_method():
    # var, but looks like qn
    s = """
    "${x.y}"
    """
    parse_string(
        s,
        ast=PuppetConcat([
            PuppetCallMethod(
                func=PuppetBinaryOperator(
                    op='.',
                    lhs=PuppetVar(name='x'),
                    rhs=PuppetQn(name='y')),
                args=[],
                block=None)]))


def test_string_interpolation_deep_call_method():
    # var, but looks like qn
    s = """
    "${x.y.z}"
    """
    parse_string(
        s,
        ast=PuppetConcat([
            PuppetCallMethod(
                func=PuppetBinaryOperator(
                    op='.',
                    lhs=PuppetCallMethod(
                        func=PuppetBinaryOperator(
                            op='.',
                            lhs=PuppetVar(name='x'),
                            rhs=PuppetQn(name='y')),
                        args=[],
                        block=None),
                    rhs=PuppetQn(name='z')),
                args=[],
                block=None)]))


def test_string_interpolation_access_call():
    s = """
    "${x.y('Hello')[1]}"
    """

    parse_string(
        s,
        ast=PuppetConcat([
            PuppetAccess(
                how=PuppetCallMethod(
                    func=PuppetBinaryOperator(
                        op='.',
                        lhs=PuppetVar('x'),
                        rhs=PuppetQn('y')),
                    args=[PuppetString('Hello')],
                    block=None),
                args=[PuppetNumber(1)])]))


def test_string_interpolation_call_access():
    s = """
    "${x[1].y('Hello')}"
    """

    parse_string(
        s,
        ast=PuppetConcat([
            PuppetCallMethod(
                func=PuppetBinaryOperator(
                    op='.',
                    lhs=PuppetAccess(how=PuppetVar('x'),
                                     args=[PuppetNumber(1)]),
                    rhs=PuppetQn('y')),
                args=[PuppetString('Hello')],
                block=None)]))


def test_concat_with_actual_string():
    parse_string('"Hello ${name}"')


def test_collect():
    parse_string("""
    Exec <| title=='apt_update' |> {
      refreshonly => false,
    }
    """)


def test_collect_nested():
    parse_string("""
    if $_update['frequency'] == 'always' {
        Exec <| title=='apt_update' |> {
            refreshonly => false,
        }
    }
    """)


def test_unless():
    # Outer if to force unless to parse correctly
    parse_string("""
    if true {
        unless 1 {
            2
        } else {
            3
        }
    }
    """)


def test_bare_hash():
    # This will be parsed just as if there was a curly brace after
    # `"String",`, with a matching end brace before the closing
    # parenthesis.
    s = """
    f("String",
        x => 1,
        y => 2,
    )
    """
    parse_string(
        s,
        ast=PuppetInvoke(
            PuppetQn(name='f'),
            [PuppetString('String'),
             PuppetHash(entries=[HashEntry(PuppetQn('x'),
                                           PuppetNumber(1)),
                                 HashEntry(PuppetQn('y'),
                                           PuppetNumber(2))])]))


def test_literal_heredoc():
    s = """
    @(EOF)
    load_module Heredoc
    | EOF
    """
    parse_string(
        s,
        ast=PuppetLiteralHeredoc(content='load_module Heredoc\n')
    )


def test_heredoc():
    s = """
    @("EOF")
    load_module /usr/lib/nginx/modules/${modname}.so;
    | EOF
    """

    parse_string(s)


def test_heredoc_2():
    """
    Tests.

    - a variable alone on a line
    - A line starting with '#' (previously ignored as a comment ...)
    """
    s = """
    @("EOF")
       # File managed by puppet
       ${lines}
       | EOF
    """

    parse_string(s)
