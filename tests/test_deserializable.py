import pytest
from muppet.puppet.strings import (
    DocStringTag,
    DocStringParamTag,
    DocString,
    DataTypeAlias,
)


def test_deserialize_from_json():
    """
    Test deserializing of tag objects.

    Of note here is that DocStringTag have many subclasses, and should
    dynamically find the correct one based on 'tag_name'.

    Calling ``.to_json()`` directly on a `DocStringTag` (or on of its
    subclasses) is ratnher uninteresting, since the dynamic choice
    isn't available there.
    """
    data = {
        'tag_name': 'param',
        'text': 'Contents of tagname',
        'types': ['String'],
        'name': 'x',
    }

    assert [DocStringParamTag(**data)] \
        == DocString.from_json({
            'text': '',
            'tags': [data],
            }).tags


def test_deserialize_nested():
    """
    Test a nested parse.

    This test is slightly redundant.
    """
    text = 'A'
    tn = 'B'
    ttext = 'C'

    assert DocString(text=text,
                     tags=[DocStringTag(tag_name=tn,
                                        text=ttext)]) \
        == DocString.from_json({
            'text': text,
            'tags': [
                {
                    'tag_name': tn,
                    'text': ttext,
                    }
                ]
            })


def test_deserialize_from_json_error():
    with pytest.raises(AssertionError):
        DocString.from_json({
            'text': 'text',
            'tags': [
                'no',
            ]
        })
