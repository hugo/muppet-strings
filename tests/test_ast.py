"""
Test for building, and reserializing our Puppet ASTs.

All Puppet "child" declared in :py:mod:`muppet.puppet.ast`
should be tested here, with the exceptions noted below.

Note that PuppetParseError isn't tested, since there should be no way
to create those objects through ``build_ast``.

Skipped
-------

The following forms lack a dedicated test, for the reasons
stated bellow.

exported query & virtual query
    Only makes since in a collect statement

lambda
    Can only exist in certain contexts, so they are covered there.

qn, qr, nop
    Only exists within other forms.

str
    Not a true form, but part of string interpolation forms.
"""

from muppet.puppet.ast import (
    PuppetAST, build_ast,

    PuppetLiteral, PuppetAccess, PuppetBinaryOperator,
    PuppetUnaryOperator, PuppetArray, PuppetCallMethod,
    PuppetCase, PuppetDeclarationParameter,
    PuppetInstanciationParameter, PuppetClass, PuppetConcat,
    PuppetCollect, PuppetIfChain, PuppetUnless, PuppetKeyword,
    PuppetExportedQuery, PuppetVirtualQuery, PuppetFunction,
    PuppetHash, PuppetHeredoc, PuppetLiteralHeredoc, PuppetVar,
    PuppetLambda,  PuppetQn, PuppetQr, PuppetRegex,
    PuppetResource, PuppetDefine, PuppetString,
    PuppetNumber, PuppetInvoke, PuppetResourceDefaults,
    PuppetResourceOverride, PuppetDeclaration, PuppetSelector,
    PuppetBlock, PuppetNode, PuppetCall,

    HashEntry,

    # # These should be tested, if I figure out how to cause them
    # , PuppetParenthesis,
    # , PuppetNop

    # # This is intentionally ignored, since it should be impossible
    # # to cause.
    # , PuppetParseError
)
import muppet.puppet.parser
from muppet.puppet.format import serialize
from muppet.puppet.format.text import TextFormatter
import pytest


def parse(puppet_source: str) -> PuppetAST:
    """Shorthand for running the parser in tests."""
    return build_ast(muppet.puppet.parser.puppet_parser(
        puppet_source))

def ser(ast: PuppetAST) -> str:
    return serialize(ast, TextFormatter)


# from pprint import pprint
# def run(x):
#     """Function for generating test cases."""
#     pprint(parse(x))


def test_literal():
    s1 = "true"
    r1 = PuppetLiteral('true')
    assert parse(s1) == r1
    assert ser(r1) == s1


def test_keyword():
    s1 = "default"
    r1 = PuppetKeyword("default")

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_var():
    s1 = "$x"
    r1 = PuppetVar(name='x')

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_string():
    s1 = "'Hello'"
    r1 = PuppetString(s='Hello')

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_concat():
    s1 = '"Hello ${name}"'
    s2 = '"Hello ${$x + 1}"'
    s3 = '"Hello ${4}"'

    r1 = PuppetConcat(fragments=[PuppetString(s='Hello '),
                                 PuppetVar(name='name')])
    r2 = PuppetConcat(fragments=[PuppetString(s='Hello '),
                                 PuppetBinaryOperator(op='+',
                                                      lhs=PuppetVar(name='x'),
                                                      rhs=PuppetNumber(x=1))])
    r3 = PuppetConcat(fragments=[PuppetString(s='Hello '), PuppetVar(name='4')])

    assert parse(s1) == r1
    assert ser(r1) == s1
    assert parse(s2) == r2
    assert ser(r2) == s2
    assert parse(s3) == r3
    assert ser(r3) == s3


def test_access():
    s1 = "$x[1]"
    r1 = PuppetAccess(how=PuppetVar(name='x'), args=[PuppetNumber(x=1)])

    s2 = "$x[1, 2]"
    r2 = PuppetAccess(how=PuppetVar(name='x'),
                      args=[PuppetNumber(x=1), PuppetNumber(x=2)])

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2


def test_bin_op():
    s1 = "$x = $y"
    r1 = PuppetDeclaration(k=PuppetVar(name='x'), v=PuppetVar(name='y'))

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_un_op():
    s1 = "- $x"
    r1 = PuppetUnaryOperator(op='-', x=PuppetVar(name='x'))

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_array():
    s1 = '[]'
    r1 = PuppetArray([])

    s2 = """
[
  1,
  2,
]
    """.strip()
    r2 = PuppetArray(items=[PuppetNumber(x=1),
                            PuppetNumber(x=2)])

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2


def test_call():
    s1 = "$x = f(1, 5)"
    r1 = PuppetDeclaration(PuppetVar('x'),
                           PuppetCall(PuppetQn('f'),
                                      [PuppetNumber(1),
                                       PuppetNumber(5)]))

    assert parse(s1) == r1
    assert ser(r1) == s1


@pytest.mark.xfail(reason=". is treated as a binary operator, so spaces are added")
def test_call_method():
    """
    Test method calls.

    This method also covers lambda.
    """
    s1 = 'm.f()'
    s2 = 'm.f(1)'
    s3 = 'm.f |$x| { 1 }'

    r1 = PuppetCallMethod(func=PuppetBinaryOperator(op='.',
                                                    lhs=PuppetQn(name='m'),
                                                    rhs=PuppetQn(name='f')),
                          args=[])
    r2 = PuppetCallMethod(func=PuppetBinaryOperator(op='.',
                                                    lhs=PuppetQn(name='m'),
                                                    rhs=PuppetQn(name='f')),
                          args=[PuppetNumber(x=1)])
    r3 = PuppetCallMethod(func=PuppetBinaryOperator(op='.',
                                                    lhs=PuppetQn(name='m'),
                                                    rhs=PuppetQn(name='f')),
                          args=[],
                          block=PuppetLambda(params=[PuppetDeclarationParameter(k='x')],
                                             body=[PuppetNumber(x=1)]))

    assert parse(s1) == r1
    assert ser(r1) == s1
    assert parse(s2) == r2
    assert ser(r2) == s2

    assert parse(s3) == r3
    assert ser(r3) == s3


def test_case():
    s1 = """
case 1 {
  'a': {
    1
  }
  /b/, /c/: {
    2
  }
}
    """.strip()

    r1 = PuppetCase(test=PuppetNumber(x=1),
                    cases=[([PuppetString(s='a')], [PuppetNumber(x=1)]),
                           ([PuppetRegex(s='b'), PuppetRegex(s='c')],
                            [PuppetNumber(x=2)])])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_collect():
    s1 = "File <| 10 |>"
    s2 = "File <<| 20 |>>"

    r1 = PuppetCollect(type=PuppetQr(name='File'),
                       query=PuppetVirtualQuery(q=PuppetNumber(x=10)))
    r2 = PuppetCollect(type=PuppetQr(name='File'),
                       query=PuppetExportedQuery(filter=PuppetNumber(x=20)))

    assert parse(s1) == r1
    assert ser(r1) == s1
    assert parse(s2) == r2
    assert ser(r2) == s2


def test_if():
    s1 = """
if 1 {
  'a'
}
    """.strip()

    s2 = """
if 1 {
  'a'
} else {
  'b'
}
    """.strip()

    s3 = """
if 1 {
  'a'
} elsif 2 {
  'b'
} else {
  'c'
}
    """.strip()

    r1 = PuppetIfChain([(PuppetNumber(x=1), [PuppetString(s='a')])])

    r2 = PuppetIfChain([(PuppetNumber(x=1), [PuppetString(s='a')]),
                        ('else', [PuppetString(s='b')])])

    r3 = PuppetIfChain([(PuppetNumber(x=1), [PuppetString(s='a')]),
                        (PuppetNumber(x=2), [PuppetString(s='b')]),
                        ('else', [PuppetString(s='c')])])

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2

    assert parse(s3) == r3
    assert ser(r3) == s3


def test_unless():
    s1 = """
unless 1 {
  'a'
}
    """.strip()
    r1 = PuppetUnless(condition=PuppetNumber(x=1),
                      consequent=[PuppetString(s='a')])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_hash():
    s1 = "{}"
    # TODO alignment
    s2 = """
{
  a => 1,
  long_key => 'Hello',
}
    """.strip()

    r1 = PuppetHash(entries=[])
    r2 = PuppetHash([HashEntry(k=PuppetQn(name='a'),
                               v=PuppetNumber(x=1)),
                     HashEntry(k=PuppetQn(name='long_key'),
                               v=PuppetString(s='Hello'))])

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2


def test_heredoc():
    s1 = r"""@("EOF"/$rt)
\$ ${x}
\r \\
| EOF"""

    r1 = PuppetHeredoc([PuppetString(s='$ '),
                        PuppetVar(name='x'),
                        PuppetString(s='\n\r \\\n')])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_literalheredoc():
    s1 = """
@(EOF)
  Hello, ${world}"
  | EOF
    """.strip()

    # Test both that EOF isn't hard coded
    # Note that this is slightly hacky with giving EOL, since we need
    # to "know" that is how the code is implemented.
    #
    # Test syntax at same ttime
    s2 = """
@(EOL:txt)
  EOF
  |- EOL
  """.strip()

    r1 = PuppetLiteralHeredoc('Hello, ${world}"\n')
    r2 = PuppetLiteralHeredoc('EOF', syntax='txt')

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2


@pytest.mark.skip(reason="When is this triggered?")
def test_parenthesis():
    # TODO
    assert parse(s1) == r1
    assert ser(r1) == s1


def test_regex():
    s1 = "/test/"
    r1 = PuppetRegex('test')

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_invoke():
    s1 = "f()"
    s2 = "f(1, 2)"
    s3 = "include ::example"

    r1 = PuppetInvoke(func=PuppetQn(name='f'), args=[])
    r2 = PuppetInvoke(func=PuppetQn(name='f'),
                      args=[PuppetNumber(x=1), PuppetNumber(x=2)])
    r3 = PuppetInvoke(func=PuppetQn(name='include'),
                      args=[PuppetQn(name='::example')])

    assert parse(s1) == r1
    assert ser(r1) == s1

    assert parse(s2) == r2
    assert ser(r2) == s2

    assert parse(s3) == r3
    assert ser(r3) == s3


def test_resourcedefaults():
    s1 = """
File {
  path => '/',
}
    """.strip()

    r1 = PuppetResourceDefaults(
        type=PuppetQr(name='File'),
        ops=[PuppetInstanciationParameter(k='path',
                                          v=PuppetString(s='/'),
                                          arrow='=>')])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_resourceoverride():
    s1 = """
File['/'] {
  ensure => absent,
}
    """.strip()

    r1 = PuppetResourceOverride(
        resource=PuppetAccess(how=PuppetQr(name='File'),
                              args=[PuppetString(s='/')]),
        ops=[PuppetInstanciationParameter(k='ensure',
                                          v=PuppetQn(name='absent'),
                                          arrow='=>')])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_declaration():
    s1 = "$x = 10"
    r1 = PuppetDeclaration(k=PuppetVar(name='x'), v=PuppetNumber(x=10))

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_selector():
    s1 = """
$test ? {
  Int   => 1,
  Float => 2,
}
    """.strip()

    r1= PuppetSelector(resource=PuppetVar(name='test'),
                       cases=[(PuppetQr(name='Int'), PuppetNumber(x=1)),
                              (PuppetQr(name='Float'), PuppetNumber(x=2))])


    assert parse(s1) == r1
    assert ser(r1) == s1


def test_block():
    s1 = """
1
2
""".strip()
    r1 = PuppetBlock(entries=[PuppetNumber(x=1), PuppetNumber(x=2)])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_node():
    s1 = """
node 'node.example.com' {
  include profiles::example
}
    """.strip()

    r1 = PuppetNode(matches=[PuppetString('node.example.com')],
                    body=[PuppetInvoke(func=PuppetQn('include'),
                                       args=[PuppetQn('profiles::example')])])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_resource():

    # TODO alignment
    s1 = """
file { '/path':
  key => 'value',
  * => {},
}
    """.strip()

    s2 = """
file {
  default:
    mode => '0700',
    ;
  [
    '/a',
    '/b',
  ]:
    user => 'root',
    ;
}
    """.strip()

    r1 = PuppetResource(type=PuppetQn(name='file'),
                        bodies=[(PuppetString(s='/path'),
                                 [PuppetInstanciationParameter(k='key',
                                                               v=PuppetString(s='value'),
                                                               arrow='=>'),
                                  PuppetInstanciationParameter(k='*',
                                                               v=PuppetHash(entries=[]),
                                                               arrow='=>')])])

    r2 = PuppetResource(
            type=PuppetQn(name='file'),
            bodies=[(PuppetKeyword(name='default'),
                     [PuppetInstanciationParameter(k='mode',
                                                   v=PuppetString(s='0700'),
                                                   arrow='=>')]),
                    (PuppetArray(items=[PuppetString('/a'),
                                        PuppetString('/b')]),
                     [PuppetInstanciationParameter(k='user',
                                                   v=PuppetString('root'),
                                                   arrow='=>')])])

    assert parse(s1) == r1
    assert ser(r1) == s1
    assert parse(s2) == r2
    assert ser(r2) == s2


def test_define():
    s1 = """
define a::b (
  String $x,
  $y,
  $z = 20,
  String $w = '10',
) {
  include ::something
}
    """.strip()

    r1 = PuppetDefine(name='a::b',
                      params=[PuppetDeclarationParameter(k='x',
                                                         v=None,
                                                         type=PuppetQr(name='String')),
                              PuppetDeclarationParameter(k='y', v=None, type=None),
                              PuppetDeclarationParameter(k='z',
                                                         v=PuppetNumber(x=20),
                                                         type=None),
                              PuppetDeclarationParameter(k='w',
                                                         v=PuppetString(s='10'),
                                                         type=PuppetQr(name='String'))],
                      body=[PuppetInvoke(func=PuppetQn(name='include'),
                                         args=[PuppetQn(name='::something')])])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_function():
    s1 = """
function f (
  $x,
) >> String {
  $x
}
    """.strip()

    r1 = PuppetFunction(
        name='f',
        params=[PuppetDeclarationParameter(k='x')],
        returns=PuppetQr(name='String'),
        body=[PuppetVar(name='x')])

    assert parse(s1) == r1
    assert ser(r1) == s1


def test_class():
    s1 = """
class name (
  String $x = 'Hello',
  Int $y = 10,
) {
  notice($x)
}
    """.strip()

    r1 = PuppetClass(name='name',
                     params=[
                         PuppetDeclarationParameter(
                             k='x',
                             v=PuppetString(s='Hello'),
                             type=PuppetQr(name='String')),
                         PuppetDeclarationParameter(
                             k='y',
                             v=PuppetNumber(x=10),
                             type=PuppetQr(name='Int'))
                     ],
                     body=[PuppetInvoke(func=PuppetQn(name='notice'),
                                        args=[PuppetVar(name='x')])])

    assert parse(s1) == r1
    assert ser(r1) == s1
