.. _highlight-readme:

.. Introduction

Syntax highlighting in the HTML output is handled in a way to allow
easy changing of highlighting engine, and of colorschemes.
All syntax objects are grouped into one of the Abstract highlight
groups (see :ref:`abstract-highlight-groups`). Highlighting files maps
these groups onto distinct CSS classes depending on the highlighting
engine, and colorscheme files maps these into CSS styles.



Colorschemes
============

Colorscheme files should be placed in ``colorscheme/*.yaml``, and
should contain a mapping for all (wanted) abstract highlight groups,
onto mappings containing (any of) the following keys:

- ``color``
- ``background``
- ``font-style``
- ``font-weight``

These will be mapped directly into the CSS.

The special key *raw-append* also exists, which, if present, should
contain raw CSS code which will be appended to this colorscheme.
This is mainly useful to target highlighting specific classes not
exposed by the abstract highlight groups.

.. TODO useful raw selectors
    - ``:root``
    - ``.highlight-pygments``
    - ``.highlight-andre-simon``
    - ``.highlight-muppet``

Sample CSS
----------

The generated CSS code will have the following form detailed in
:ref:`sample-css`. Note that the selector (``.comment``) will depend
on the highlight file, while the variable names (``--hl-comment-*``)
depend on the abstract highlight group.

.. code-block:: css
   :name: sample-css
   :caption: fragment of generated CSS for a colorscheme.

    .comment {
        color:            var(--hl-comment-color);
        background-color: var(--hl-comment-background);
        font-style:       var(--hl-comment-font-style);
        font-weight:      var(--hl-comment-font-weight);
    }


Highlight files
===============

Highlight files map ref:`abstract-highlight_group` to concrete CSS
classes. Each Yaml file (located in ``highlight/*.yaml``) should
contain a mapping from abstract highlight groups, to lists of css
classes.


.. _abstract-highlight-group:

Abstract highlight groups
=========================

The available Abstract highlight groups are as follows:

``comment``
	Comments

``error``
	TODO

``escape``
	Escape sequences embedded in strings

``interpolate``
	Variables interpolated in strings

``keyword``
	Language reserved words (like ``class``, ...)

``line``
	Line numbers (not part of the actual code)

``number``
	Numeric literals

``operator``
	operators (``+``, ``in``, ...)

``regex``
	Regex literals

``special``
	TODO

``string``
	String literals

``variable``
	Variables
