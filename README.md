Muppet Strings
==============

*Like puppet strings, but for multiple modules.*

Utility for building hyperlinked and pretty doc from Puppet modules and
environments.

Usage
-----

### Building the example (probably broken for you)

```bash
make clean all
```

### Running the tests

```bash
make check  # Run linters
make test   # Run unit tests
```

Coverage information will end up in `htmlcov/`.

### Building the documentation
```bash
make documentation
```

Documentation will end up in `doc.rendered/`.


Dependencies
------------

General python dependencies are documented in [./setup.cfg](`setup.cfg`).

### Runtime dependencies
- Puppet
- Puppet Strings

### Testing dependencies
- `flake8`
- `mypy`
- `pytest`

### Documentation dependencies
- sphinx
