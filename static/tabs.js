window.addEventListener('load', function () {
	for (let tabgroup of document.getElementsByClassName('tabs')) {
		let tab_labels = tabgroup.querySelectorAll('[data-tab]')
		let tabs = tabgroup.getElementsByClassName('tab')
		for (let tab_label of tab_labels) {
			tab_label.addEventListener('click', function () {
				for (let tab of tabs) {
					tab.classList.remove('selected')
				}
				document.getElementById(tab_label.dataset.tab).classList.add('selected')
			})
		}
	}
});
