"""Intersperse."""

from typing import TypeVar
from collections.abc import Sequence, Generator


T = TypeVar('T')
U = TypeVar('U')


def intersperse(inset: U, sequence: Sequence[T]) -> Generator[U | T, None, None]:
    """Intersperse the inset between each element in sequence."""
    if not sequence:
        return

    yield sequence[0]
    for item in sequence[1:]:
        yield inset
        yield item
