"""Thingies for an HTML tab widget."""

from dataclasses import dataclass
from jinja2 import (
    Environment,
    # PackageLoader,
    FileSystemLoader,
)


env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=False,
)


@dataclass
class Tab:
    """
    A single tab part of a tab group.

    :parameters:
        title - Display name of the tab
        id - Internal reference ID, must be globaly unique
        content - contents of the tab
    """

    title: str
    id: str
    content: str


@dataclass
class TabGroup:
    """A collection of tabs."""

    id: str
    tabs: list[Tab]


def tab_widget(tabgroup: TabGroup) -> str:
    """
    Render a HTML tab widget.

    The argument is the list of tabs, nothing is returned, but instead
    written to stdout.
    """
    template = env.get_template('snippets/tabset.html')
    return template.render(tabset=tabgroup)


__counter = 0


def next_id(prefix: str = 'id') -> str:
    """Return a new unique id."""
    global __counter
    __counter += 1
    return f'{prefix}-{__counter}'


def tabs(panes: dict[str, str]) -> str:
    """
    Build a tab widget from given dictionary.

    Keys are used as tab names, values as tab content.
    Id's are generated.
    """
    tabs = []
    for title, content in panes.items():
        tabs.append(Tab(title=title, content=content, id=next_id('tab')))

    return tab_widget(TabGroup(id=next_id('tabgroup'), tabs=tabs))
