"""New, better, entry point."""

import argparse
import pathlib
import os
import logging
import colorlog
import subprocess
import shutil

from .cache import Cache
# from .gather import get_module, get_modules, ModuleEntry
from .output import PuppetEnvironment

# Our parser combinator system builds heavily on recursing,
# overflowing Pythons' stack. Simply cranking up the stack size solves
# this problem.
import sys
sys.setrecursionlimit(100_000)

logger = logging.getLogger('muppet')
logger.setLevel(logging.INFO)


ch = colorlog.StreamHandler()
ch.setLevel(logging.DEBUG)
# formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
formatter = colorlog.ColoredFormatter('%(log_color)s%(name)s:%(lineno)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

formatter2 = colorlog.ColoredFormatter('%(log_color)s%(name)s - %(file)s - %(message)s')
ch2 = colorlog.StreamHandler()
ch2.setLevel(logging.DEBUG)
ch2.setFormatter(formatter2)
logging.getLogger('muppet.puppet-string').addHandler(ch2)


def __main() -> None:
    parser = argparse.ArgumentParser(
            prog='muppet',
            description='Sets up puppet doc')

    parser.add_argument('--env', action='store', default='/etc/puppetlabs/code/modules',
                        help='''
                        Path to a puppet `modules` directory.
                        ''')
    # If deploying to http://example.com/~user/muppet then this should
    # be set to `~/user/muppet`
    parser.add_argument('--path-base', action='store', metavar='path_base',
                        default='', help='''
                        Prefix to web path the pages will be displayed under.
                        ''')
    parser.add_argument('modules', nargs='*', type=pathlib.Path, help='''
                        Any number of specific modules to generate documentation for.
                        Mutually exclusive with --env.
                        ''')

    args = parser.parse_args()

    cache = Cache('/home/hugo/.cache/muppet-strings')

    output_directory = 'output'

    # modules: list[ModuleEntry]
    # if args.modules != []:
    #     modules = [get_module(cache, mod)
    #                for mod in args.modules]
    # else:
    #     modules = get_modules(cache, env)

    env = PuppetEnvironment(source_path=args.env,
                            output_prefix=args.path_base,
                            cache=cache)
    env.output(output_directory)

    # TODO do this properly
    subprocess.run(['make',
                    '-C', 'static-src',
                    '--silent',
                    'install-full',
                    f'PREFIX={os.path.join(os.getcwd(), output_directory)}'],
                   check=True)
    shutil.copytree('static', os.path.join(output_directory, 'static'),
                    dirs_exist_ok=True)


if __name__ == '__main__':
    __main()
