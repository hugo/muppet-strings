"""
Muppet central markdown module.

This module exports one procedure, ``markdown``. This to easily
configure all markdown rendering to be the same in one central place,
and to allow "easy" switching of the markdown engine.
"""

from markdown_it import MarkdownIt
# Mypy believes that mdit_py_plugins.anchors doesn't explicitly export
# "anchors_plugin" ([attr-defined]), but it does.
from mdit_py_plugins.anchors import anchors_plugin  # type: ignore


def markdown(text: str) -> str:
    """
    Render the given markdown string to HTML.

    The current implementations sets these options and plugins:

    html
        Enable HTML in the source, which will be passed verbatim

    linkify
        Things which looks like links will be hyperlinked

    anchors_plugin
        Each header will get an appropriate id-attribute set, allowing
        hyperlinks to it.

    table
        Allow markdown tables.

    strikethrough
        Allow GFM-like strikethrough (``~~striken out~~``).

    :param text:
        A Markdown string.

    :returns:
        A HTML string.
    """
    md = MarkdownIt('commonmark', {'html': True, 'linkify': True}) \
        .use(anchors_plugin) \
        .enable('table') \
        .enable('strikethrough')

    output = md.render(text)
    if not isinstance(output, str):
        raise ValueError(f"Unexpected markdown output: expected str, got {type(output)}")
    return output


# header_text.downcase().replace(' ', '-')
