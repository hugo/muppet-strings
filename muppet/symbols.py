"""
Prettify symbols appearing in puppet code.

For example, replace bangs ('!') with negation signs ('¬').
"""

symbols: dict[str, str] = {  # pragma: no cover
    '=>': '⇒',
    '!': '¬',
    '!=': '≠',
    '*': '×',
    '>=': '≥',
    '<=': '≤',
    '~>': '⤳',
    '->': '→',
    '==': '≡',
    '!~': '≁',
}


def prettify(symb: str) -> str:  # pragma: no cover
    """Either turn the symbol into it's "pretty" variant, or return itself."""
    return symbols.get(symb, symb)
