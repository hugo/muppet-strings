"""
Python wrapper around puppet strings.

This maps
`Puppet Strings <https://www.puppet.com/docs/puppet/7/puppet_strings.html>`_
JSON output onto python objects, using type-level reflection (see
:class:`Deserializable` for details).

Fields declared without defaults are required, those with defaults are
optional. Extra fields may be given, but will result in a warning
being logged (TODO make this configurable).

Puppet Strings output should follow
`this schema <https://github.com/puppetlabs/puppet-strings/blob/main/JSON.md>`_,
unfortunately, that document is out of date [#f1]_, so the following
is mostly based on observation.

Some of the fields defined here have an Optional type, all these can
be changed to simply having a default value, if that better reflects
the possibilities of the field. The same is true with the above types
swaped.

.. rubric:: Footnotes

.. [#f1] It's out of date as of writing this (2023-07-03)
"""

import subprocess
from typing import (
    Any,
    Optional,
    Protocol,
)
from dataclasses import dataclass, field
import logging
from .internal import Deserializable
import re
import json
import os.path
import hashlib
from muppet.cache import AbstractCache


logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class DocStringTag(Deserializable):
    """
    A generic DocString tag.

    These are the tagged blocks/lines in puppet comments, on the form:

    .. code-block:: puppet

        # @sumamry
        #   The summary of the module

    Everything after ``@summary`` on the same line is in this document
    refered to as the "tag line", while the indented body below is
    refered as the "tag body"

    Note that many of these tags can be "generated" from the source.
    For example, the following block will have a return tag in the
    output of Puppet STrings, due to it's in code return type
    declaration. This holds for other types as well.

    .. code-block:: puppet

        function f() >> Integer {
            1
        }


    Finnaly, the tags `@!puppet.type.param` and
    `!puppet.type.property` exists for dynamically generated
    parameters and properties, respectively. These are currently not
    handled separately by us.

    https://www.puppet.com/docs/puppet/7/puppet_strings.html#strings-tags

    :param tag_name:
        Type of the tag, like 'param', 'api', e.t.c.

    :param text:
        Freeform text content of the tag.
    """

    tag_name: str
    text:     str = ''

    @staticmethod
    def _key() -> str:
        raise NotImplementedError()

    @classmethod
    def get_type(cls, obj: dict[str, Any]) -> type['DocStringTag']:
        """
        Find the correct tag subclass for the given dict.

        Find the subclass of `DocStringTag` where ``_key()`` returns
        the same name as the key "tag_name" in the given dictionary.

        Defaults to the bare `DocStringTag`.

        :param obj:
            Object should be a dictionary representing a tag, as
            returned by ``puppet strings``.

            .. code-block:: json
               :caption:
                 An example JSON object.

                {
                  "tag_name": "example",
                  "text": "include ::test",
                  "name": "block name"
                }
        """
        assert 'tag_name' in obj, "Tag dict MUST contain tag_name"
        for c in cls.__subclasses__():
            if obj['tag_name'] == c._key():
                return c
        else:
            return DocStringTag


@dataclass(kw_only=True)
class DocStringParamTag(DocStringTag):
    """
    Documentation for a parameter to a class, definition, or function.

    The following puppet code

    .. code-block:: puppet

        # @param path
        #   Where the file should end up
        class CLS (String $x = '/dev/null') {
            # ...
        }

    Would yield the following the object:

    .. code-block:: python

        DocStringParamTag(tag_name='param',
                          text='Where the file should end up'
                          types=['String'])

    TODO link to how default values are handled.

    :param types:
        A singleton list containing the type, exactly as it appeared
        in the Puppet source (this means that compound types with
        internal newlines will still have their newlines).

        Note that the type declaration can also appear in the
        documentation tag instead of the source code.

        TODO won't this cause our parser to sometimes fail?

    :param name:
        Parameter name.
    """

    name: str
    types: Optional[list[str]] = None

    @staticmethod
    def _key() -> str:
        return 'param'


@dataclass(kw_only=True)
class DocStringExampleTag(DocStringTag):
    """
    An example block.

    .. code-block:: puppet

        # @example Name of the block
        #   !!Contents of the block
        #   Can (and probably should)
        #   be multiline!!

    :param text:
        The contents of the example. Will in the example above be the
        string delimited by '!!'. Whitespace equivalent to the
        indentation of the first line will be removed from each line,
        trailing whitespace is always removed.

    :param name:
        The name of the block, "Name of the block" in the example
        above.

        Will be the empty string if not set.
    """

    name: str

    @staticmethod
    def _key() -> str:
        return 'example'


@dataclass(kw_only=True)
class DocStringOverloadTag(DocStringTag):
    """
    The overload "tag".

    This isn't a real tag, but rather something ``puppet strings``
    generates for overrides in Puppet 4.x functions written in Ruby
    (and possibly others).

    One such example is puppetlabs-stdlib's
    lib/puppet/functions/validate_legacy.rb (as of v8.1.0)
    https://github.com/puppetlabs/puppetlabs-stdlib/blob/v8.1.0/lib/puppet/functions/validate_legacy.rb

    :param signature:
        A normalized version of the function signature.

        .. code-block:: python

            "validate_legacy(Any $scope, String $type_string, Any $value, Any *$args)"

    :param docstring:
        The actual documentation for the instance. Usually rather
        long.

    :param defaults:
        TODO

    """

    name: str
    signature: str
    docstring: Optional['DocString'] = None
    defaults: dict[Any, Any] = field(default_factory=dict)

    @staticmethod
    def _key() -> str:
        return 'overload'


@dataclass(kw_only=True)
class DocStringOptionTag(DocStringTag):
    """
    An option for a hash parameter.

    Parameters can be hashes, which should have their options
    documented.

    .. code-block:: puppet
      :caption:
        Example borrowed from the `puppet documentation
        <https://www.puppet.com/docs/puppet/7/puppet_strings.html#strings-tags>`_

        # @param [Hash] opts
        #   List of options
        # @option opts [String] :option1
        #   option 1 in the hash
        # @option opts [Array] :option2
        #   option 2 in the hash

    :param opt_name:
        Name of the option. This is what will be given a string key.
    :param opt_text:
        The description of the option, given in the tag body.
    :param opt_types:
        A singleton list, if present, declaring valid value types. See
        @param for further details.
    :param parent:
        The hash parameter which this option belongs to. Would be
        ``opts`` in the above example.
    :param name:
        Seems to always be equal to parent.
    """

    opt_name:   str
    opt_text:   str
    parent:     str
    name:       str
    opt_types:  Optional[list[str]] = None

    @staticmethod
    def _key() -> str:
        return 'option'


@dataclass(kw_only=True)
class DocStringAuthorTag(DocStringTag):
    """
    Declaration of the author of an object.

    .. code-block:: puppet

        # @author Hugo Hörnquist

    :param text:
        The name of the author, "Hugo Hörnquist" in this example.
    """

    @staticmethod
    def _key() -> str:
        return 'author'


@dataclass(kw_only=True)
class DocStringApiTag(DocStringTag):
    """
    Declaration of API visibility.

    .. code-block:: puppet

        # @api private

    :param text:
        Only the value "private" seems to be in use.
    """

    @staticmethod
    def _key() -> str:
        return 'api'


@dataclass(kw_only=True)
class DocStringRaiseTag(DocStringTag):
    """
    Information about what a function may raise.

    .. code-block:: puppet

        # @raise PuppetError this error is raised if x

    :param text:
        The entire contents contentns of the tag.
        The first word doesn't get any special treatment.
    """

    @staticmethod
    def _key() -> str:
        return 'raise'


@dataclass(kw_only=True)
class DocStringReturnTag(DocStringTag):
    """
    A return type annotation.

    Notes what a function will return, can be given multiple times if
    if multiple return values are possible. It's recommended that they
    then start with "if ..."

    .. code-block:: puppet

        # @return [String] if something is true

    :param text:
        Freeform text descriping the return value. Can be the empty string.

    :param types:
        A singleton list, if present, containing the noted return type.
    """

    types: Optional[list[str]] = None

    @staticmethod
    def _key() -> str:
        return 'return'


@dataclass(kw_only=True)
class DocStringSinceTag(DocStringTag):
    """
    Declaration of when a procedure was added.

    Barely in use. Blame git blame.

    :param text:
        Free text describing version when the object was added.
    """

    @staticmethod
    def _key() -> str:
        return 'since'


@dataclass(kw_only=True)
class DocStringSummaryTag(DocStringTag):
    """
    Short summary of the object.

    :param text:
        Free-text summary of the object, should be limited to 140
        characters.
    """

    @staticmethod
    def _key() -> str:
        return 'summary'


@dataclass(kw_only=True)
class DocStringSeeTag(DocStringTag):
    """
    A see other tag.

    .. code-block:: puppet

        # @see https://example.com For more information

    .. code-block:: python

        DocStringSeeTag(name="https://example.com",
                        text="For more information")

    :param name:
        The first word of the tag line. Should be an URL or code object.

    :param text:
        Remaining elements on tag line, or the tag body. A freeform
        description of the reference for human consumption.
    """

    name: str

    @staticmethod
    def _key() -> str:
        return 'see'


@dataclass(kw_only=True)
class DocString(Deserializable):
    """Documentation entry for any type of object."""

    text: str
    tags: list[DocStringTag] = field(default_factory=list)

    @staticmethod
    def handle_tags(items: list[dict[str, Any]]) -> list[DocStringTag]:
        """
        Parse list of tag dictionaries.

        The dynamic type choice is done here instead of in
        `DocStringTag`, due to how `Deserialize` is constructed. In
        short, it's much easier to modify the deserialization
        behaviour of children rather than changing oneself. #deep

        See `DocStringTag.get_type` for further information.
        """
        result: list[DocStringTag] = []
        for object in items:
            cls = DocStringTag.get_type(object)
            try:
                result.append(cls(**object))
            except TypeError as e:
                logger.error("Bad tag set for tag object (class=%s) %s (%s)",
                             cls.__name__, object, e,
                             exc_info=e)
                raise e
        return result


@dataclass(kw_only=True)
class PuppetClass(Deserializable):
    """Documentation for a puppet class."""

    name:       str
    file:       str
    line:       int
    inherits:   Optional[str] = None
    docstring:  DocString
    defaults:   dict[str, str] = field(default_factory=dict)
    source:     str


@dataclass(kw_only=True)
class DataType(Deserializable):
    """Documentation for a data type."""

    name:       str
    file:       str
    line:       int
    docstring:  DocString
    defaults:   dict[str, str]
    # source: str


@dataclass(kw_only=True)
class DataTypeAlias(Deserializable):
    """Documentation for a type alias."""

    name:       str
    file:       str
    line:       int
    docstring:  DocString
    alias_of:   str
    # source: str


@dataclass(kw_only=True)
class DefinedType(Deserializable):
    """Documentation for a defined type."""

    name: str
    file: str
    line: int
    docstring: DocString
    defaults: dict[str, str] = field(default_factory=dict)
    source: str


@dataclass(kw_only=True)
class Provider(Deserializable):
    """Documentation for a resource type provider."""

    name:       str
    type_name:  str
    file:       str
    line:       int
    docstring:  DocString
    confines:   dict[str, str] = field(default_factory=dict)
    features:   list[str] = field(default_factory=list)
    defaults:   list[Any] = field(default_factory=list)
    commands:   dict[str, str] = field(default_factory=dict)


@dataclass(kw_only=True)
class ResourceTypeProperty(Deserializable):
    """Documentation for a property of a resource type."""

    name:         str
    description:  str
    values:       Optional[list[str]] = None
    aliases:      dict[str, str] = field(default_factory=dict)
    isnamevar:    bool = False
    default:      Optional[str] = None

    required_features: Optional[str] = None
    data_type: Optional[str] = None


@dataclass(kw_only=True)
class ResourceTypeParameter(Deserializable):
    """Documentation for a parameter of a resource type."""

    name:         str
    description:  Optional[str] = None
    values:       Optional[list[str]] = None
    aliases:      Any = field(default_factory=list)
    isnamevar:    bool = False
    default:      Optional[str] = None

    required_features: Optional[str] = None
    data_type: Optional[str] = None


@dataclass(kw_only=True)
class ResourceTypeFeature(Deserializable):
    """Documentation for a published feature of a resource type."""

    name:         str
    description:  str


@dataclass(kw_only=True)
class ResourceType(Deserializable):
    """Documentation for a resource type."""

    name:        str
    file:        str
    line:        int
    docstring:   DocString
    properties:  list[ResourceTypeProperty] = field(default_factory=list)
    parameters:  list[ResourceTypeParameter]
    features:    Optional[list[ResourceTypeFeature]] = None
    providers:   Optional[list[Provider]] = None


@dataclass(kw_only=True)
class Signature(Deserializable):
    """Documentation for a function signature."""

    signature: str
    docstring: DocString


@dataclass(kw_only=True)
class Function(Deserializable):
    """Documentation for a function."""

    name:        str
    file:        str
    line:        int
    type:        str  # Probably one of 'ruby3x', 'ruby4x', 'puppet'
    signatures:  list[Signature]
    docstring:   DocString
    defaults:    dict[str, str] = field(default_factory=dict)
    source:      str


@dataclass(kw_only=True)
class Task(Deserializable):
    """Documentation for a task."""

    name:           str
    file:           str
    line:           int
    docstring:      Optional[DocString] = None
    source:         str
    supports_noop:  bool
    input_method:   Any


@dataclass(kw_only=True)
class Plan(Deserializable):
    """Documentation for a plan."""

    name:       str
    file:       str
    line:       int
    docstring:  DocString
    defaults:   dict[str, Any]
    source:     str


@dataclass(kw_only=True)
class PuppetStrings(Deserializable):
    """Complete documentation for a Puppet module."""

    puppet_classes:     list[PuppetClass]
    data_types:         list[DataType]
    data_type_aliases:  list[DataTypeAlias]
    defined_types:      list[DefinedType]
    resource_types:     list[ResourceType]
    providers:          list[Provider]
    puppet_functions:   list[Function]
    puppet_tasks:       list[Task]
    puppet_plans:       list[Plan]


# --------------------------------------------------


class HasDocstring(Protocol):
    """Something which has a docstring attribute."""

    docstring: DocString


def isprivate(entry: HasDocstring) -> bool:
    """
    Is the given puppet declaration marked private.

    Assumes input is a dictionary as returned by puppet strings, one
    of the entries in (for example) 'puppet_classes'.

    Currently only checks for an "@api private" tag.
    """
    for tag in entry.docstring.tags:
        if tag.tag_name == 'api' and \
           tag.text == 'private':
            return True
    return False


def puppet_strings(path: str) -> bytes:
    """
    Run ``puppet strings`` on puppet module at path.

    Returns a bytes object rather than a :class:`PuppetStrings`
    object, to help with the implementation of cachinge.

    See :func:`puppet_strings_cached` for a caching version.

    .. code-block:: python
       :caption: Example Invocation

        >>> path = "/etc/puppetlabs/code/modules/stdlib"
        >>> PuppetStrings.from_json(puppet_strings(path))
    """
    # All this extra weird stuff with tempfiles and pipes since puppet
    # strings output errors on stdout, and only if the --out flag
    # isn't used.
    import tempfile

    tmpfile = tempfile.NamedTemporaryFile()
    logger = logging.getLogger('muppet.puppet-strings')
    # logger = ILoggerAdapter(logging.getLogger('muppet.puppet-strings'),
    #                         extra={'file': path})

    cmd = subprocess.Popen(
            ['puppet', 'strings', 'generate',
             '--format', 'json',
             '--out', tmpfile.name],
            cwd=path,
            stdout=subprocess.PIPE,
            text=True,
            )

    if not cmd.stdout:
        # TODO better error?
        raise Exception(cmd.returncode)

    for line in cmd.stdout:
        line = line.strip()
        # These debug levels are by best effort, and based on the
        # enum found here:
        # https://github.com/puppetlabs/puppet-strings/blob/afe75151f8b47ce33433c488e22ca508aa48ac7c/spec/unit/puppet-strings/yard/handlers/ruby/rsapi_handler_spec.rb#L105
        # Expected formatting from observing the output.
        # TODO document this special logger
        if m := re.match(r'^\[(\w+)]: (.*)', line):
            match m[1]:
                case "debug":  logger.debug(m[2])
                case "warn":   logger.warning(m[2])
                case "error":  logger.error(m[2])
                case _:        logger.warning(line)
        else:
            logger.info(line)

    return tmpfile.read()


def puppet_strings_cached(path: str, cache: AbstractCache) -> PuppetStrings:
    """
    Run puppet strings, but check cache first.

    :param path:
        Path to the source of this module.

    :param cache:
        Cache instance for
    """
    print("path =", path)
    try:
        with open(os.path.join(path, 'metadata.json'), 'rb') as f:
            # self.metadata = json.loads(data)
            data = f.read()
            checksum = hashlib.sha1(data).hexdigest()
            key = f'puppet-strings{checksum}'

            if parsed := cache.get(key):
                result = parsed
            else:
                result = puppet_strings(path)
                cache.put(key, result)
    except FileNotFoundError:
        result = puppet_strings(path)

    return PuppetStrings.from_json(json.loads(result))
