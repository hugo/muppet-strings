"""
Loads the output of puppet strings into puppet items.

This is mostly to aid in debugging.
"""

from . import PuppetStrings


def __main() -> None:
    import json
    import argparse
    from pprint import PrettyPrinter
    pp = PrettyPrinter(compact=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('--pretty', action='store_true')
    parser.add_argument('source', type=argparse.FileType('r'))
    args = parser.parse_args()

    data = PuppetStrings.from_json(json.load(args.source))
    if args.pretty:
        pp.pprint(data)
    else:
        print(data)


if __name__ == '__main__':
    __main()
