"""
Setup for automatic deserialization into dataclasses.

This automatically deserializes dictionaries like those returned from
json objects, into actual python classes, using the dataclass
typelevel introspection.

Each key in the dataclass should either be a primitive type
:py:data:`primitive_types`, a class extending from
:py:class:`Deserializable`, or an optional, list, or dictionary with
one of the above as it's type.

See :py:meth:`Deserializable.handle` for details about customizing deserialization.

.. code-block:: python
   :caption: Sample usage.

    @dataclass
    class OtherItem(Deserializable):
        x: str

   @dataclass
   class MyItem(Deserializable):
       x: str
       y: Optional[int] = None
       xs: OtherItem = None

   MyItem.from_json({
       'x': 'Hello',
       'xs': {
           'x': 'World',
       }
   })

.. code-block::
   :caption: result of the above:

   MyItem(x='Hello',
          y=None,
          xs=OtherItem(x='World'))


.. todo::

    Forwards declarations in type fields sometimes work, and sometimes
    not. I don't know what causes it. Use with caution.
"""

import typing
from typing import (
    Any,
    Optional,
    TypeVar,
    final,
)
from types import GenericAlias, NoneType
import dataclasses
from dataclasses import dataclass
import logging

logger = logging.getLogger(__name__)


T = TypeVar('T', bound='Deserializable')


primitive_types = {str, int, bool}
"""Types which are directly allowed as children."""


def check_optional(field: dataclasses.Field[Any]) -> Optional[Any]:
    """
    Check if field represents an optional field.

    :returns:
        ``None`` if the object isn't an Optional, and the contained
        type if it is an Optional.
    """
    # Documentation I have seem indicates that the correct way to
    # check for unions would be
    # ``isinstance(field.type, types.UnionType)``,
    # but that always returns False for me.
    if typing.get_origin(field.type) == typing.Union \
            and len(typing.get_args(field.type)) == 2:
        args = typing.get_args(field.type)
        if args[0] == NoneType:
            return args[1]
        if args[1] == NoneType:
            return args[0]
        else:
            return None
    else:
        return None


@dataclass
class Deserializable:
    """
    Something which can be deserialized from a JSON object.

    This class shouldn't be instansiated directly, but instead
    subclassed to allow deserialization.
    """

    @final
    @classmethod
    def fields_dict(cls) -> dict[str, dataclasses.Field[Any]]:
        """Return "this" dataclass fields as a dictionary from name to field."""
        return {f.name: f for f in dataclasses.fields(cls)}

    @final
    @classmethod
    def from_json(cls: type[T], d: dict[str, Any]) -> T:
        """
        Load instance of "this" class from given dictionary.

        The name ``from_json`` is thereby slightly missleading.

        :param cls:

        :param d:
            Dictionary which should be deseriablized.

            If the entry is another Deserializable (or lists of
            Deserializable, or dictionaries with Deserialiazable
            values) then ``from_json`` is called recursively.

            Other objects are returned verbatim.
        """
        # if (extra := d.keys() - set(x.name for x in dataclasses.fields(cls))) != set():

        params = {}
        fields = cls.fields_dict()  # allowed keys

        # For each present key
        for k, v in d.items():
            if k in fields:
                try:
                    params[k] = cls.handle(fields[k], k, v)
                except TypeError as e:
                    logger.error('An error occurred while handling class [%s]',
                                 cls.__name__,
                                 exc_info=e)
                    raise e
            else:
                msg = "Attempting to set non-existant field [%(field)s] on class [%(cls)s] to value %(value)a"  # noqa: E501
                logger.warning(msg, {
                    'field': k,
                    'cls': cls.__name__,
                    'value': v
                })

        try:
            return cls(**params)
        except TypeError as e:
            msg = 'Failed constructing object in from_json. class=%(cls)s, params=%(params)s'
            logger.error(msg, {
                'cls': cls.__name__,
                'params': params,
            }, exc_info=e)

            raise e

    @final
    @classmethod
    def handle(cls, field: dataclasses.Field[Any], key: str, value: Any) -> Any:
        """
        Deserialize given field.

        The given value is deserialized according to the type of ``field``.
        First, any ``Optional`` wrapping is removed (e.g. ``Optional[T] → T``).

        Then, one of the following steps are taken:

        if the class has a method called ``handle_${key}``:
            Call that method with the value, propagating the result.

        if the type indicates another subclass of ``Deserializable``:
            Recurse, returing that deserialized value.

        if the type indicates a list:
            If it's either a primitive type, or another subclass of
            ``Deserializable``, then each element in deserialized, and
            collected into a list which is then returned.

        if the type indicates a dictionary:
            The key type is ignored, and propagated directly, while
            the value type follows the same rules as for lists (see
            above).

        if the type is a primitive type (:py:data:`primitive_types`):
            Return the value directoly

        otherwise:
            Also return the value directly, but log a warning about
            unhandled complex data.

        :param field:
            Specifies the expected type of ``value``, and is used in
            determining how to proceed.
        :param key:
            Key of the value we are deserializing. Is only used for
            the manual method dispatch, and error messages.
        :param value:
            The value to deserialize.
        :raises TypeError:
            If there's a mismatch between the type and value a
            TypeError will most likely be raised. These should
            preferably be handled somehow.
        """
        # Unwrap optional types. This is always ok, since handle is
        # only called when we have a value, so the None case is
        # already discarded.
        if unwrapped := check_optional(field):
            ttt = unwrapped
        else:
            ttt = field.type

        # Check for explicit handle_{key} method in current class
        if callable(method := getattr(cls, f'handle_{key}', None)):
            return method(value)

        # If the target class is Deserializable
        elif isinstance(ttt, type) and issubclass(ttt, Deserializable):
            return ttt.from_json(value)

        # If the target is a list of Deserializable
        elif isinstance(ttt, GenericAlias) \
                and typing.get_origin(ttt) == list:
            if issubclass(typing.get_args(ttt)[0], Deserializable):
                typ = typing.get_args(ttt)[0]
                return [typ.from_json(x) for x in value]
            elif typing.get_args(ttt)[0] in primitive_types:
                return value

        # If the target is a dictionary with Deserializable keys
        elif isinstance(ttt, GenericAlias) \
                and typing.get_origin(ttt) == dict:
            if issubclass(typing.get_args(ttt)[1], Deserializable):
                typ = typing.get_args(ttt)[1]
                return {k: typ.from_json(v) for (k, v) in value.items()}
            elif typing.get_args(ttt)[1] in primitive_types:
                return value

        # If the target is a primitive type
        elif type(value) in primitive_types:
            return value

        # if the target is something else weird
        else:
            msg = "setting complex variable: %(cls)s[%(field)a] = %(value)a as %(type)s"
            args = {
                'cls': cls.__name__,
                'field': key,
                'value': value,
                'type': type(value).__name__,
            }
            logger.warning(msg, args)

            return value
