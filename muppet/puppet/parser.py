"""
A Python wrapper around `puppet parser dump`.

puppet_parser_raw simply runs `puppet parser dump --format json` on
the given output. `puppet_parser` also reflows the output into
something managable.
"""

import subprocess
import json

from typing import Any, TypeAlias, Union
from ..cache import Cache
import logging
from collections import OrderedDict


logger = logging.getLogger(__name__)


# TODO cache path
cache = Cache('/home/hugo/.cache/muppet-strings')


def tagged_list_to_dict(lst: list[Any]) -> OrderedDict[Any, Any]:
    """
    Turn a tagged list into a dictionary.

    A tagged list in this context is a list where every even value
    (zero-indexed) is a key, and every odd value is a value.

    .. sourcecode:: python

        >>> tagged_list_to_dict(['a', 1, 'b', 2])
        {'a': 1, 'b': 2}
    """
    return OrderedDict((lst[i], lst[i+1])
                       for i in range(0, len(lst), 2))


def traverse(tree: Any) -> Any:
    """
    Reflow a puppet parse output tree.

    Reworks a tree returned by `puppet parser dump --format json` into
    something amnagable by python.

    - '^' tags are recursed as blocks of forms
    - '#' are expanded as key-value pairs, turning into python
      dictionaries
    - lists are recursed through
    - strings (and other stuff) is kept verbatim
    """
    if type(tree) is str:
        # print(tree)
        return tree
    elif type(tree) is dict:
        # `x in tree` pattern since there may be empty lists (which
        # are "False")
        if '#' in tree:
            return OrderedDict((key, traverse(value))
                               for (key, value)
                               in tagged_list_to_dict(tree['#']).items())
        elif '^' in tree:
            return [traverse(subtree) for subtree in tree['^']]
        else:
            raise Exception('Unexpected dictionary', tree)
    elif type(tree) is list:
        return [traverse(branch) for branch in tree]
    else:
        return tree


@cache.memoize('puppet-parser')
def puppet_parser_raw(code: bytes) -> bytes:
    """Parse the given puppetstring into a json representation."""
    cmd = subprocess.run('puppet parser dump --format json'.split(' '),
                         input=code,
                         check=True,
                         capture_output=True)
    return cmd.stdout


JsonPrimitive: TypeAlias = Union[str, int, float, bool, None]
JSON: TypeAlias = JsonPrimitive | dict[str, 'JSON'] | list['JSON']


def puppet_parser(code: str) -> JSON:
    """Parse the given puppet string, and reflow it."""
    data = traverse(json.loads(puppet_parser_raw(code.encode('UTF-8'))))

    # The two compound cases technically needs to recursively check
    # the type, but it should be fine.
    #
    # isinstance(data, JsonPrimitive) would be better here, and it
    # works in runtime. But mypy fails on it...
    if data is None:
        return data
    elif isinstance(data, str):
        return data
    elif isinstance(data, int):
        return data
    elif isinstance(data, float):
        return data
    elif isinstance(data, bool):
        return data
    elif isinstance(data, dict):
        return data
    elif isinstance(data, list):
        return data
    else:
        raise ValueError('Expected well formed tree, got %s', data)
