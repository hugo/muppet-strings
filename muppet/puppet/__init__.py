"""
Various wrappers around different puppet functionality.

strings
    wraps ``puppet strings``, and maps it to python types.

parser
    Wraps ``puppet parser``.
"""
