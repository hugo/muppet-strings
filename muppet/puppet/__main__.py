"""Extra executable for testing the various parser steps."""

from .parser import puppet_parser
from .ast import build_ast
from .format.text import TextFormatter
from .format import serialize
import json
import argparse
import pprint
import sys
from typing import Any


def __main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('mode',
                        choices=['raw', 'parser', 'ast', 'serialize'],
                        help='Mode of operation')

    parser.add_argument('file',
                        type=argparse.FileType('r'),
                        help='Puppet file to parse')
    parser.add_argument('--format',
                        choices=['json', 'python'],
                        action='store',
                        default='json',
                        help='Format of the output')
    parser.add_argument('--pretty',
                        nargs='?',
                        action='store',
                        type=int,
                        help='Prettify the output, optionally specifying indent width')

    args = parser.parse_args()

    def output(thingy: Any) -> None:
        match args.format:
            case 'json':
                json.dump(thingy, sys.stdout, indent=args.pretty)
                print()
            case 'python':
                if indent := args.pretty:
                    pprint.pprint(thingy, indent=indent)
                else:
                    print(thingy)

    data = args.file.read()
    if args.mode == 'raw':
        output(data)
        return

    # print("raw data:", data)
    result = puppet_parser(data)
    if args.mode == 'parser':
        output(result)
        return

    ast = build_ast(result)
    if args.mode == 'ast':
        output(ast)
        return

    reserialized = serialize(ast, TextFormatter)
    if args.mode == 'serialize':
        print(reserialized)
        return

    print('No mode given')


if __name__ == '__main__':
    __main()
