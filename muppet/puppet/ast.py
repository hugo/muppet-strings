"""The Puppet AST, in Python."""

from dataclasses import dataclass, field
import logging

from typing import (
    Any,
    Literal,
    Optional,
)
# from abc import abstractmethod


logger = logging.getLogger(__name__)


# --------------------------------------------------


@dataclass
class HashEntry:
    """An entry in a hash table."""

    k: 'PuppetAST'
    v: 'PuppetAST'


@dataclass
class PuppetDeclarationParameter:
    """
    A parameter to a class, definition, or function.

    .. code-block:: puppet

        class A (
            type k = v,
        ) {
        }
    """

    k: str
    v: Optional['PuppetAST'] = None
    type: Optional['PuppetAST'] = None


# --------------------------------------------------

@dataclass(kw_only=True)
class PuppetAST:
    """Base for puppet item."""


@dataclass
class PuppetLiteral(PuppetAST):
    """
    A self quoting value.

    e.g. ``true``, ``undef``, ...
    """

    literal: str


@dataclass
class PuppetAccess(PuppetAST):
    """
    Array access and similar.

    .. code-block:: puppet

        how[args]
    """

    how: PuppetAST
    args: list[PuppetAST]


@dataclass
class PuppetBinaryOperator(PuppetAST):
    """An operator with two values."""

    op: str
    lhs: PuppetAST
    rhs: PuppetAST


@dataclass
class PuppetUnaryOperator(PuppetAST):
    """A prefix operator."""

    op: str
    x: PuppetAST


@dataclass
class PuppetArray(PuppetAST):
    """An array of values."""

    items: list[PuppetAST]


@dataclass
class PuppetCall(PuppetAST):
    """
    A function call.

    .. highlight: puppet

        func(args)
    """

    func: PuppetAST
    args: list[PuppetAST]
    block: Optional[PuppetAST] = None


@dataclass
class PuppetCallMethod(PuppetAST):
    """A method call."""

    func: PuppetAST
    args: list[PuppetAST]
    block: Optional[PuppetAST] = None


@dataclass
class PuppetCase(PuppetAST):
    """A case "statement"."""

    test: PuppetAST
    cases: list[tuple[list[PuppetAST], list[PuppetAST]]]


@dataclass
class PuppetInstanciationParameter(PuppetAST):
    """
    Key-value pair used when instanciating resources.

    Also used with resource defaults and resoruce overrides.

    ``+>`` arrow is only valid on overrides,
    `See <https://www.puppet.com/docs/puppet/7/lang_resources.html>`.

    .. code-block:: puppet

        file { 'hello':
            k => v,
        }
    """

    k: str
    v: PuppetAST
    arrow: Literal['=>'] | Literal['+>'] = '=>'


@dataclass
class PuppetClass(PuppetAST):
    """A puppet class declaration."""

    name: str
    params: Optional[list[PuppetDeclarationParameter]] = None
    body: list[PuppetAST] = field(default_factory=list)
    parent: Optional[str] = None


@dataclass
class PuppetConcat(PuppetAST):
    """A string with interpolated values."""

    fragments: list[PuppetAST]


@dataclass
class PuppetCollect(PuppetAST):
    """
    Resource collectors.

    These should be followed by a query (either exported or virtual).
    """

    type: PuppetAST
    query: PuppetAST
    ops: list[PuppetInstanciationParameter] = field(default_factory=list)


@dataclass
class PuppetIfChain(PuppetAST):
    """
    Puppet if expressions.

    .. code-block:: puppet

        if a {
            1
        } elsif b {
            2
        } else {
            3
        }

    .. code-block:: python

        [('a', [1]),
         ('b', [2]),
         ('else', [3])]
    """

    clauses: list[tuple[PuppetAST | Literal['else'], list[PuppetAST]]]


@dataclass
class PuppetUnless(PuppetAST):
    """A puppet unless expression."""

    condition: PuppetAST
    consequent: list[PuppetAST]
    alternative: list[PuppetAST] = field(default_factory=list)


@dataclass
class PuppetKeyword(PuppetAST):
    """
    A reserved word in the puppet language.

    This class is seldom instanciated, since most keywords are
    embedded in the other forms.
    """

    name: str


@dataclass
class PuppetExportedQuery(PuppetAST):
    """
    An exported query.

    .. code-block:: puppet

        <<| filter |>>
    """

    filter: Optional[PuppetAST] = None


@dataclass
class PuppetVirtualQuery(PuppetAST):
    """
    A virtual query.

    .. code-block:: puppet

        <| q |>
    """

    q: Optional[PuppetAST] = None


@dataclass
class PuppetFunction(PuppetAST):
    """Declaration of a Puppet function."""

    name: str
    params: Optional[list[PuppetDeclarationParameter]] = None
    returns: Optional[PuppetAST] = None
    body: list[PuppetAST] = field(default_factory=list)


@dataclass
class PuppetHash(PuppetAST):
    """A puppet dictionary."""

    entries: list[HashEntry] = field(default_factory=list)


@dataclass
class PuppetHeredoc(PuppetAST):
    """A puppet heredoc."""

    fragments: list[PuppetAST]
    syntax: Optional[str] = None


@dataclass
class PuppetLiteralHeredoc(PuppetAST):
    """A puppet heredoc without any interpolation."""

    content: str
    syntax: Optional[str] = None


@dataclass
class PuppetVar(PuppetAST):
    """A puppet variable."""

    name: str


@dataclass
class PuppetLambda(PuppetAST):
    """A puppet lambda."""

    params: list[PuppetDeclarationParameter]
    body: list[PuppetAST]


@dataclass
class PuppetParenthesis(PuppetAST):
    """Forms surrounded by parethesis."""

    form: PuppetAST


@dataclass
class PuppetQn(PuppetAST):
    """Qn."""

    name: str


@dataclass
class PuppetQr(PuppetAST):
    """Qr."""

    name: str


@dataclass
class PuppetRegex(PuppetAST):
    """A regex literal."""

    s: str


@dataclass
class PuppetResource(PuppetAST):
    """Instansiation of one (or more) puppet resources."""

    type: PuppetAST
    bodies: list[tuple[PuppetAST, list[PuppetInstanciationParameter]]]
    # Marks if the resource is virtual or exported.
    # A single '@' means virtual, two '@' ('@@') means exported.
    form: Optional[str] = None


@dataclass
class PuppetNop(PuppetAST):
    """A no-op."""

    def serialize(self, indent: int, **_: Any) -> str:  # noqa: D102
        return ''


@dataclass
class PuppetDefine(PuppetAST):
    """A puppet resource declaration."""

    name: str
    params: Optional[list[PuppetDeclarationParameter]] = None
    body: list[PuppetAST] = field(default_factory=list)


@dataclass
class PuppetString(PuppetAST):
    """A puppet string literal."""

    s: str


@dataclass
class PuppetNumber(PuppetAST):
    """A puppet numeric literal."""

    x: int | float
    radix: Optional[int] = None


@dataclass
class PuppetInvoke(PuppetAST):
    """
    A puppet function invocation.

    This is at least used when including classes:

    .. code-block:: puppet

        include ::example

    Where func is ``include``, and args is ``[::example]``
    """

    func: PuppetAST
    args: list[PuppetAST]
    block: Optional[PuppetAST] = None


@dataclass
class PuppetResourceDefaults(PuppetAST):
    """
    Default values for a resource.

    .. code-block:: puppet

        File {
            x => 10,
        }
    """

    type: PuppetAST
    ops: list[PuppetInstanciationParameter]


@dataclass
class PuppetResourceOverride(PuppetAST):
    """
    A resource override.

    The resource attribute will most likely be a PuppetAccess object.

    .. code-block:: python

        PuppetAccess(how=PuppetQr(name='File'), args=[PuppetString(s='name')])

    .. code-block:: puppet

        File["name"] {
            x => 10,
        }
    """

    resource: PuppetAST
    ops: list[PuppetInstanciationParameter]


@dataclass
class PuppetDeclaration(PuppetAST):
    """A stand-alone variable declaration."""

    k: PuppetAST  # PuppetVar
    v: PuppetAST


@dataclass
class PuppetSelector(PuppetAST):
    """
    A puppet selector.

    .. code-block:: puppet

        resource ? {
            case_match => case_body,
        }
    """

    resource: PuppetAST
    cases: list[tuple[PuppetAST, PuppetAST]]


@dataclass
class PuppetBlock(PuppetAST):
    """Used if multiple top level items exists."""

    entries: list[PuppetAST]


@dataclass
class PuppetNode(PuppetAST):
    """
    A node declaration.

    Used with an ENC is not in use,

    .. code-block:: puppet

        node 'host.example.com' {
            include ::profiles::example
        }
    """

    matches: list[PuppetAST]
    body: list[PuppetAST]


@dataclass
class PuppetTypeAlias(PuppetAST):
    """A type alias."""

    name: str
    implementation: PuppetAST


@dataclass
class PuppetParseError(PuppetAST):
    """Anything we don't know how to handle."""

    x: Any = None

    def serialize(self, indent: int, **_: Any) -> str:  # noqa: D102
        return f'INVALID INPUT: {repr(self.x)}'

# ----------------------------------------------------------------------


def parse_hash_entry(data: list[Any]) -> HashEntry:
    """Parse a single hash entry."""
    match data:
        case [_, key, value]:
            return HashEntry(k=build_ast(key), v=build_ast(value))
        case _:
            raise ValueError(f'Not a hash entry {data}')


def parse_puppet_declaration_params(data: dict[str, dict[str, Any]]) \
        -> list[PuppetDeclarationParameter]:
    """Parse a complete set of parameters."""
    parameters = []

    for name, definition in data.items():
        type: Optional[PuppetAST] = None
        value: Optional[PuppetAST] = None
        if t := definition.get('type'):
            type = build_ast(t)
        if 'value' in definition:
            value = build_ast(definition['value'])

        parameters.append(PuppetDeclarationParameter(k=name, v=value, type=type))

    return parameters


def parse_puppet_instanciation_param(data: list[Any]) -> PuppetInstanciationParameter:
    """Parse a single parameter."""
    match data:
        case [arrow, key, value]:
            return PuppetInstanciationParameter(
                k=key, v=build_ast(value), arrow=arrow)
        case ['splat-hash', value]:
            return PuppetInstanciationParameter('*', build_ast(value))
        case _:
            raise ValueError(f'Not an instanciation parameter {data}')


# ----------------------------------------------------------------------


def build_ast(form: Any) -> PuppetAST:
    """
    Print everything from a puppet parse tree.

    :param from:
        A puppet AST.
    :param indent:
        How many levels deep in indentation the code is.
        Will get multiplied by the indentation width.
    """
    # items: list[Markup]
    match form:
        case None:  return PuppetLiteral('undef')  # noqa: E272
        case True:  return PuppetLiteral('true')  # noqa: E272
        case False: return PuppetLiteral('false')
        case ['default']: return PuppetKeyword('default')

        case ['access', how, *args]:
            return PuppetAccess(how=build_ast(how),
                                args=[build_ast(arg) for arg in args],
                                )

        case [('and' | 'or' | 'in' | '!=' | '+' | '-' | '*' | '%'
                | '<<' | '>>' | '>=' | '<=' | '>' | '<' | '/' | '=='
                | '=~' | '!~' | '~>' | '->' | '.') as op,
              a, b]:
            return PuppetBinaryOperator(
                    op=op,
                    lhs=build_ast(a),
                    rhs=build_ast(b),
                    )

        case [('!' | '-') as op, x]:
            return PuppetUnaryOperator(op=op, x=build_ast(x))

        case ['array', *items]:
            return PuppetArray([build_ast(x) for x in items])

        case ['call', {'functor': func, 'args': args, 'block': block}]:
            return PuppetCall(
                build_ast(func),
                [build_ast(x) for x in args],
                build_ast(block))

        case ['call', {'functor': func, 'args': args}]:
            return PuppetCall(
                build_ast(func),
                [build_ast(x) for x in args])

        case ['call-method', {'functor': func, 'args': args, 'block': block}]:
            return PuppetCallMethod(func=build_ast(func),
                                    args=[build_ast(x) for x in args],
                                    block=build_ast(block))

        case ['call-method', {'functor': func, 'args': args}]:
            return PuppetCallMethod(func=build_ast(func),
                                    args=[build_ast(x) for x in args])

        case ['case', test, forms]:
            cases = []
            for form in forms:
                cases.append((
                    [build_ast(x) for x in form['when']],
                    [build_ast(x) for x in form['then']]))

            return PuppetCase(build_ast(test), cases)

        case [('class' | 'define' | 'function') as what, {'name': name, **rest}]:
            cls = {
                'class': PuppetClass,
                'define': PuppetDefine,
                'function': PuppetFunction,
            }[what]

            args = {'name': name}

            if p := rest.get('params'):
                args['params'] = parse_puppet_declaration_params(p)

            if b := rest.get('body'):
                args['body'] = [build_ast(x) for x in b]

            if p := rest.get('parent'):
                args['parent'] = p

            # This is only valid for 'function', and simply will
            # never be true for the other cases.
            if r := rest.get('returns'):
                args['returns'] = build_ast(r)

            return cls(**args)

        case ['concat', *parts]:
            return PuppetConcat([build_ast(p) for p in parts])

        case ['collect', {'type': t, 'query': q, 'ops': ops}]:
            return PuppetCollect(build_ast(t),
                                 build_ast(q),
                                 [parse_puppet_instanciation_param(x) for x in ops])

        case ['collect', {'type': t, 'query': q}]:
            return PuppetCollect(build_ast(t),
                                 build_ast(q))

        case ['exported-query']:
            return PuppetExportedQuery()

        case ['exported-query', arg]:
            return PuppetExportedQuery(build_ast(arg))

        case ['hash']:
            return PuppetHash()
        case ['hash', *hash]:
            return PuppetHash([parse_hash_entry(e) for e in hash])

        # Parts can NEVER be empty, since that case wouldn't generate
        # a concat element, but a "plain" text element
        case ['heredoc', {'syntax': syntax, 'text': ['concat', *parts]}]:
            return PuppetHeredoc([build_ast(p) for p in parts], syntax=syntax)

        case ['heredoc', {'text': ['concat', *parts]}]:
            return PuppetHeredoc([build_ast(p) for p in parts])

        case ['heredoc', {'syntax': syntax, 'text': text}]:
            return PuppetLiteralHeredoc(text, syntax=syntax)

        case ['heredoc', {'text': text}]:
            return PuppetLiteralHeredoc(text)

        # Non-literal part of a string or heredoc with interpolation
        case ['str', form]:
            return build_ast(form)

        case ['if', {'test': test, **rest}]:
            clauses = []

            clauses.append((build_ast(test),
                            [build_ast(x) for x in rest.get('then', [])]))

            if els := rest.get('else'):
                match [build_ast(x) for x in els]:
                    case [PuppetIfChain(cls)]:
                        clauses += cls
                    case xs:
                        clauses.append(('else', xs))

            return PuppetIfChain(clauses)

        case ['unless', {'test': test, 'then': forms, 'else': else_}]:
            return PuppetUnless(build_ast(test),
                                [build_ast(x) for x in forms],
                                [build_ast(x) for x in else_])

        case ['unless', {'test': test, 'then': forms}]:
            return PuppetUnless(build_ast(test),
                                [build_ast(x) for x in forms])

        case ['invoke', {'functor': func, 'args': args, 'block': block}]:
            return PuppetInvoke(build_ast(func),
                                [build_ast(x) for x in args],
                                build_ast(block))

        case ['invoke', {'functor': func, 'args': args}]:
            return PuppetInvoke(build_ast(func), [build_ast(x) for x in args])

        case ['nop']: return PuppetNop()

        case ['lambda', {'params': params, 'body': body}]:
            return PuppetLambda(params=parse_puppet_declaration_params(params),
                                body=[build_ast(x) for x in body])

        case ['lambda', {'body': body}]:
            return PuppetLambda([], [build_ast(x) for x in body])

        case ['paren', form]:
            return PuppetParenthesis(build_ast(form))

        # Qualified name and Qualified resource?
        case ['qn', x]: return PuppetQn(x)
        case ['qr', x]: return PuppetQr(x)

        case ['var', x]: return PuppetVar(x)

        case ['regexp', s]: return PuppetRegex(s)

        case ['resource', {'type': t, 'bodies': bodies, 'form': form}]:
            assert form in {'exported', 'virtual'}
            return PuppetResource(
                    build_ast(t),
                    [(build_ast(body['title']),
                      [parse_puppet_instanciation_param(x) for x in body['ops']])
                     for body in bodies],
                    form)

        case ['resource', {'type': t, 'bodies': bodies}]:
            return PuppetResource(
                    build_ast(t),
                    [(build_ast(body['title']),
                      [parse_puppet_instanciation_param(x) for x in body['ops']])
                     for body in bodies])

        case ['resource-defaults', {'type': t, 'ops': ops}]:
            return PuppetResourceDefaults(
                    build_ast(t),
                    [parse_puppet_instanciation_param(x) for x in ops])

        case ['resource-override', {'resources': resources, 'ops': ops}]:
            return PuppetResourceOverride(
                    build_ast(resources),
                    [parse_puppet_instanciation_param(x) for x in ops])

        case ['virtual-query']: return PuppetVirtualQuery()
        case ['virtual-query', q]: return PuppetVirtualQuery(build_ast(q))

        case ['=', field, value]:
            return PuppetDeclaration(k=build_ast(field), v=build_ast(value))

        case ['?', condition, cases]:
            cases_ = []
            for case in cases:
                match case:
                    case [_, lhs, rhs]:
                        cases_.append((build_ast(lhs),
                                       build_ast(rhs)))
                    case _:
                        raise ValueError(f"Unexepcted '?' form: {case}")
            return PuppetSelector(build_ast(condition), cases_)

        case ['block', *items]:
            return PuppetBlock([build_ast(x) for x in items])

        case ['node', {'matches': matches, 'body': body}]:
            return PuppetNode([build_ast(x) for x in matches],
                              [build_ast(x) for x in body])

        case ['type-alias', str(name), implementation]:
            return PuppetTypeAlias(name, build_ast(implementation))

        case str(s):
            return PuppetString(s)

        case ['int', {'radix': radix, 'value': value}]:
            return PuppetNumber(value, radix)

        case int(x): return PuppetNumber(x)
        case float(x): return PuppetNumber(x)

        case default:
            logger.warning("Unhandled item: %a", default)
            return PuppetParseError(default)


def generate_template() -> None:
    """
    Output code for a formater class.

    Each formatter needs to implement a rather large number of
    methods, and keeping track of them all is hard. This generates a
    ready-to-use template.

    .. code-block:: sh

        python -m muppet.puppet.ast > muppet/puppet/format/NAME.py
    """

    def pyind(level: int) -> str:
        """Indent string for python code."""
        return ' ' * 4 * level

    subclasses = sorted(subclass.__name__ for subclass in PuppetAST.__subclasses__())

    def tokenize_class(s: str) -> list[str]:
        """Split a camel or pascal cased string into words."""
        out: list[str] = []
        current: str = ''
        for c in s:
            if c.isupper():
                if current.isupper():
                    current += c
                else:
                    out.append(current)
                    current = c
            else:
                current += c
        out.append(current)

        if out[0] == '':
            out = out[1:]

        return out

    def setup_override() -> None:
        print('''
from typing import (
    TypeVar,
    Callable,
)
import sys


F = TypeVar('F', bound=Callable[..., object])


if sys.version_info >= (3, 12):
    from typing import override
else:
    def override(f: F) -> F:
        """
        Return function unchanged.

        Placeholder @override annotator if the actual annotation isn't
        implemented in the current python version.
        """
        return f
'''.lstrip())

    print('''"""
__

TODOFormatter
TODO_RETURN_TYPE
"""''')

    print()
    print('import logging')
    print('from .base import Serializer')
    print('from muppet.puppet.ast import (')
    for subclass in subclasses:
        print(pyind(1) + subclass + ',')
    print(')')

    setup_override()

    print()
    print()
    print('logger = logging.getLogger(__name__)')
    print()
    print()

    print('class TODOFormatter(Serializer[TODO_RETURN_TYPE]):')
    print(pyind(1) + '"""TODO document me!!!"""')
    for subclass in subclasses:
        func_name = '_'.join(tokenize_class(subclass)).lower()
        print()
        print(pyind(1) + '@override')
        print(pyind(1) + '@classmethod')
        decl = f"def _{func_name}(cls, it: {subclass}, indent: int) -> TODO_RETURN_TYPE:"
        print(pyind(1) + decl)


if __name__ == '__main__':
    generate_template()
