"""Base class for serializing AST's into something useful."""

from typing import (
    TypeVar,
    Generic,
    final,
)
import logging

from muppet.puppet.ast import (
    PuppetAST,

    PuppetLiteral, PuppetAccess, PuppetBinaryOperator,
    PuppetUnaryOperator, PuppetArray, PuppetCallMethod,
    PuppetCase, PuppetDeclarationParameter,
    PuppetInstanciationParameter, PuppetClass, PuppetConcat,
    PuppetCollect, PuppetIfChain, PuppetUnless, PuppetKeyword,
    PuppetExportedQuery, PuppetVirtualQuery, PuppetFunction,
    PuppetHash, PuppetHeredoc, PuppetLiteralHeredoc, PuppetVar,
    PuppetLambda,  PuppetQn, PuppetQr, PuppetRegex,
    PuppetResource, PuppetDefine, PuppetString,
    PuppetNumber, PuppetInvoke, PuppetResourceDefaults,
    PuppetResourceOverride, PuppetDeclaration, PuppetSelector,
    PuppetBlock, PuppetNode, PuppetTypeAlias,
    PuppetCall, PuppetParenthesis, PuppetNop,

    # HashEntry,
    # PuppetParseError,
)


T = TypeVar('T')
logger = logging.getLogger(__name__)


# TODO flag everything as @abstractmethod


class Serializer(Generic[T]):
    """
    Base class for serialization.

    the ``serialize`` method dispatches depending on the type of the argument,
    and should be called recursively when needed.

    All other methods implement the actual serialization, and MUST be extended
    by each instance.
    """

    def __raise(self, proc: str) -> T:
        msg = f"{proc} must be implemented by subclass {self.__class__.__name__}"
        raise NotImplementedError(msg)

    def _puppet_literal(self, it: PuppetLiteral) -> T:
        return self.__raise("puppet_literal")

    def _puppet_access(self, it: PuppetAccess) -> T:
        return self.__raise("puppet_access")

    def _puppet_binary_operator(self, it: PuppetBinaryOperator) -> T:
        return self.__raise("puppet_binary_operator")

    def _puppet_unary_operator(self, it: PuppetUnaryOperator) -> T:
        return self.__raise("puppet_unary_operator")

    def _puppet_array(self, it: PuppetArray) -> T:
        return self.__raise("puppet_array")

    def _puppet_call(self, it: PuppetCall) -> T:
        return self.__raise("puppet_call")

    def _puppet_call_method(self, it: PuppetCallMethod) -> T:
        return self.__raise("puppet_call_method")

    def _puppet_case(self, it: PuppetCase) -> T:
        return self.__raise("puppet_case")

    def _puppet_declaration_parameter(self, it: PuppetDeclarationParameter) -> T:
        return self.__raise("puppet_declaration_parameter")

    def _puppet_instanciation_parameter(self, it: PuppetInstanciationParameter) -> T:
        return self.__raise("puppet_instanciation_parameter")

    def _puppet_class(self, it: PuppetClass) -> T:
        return self.__raise("puppet_class")

    def _puppet_concat(self, it: PuppetConcat) -> T:
        return self.__raise("puppet_concat")

    def _puppet_collect(self, it: PuppetCollect) -> T:
        return self.__raise("puppet_collect")

    def _puppet_if_chain(self, it: PuppetIfChain) -> T:
        return self.__raise("puppet_if_chain")

    def _puppet_unless(self, it: PuppetUnless) -> T:
        return self.__raise("puppet_unless")

    def _puppet_keyword(self, it: PuppetKeyword) -> T:
        return self.__raise("puppet_keyword")

    def _puppet_exported_query(self, it: PuppetExportedQuery) -> T:
        return self.__raise("puppet_exported_query")

    def _puppet_virtual_query(self, it: PuppetVirtualQuery) -> T:
        return self.__raise("puppet_virtual_query")

    def _puppet_function(self, it: PuppetFunction) -> T:
        return self.__raise("puppet_function")

    def _puppet_hash(self, it: PuppetHash) -> T:
        return self.__raise("puppet_hash")

    def _puppet_heredoc(self, it: PuppetHeredoc) -> T:
        return self.__raise("puppet_heredoc")

    def _puppet_literal_heredoc(self, it: PuppetLiteralHeredoc) -> T:
        return self.__raise("puppet_literal_heredoc")

    def _puppet_var(self, it: PuppetVar) -> T:
        return self.__raise("puppet_var")

    def _puppet_lambda(self, it: PuppetLambda) -> T:
        return self.__raise("puppet_lambda")

    def _puppet_qn(self, it: PuppetQn) -> T:
        return self.__raise("puppet_qn")

    def _puppet_qr(self, it: PuppetQr) -> T:
        return self.__raise("puppet_qr")

    def _puppet_regex(self, it: PuppetRegex) -> T:
        return self.__raise("puppet_regex")

    def _puppet_resource(self, it: PuppetResource) -> T:
        return self.__raise("puppet_resource")

    def _puppet_define(self, it: PuppetDefine) -> T:
        return self.__raise("puppet_define")

    def _puppet_string(self, it: PuppetString) -> T:
        return self.__raise("puppet_string")

    def _puppet_number(self, it: PuppetNumber) -> T:
        return self.__raise("puppet_number")

    def _puppet_invoke(self, it: PuppetInvoke) -> T:
        return self.__raise("puppet_invoke")

    def _puppet_resource_defaults(self, it: PuppetResourceDefaults) -> T:
        return self.__raise("puppet_resource_defaults")

    def _puppet_resource_override(self, it: PuppetResourceOverride) -> T:
        return self.__raise("puppet_resource_override")

    def _puppet_declaration(self, it: PuppetDeclaration) -> T:
        return self.__raise("puppet_declaration")

    def _puppet_selector(self, it: PuppetSelector) -> T:
        return self.__raise("puppet_selector")

    def _puppet_block(self, it: PuppetBlock) -> T:
        return self.__raise("puppet_block")

    def _puppet_node(self, it: PuppetNode) -> T:
        return self.__raise("puppet_node")

    def _puppet_type_alias(self, it: PuppetTypeAlias) -> T:
        return self.__raise("type_alias")

    def _puppet_parenthesis(self, it: PuppetParenthesis) -> T:
        return self.__raise("puppet_parenthesis")

    def _puppet_nop(self, it: PuppetNop) -> T:
        return self.__raise("puppet_nop")

    @final
    def serialize(self, form: PuppetAST) -> T:
        """Dispatch depending on type."""
        # logger.debug("Serializing %s", form)
        match form:
            case PuppetLiteral():
                return self._puppet_literal(form)
            case PuppetAccess():
                return self._puppet_access(form)
            case PuppetBinaryOperator():
                return self._puppet_binary_operator(form)
            case PuppetUnaryOperator():
                return self._puppet_unary_operator(form)
            case PuppetUnaryOperator():
                return self._puppet_unary_operator(form)
            case PuppetArray():
                return self._puppet_array(form)
            case PuppetCall():
                return self._puppet_call(form)
            case PuppetCallMethod():
                return self._puppet_call_method(form)
            case PuppetCase():
                return self._puppet_case(form)
            case PuppetDeclarationParameter():
                return self._puppet_declaration_parameter(form)
            case PuppetInstanciationParameter():
                return self._puppet_instanciation_parameter(form)
            case PuppetClass():
                return self._puppet_class(form)
            case PuppetConcat():
                return self._puppet_concat(form)
            case PuppetCollect():
                return self._puppet_collect(form)
            case PuppetIfChain():
                return self._puppet_if_chain(form)
            case PuppetUnless():
                return self._puppet_unless(form)
            case PuppetKeyword():
                return self._puppet_keyword(form)
            case PuppetExportedQuery():
                return self._puppet_exported_query(form)
            case PuppetVirtualQuery():
                return self._puppet_virtual_query(form)
            case PuppetFunction():
                return self._puppet_function(form)
            case PuppetHash():
                return self._puppet_hash(form)
            case PuppetHeredoc():
                return self._puppet_heredoc(form)
            case PuppetLiteralHeredoc():
                return self._puppet_literal_heredoc(form)
            case PuppetVar():
                return self._puppet_var(form)
            case PuppetLambda():
                return self._puppet_lambda(form)
            case PuppetQn():
                return self._puppet_qn(form)
            case PuppetQr():
                return self._puppet_qr(form)
            case PuppetRegex():
                return self._puppet_regex(form)
            case PuppetResource():
                return self._puppet_resource(form)
            case PuppetDefine():
                return self._puppet_define(form)
            case PuppetString():
                return self._puppet_string(form)
            case PuppetNumber():
                return self._puppet_number(form)
            case PuppetInvoke():
                return self._puppet_invoke(form)
            case PuppetResourceDefaults():
                return self._puppet_resource_defaults(form)
            case PuppetResourceOverride():
                return self._puppet_resource_override(form)
            case PuppetDeclaration():
                return self._puppet_declaration(form)
            case PuppetSelector():
                return self._puppet_selector(form)
            case PuppetBlock():
                return self._puppet_block(form)
            case PuppetNode():
                return self._puppet_node(form)
            case PuppetParenthesis():
                return self._puppet_parenthesis(form)
            case PuppetTypeAlias():
                return self._puppet_type_alias(form)
            case PuppetNop():
                return self._puppet_nop(form)
            case _:
                logger.warn("Unexpected form: %s", form)
                raise ValueError(f'Unexpected: {form}')
