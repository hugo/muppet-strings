"""
AST serializer returning source code.

Can be used as a "bad" pretty printer (bad, since comments are discarded).
"""

import re
import uuid
import logging
from .base import Serializer
from muppet.puppet.ast import (
    PuppetLiteral, PuppetAccess, PuppetBinaryOperator,
    PuppetUnaryOperator, PuppetArray, PuppetCallMethod,
    PuppetCase, PuppetDeclarationParameter,
    PuppetInstanciationParameter, PuppetClass, PuppetConcat,
    PuppetCollect, PuppetIfChain, PuppetUnless, PuppetKeyword,
    PuppetExportedQuery, PuppetVirtualQuery, PuppetFunction,
    PuppetHash, PuppetHeredoc, PuppetLiteralHeredoc, PuppetVar,
    PuppetLambda,  PuppetQn, PuppetQr, PuppetRegex,
    PuppetResource, PuppetDefine, PuppetString,
    PuppetNumber, PuppetInvoke, PuppetResourceDefaults,
    PuppetResourceOverride, PuppetDeclaration, PuppetSelector,
    PuppetBlock, PuppetNode,
    PuppetCall, PuppetParenthesis, PuppetNop,

    HashEntry,
    # PuppetParseError,
)

from typing import (
    TypeVar,
    Callable,
)


F = TypeVar('F', bound=Callable[..., object])

# TODO replace this decorator with
# from typing import override
# once the target python version is changed to 3.12


def override(f: F) -> F:
    """
    Return function unchanged.

    Placeholder @override annotator if the actual annotation isn't
    implemented in the current python version.
    """
    return f


logger = logging.getLogger(__name__)


def find_heredoc_delimiter(
        source: str,
        options: list[str] = ['EOF', 'EOL', 'STR', 'END'],
        ) -> str:
    """
    Find a suitable heredoc delimiter for the given string.

    Heredoc's are delimited like

    .. Syntax highlighting disabled since the puppet highlighter
       fails on the '@' sign, causing a warning in the build log (and
       highlighting the '@' as an error in the output)

    .. code-block::

        @(EOF)
          Some text
          here
          | EOF

    This looks through the text, and finds a suitable marker (``EOF``
    here) which isn't present in the text. It first tries each given
    option, and then randomizes until it finds one.

    :param source:
        String to search for collisions.
    :param options:
        Prefered delimiters, with descending priority.
    :returns:
        A string like EOF, guaranteed to not be present in the source.
    """
    for option in options:
        if option not in source:
            return option

    while True:
        delim = uuid.uuid4().hex
        if delim not in source:
            return delim


def string_width(s: str, indent: int) -> int:
    """
    Return the width of a rendered puppet expression.

    In a perfect world, this would return the rendered width of an
    expression, as in the total amount of columns between its leftmost
    and rightmost (printed) character. For example, the case below
    should return 4, and any extra highlight to the left would be
    discarded according to ``indent``.

    .. todo::

        The smart width "algorithm" is currently not implemented,
        instead, the length of the string is returned.

    .. code-block:: puppet

        [
          1,
          2,
        ]


    :param s:
        The rendered puppet expression
    :param indent:
        The indentation level which was used when creating the string.
    """
    return len(s)


class TextFormatter(Serializer[str]):
    """AST formatter returning source code."""

    def __init__(self, indent: int = 0):
        self.__indent: int = indent

    def indent(self, change: int) -> 'TextFormatter':
        """Return the current context, with an updated indentation level."""
        return self.__class__(indent=self.__indent + change)

    def ind(self, change: int = 0) -> str:
        """
        Return indentation for current context.

        :param change:
            Extra indentation level to add to this output.
        """
        return (self.__indent + change) * 2 * ' '

    def format_declaration_parameter(
            self,
            param: PuppetDeclarationParameter,
            ) -> str:
        """Format a single declaration parameter."""
        out: str = ''
        if param.type:
            out += f'{self.indent(1).serialize(param.type)} '
        out += f'${param.k}'
        if param.v:
            out += f' = {self.indent(1).serialize(param.v)}'
        return out

    def format_declaration_parameters(
            self,
            lst: list[PuppetDeclarationParameter],
            ) -> str:
        """
        Print declaration parameters.

        This formats the parameters for class, resoruce, and function declarations.
        """
        if not lst:
            return ''

        out = ' (\n'
        for param in lst:
            out += self.ind(1) + self.indent(1).format_declaration_parameter(param) + ',\n'
        out += self.ind() + ')'
        return out

    def serialize_hash_entry(
            self,
            entry: HashEntry,
            ) -> str:
        """Return a hash entry as a string."""
        return f'{self.indent(1).serialize(entry.k)} => {self.indent(2).serialize(entry.v)}'

    @override
    def _puppet_literal(self, it: PuppetLiteral) -> str:
        return it.literal

    @override
    def _puppet_access(self, it: PuppetAccess) -> str:
        args = ', '.join(self.serialize(x) for x in it.args)

        return f'{self.serialize(it.how)}[{args}]'

    @override
    def _puppet_binary_operator(self, it: PuppetBinaryOperator) -> str:
        return f'{self.serialize(it.lhs)} {it.op} {self.serialize(it.rhs)}'

    @override
    def _puppet_unary_operator(self, it: PuppetUnaryOperator) -> str:
        return f'{it.op} {self.serialize(it.x)}'

    @override
    def _puppet_array(self, it: PuppetArray) -> str:
        if not it.items:
            return '[]'
        else:
            out = '[\n'
            for item in it.items:
                out += self.ind(1) + self.indent(2).serialize(item) + ',\n'
            out += self.ind() + ']'
            return out

    @override
    def _puppet_call(self, it: PuppetCall) -> str:
        args = ', '.join(self.serialize(x) for x in it.args)
        return f'{self.serialize(it.func)}({args})'

    @override
    def _puppet_call_method(self, it: PuppetCallMethod) -> str:
        out: str = self.serialize(it.func)

        if it.args:
            args = ', '.join(self.serialize(x) for x in it.args)
            out += f' ({args})'

        if it.block:
            out += self.serialize(it.block)

        return out

    @override
    def _puppet_case(self, it: PuppetCase) -> str:
        out: str = f'case {self.serialize(it.test)} {{\n'
        for (when, body) in it.cases:
            out += self.ind(1)
            out += ', '.join(self.indent(1).serialize(x) for x in when)
            out += ': {\n'
            for item in body:
                out += self.ind(2) + self.indent(2).serialize(item) + '\n'
            out += self.ind(1) + '}\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_declaration_parameter(
            self, it: PuppetDeclarationParameter) -> str:
        out: str = ''
        if it.type:
            out += f'{self.indent(1).serialize(it.type)} '
        out += f'${it.k}'
        if it.v:
            out += f' = {self.indent(1).serialize(it.v)}'
        return out

    @override
    def _puppet_instanciation_parameter(
            self, it: PuppetInstanciationParameter) -> str:
        return f'{it.k} {it.arrow} {self.serialize(it.v)}'

    @override
    def _puppet_class(self, it: PuppetClass) -> str:
        out: str = f'class {it.name}'
        if it.params:
            out += self.format_declaration_parameters(it.params)

        out += ' {\n'
        for form in it.body:
            out += self.ind(1) + self.indent(1).serialize(form) + '\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_concat(self, it: PuppetConcat) -> str:
        out = '"'
        for item in it.fragments:
            match item:
                case PuppetString(s):
                    out += s
                case PuppetVar(x):
                    out += f"${{{x}}}"
                case puppet:
                    out += f"${{{self.serialize(puppet)}}}"
        out += '"'
        return out

    @override
    def _puppet_collect(self, it: PuppetCollect) -> str:
        return f'{self.indent(1).serialize(it.type)} {self.serialize(it.query)}'

    @override
    def _puppet_if_chain(self, it: PuppetIfChain) -> str:
        (test1, body1), *rest = it.clauses
        assert test1 != 'else'
        out: str = f'if {self.serialize(test1)} {{\n'
        for item in body1:
            out += self.ind(1) + self.indent(1).serialize(item) + '\n'
        out += self.ind() + '}'
        for (testn, bodyn) in rest:
            out += ' '
            if testn == 'else':
                out += 'else'
            else:
                out += f'elsif {self.serialize(testn)}'
            out += ' {\n'
            for item in bodyn:
                out += self.ind(1) + self.indent(1).serialize(item) + '\n'
            out += self.ind() + '}'
        return out

    @override
    def _puppet_unless(self, it: PuppetUnless) -> str:
        out: str = f'unless {self.serialize(it.condition)} {{\n'
        for item in it.consequent:
            out += self.ind(1) + self.indent(1).serialize(item) + '\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_keyword(self, it: PuppetKeyword) -> str:
        return it.name

    @override
    def _puppet_exported_query(self, it: PuppetExportedQuery) -> str:
        out: str = '<<|'
        if f := it.filter:
            out += ' ' + self.serialize(f)
        out += ' |>>'
        return out

    @override
    def _puppet_virtual_query(self, it: PuppetVirtualQuery) -> str:
        out: str = '<|'
        if f := it.q:
            out += ' ' + self.serialize(f)
        out += ' |>'
        return out

    @override
    def _puppet_function(self, it: PuppetFunction) -> str:
        out: str = f'function {it.name}'
        if it.params:
            out += self.format_declaration_parameters(it.params)

        if ret := it.returns:
            out += f' >> {self.indent(1).serialize(ret)}'

        out += ' {\n'
        for item in it.body:
            out += self.ind(1) + self.indent(1).serialize(item) + '\n'
        out += self.ind() + '}'

        return out

    @override
    def _puppet_hash(self, it: PuppetHash) -> str:
        if not it.entries:
            return '{}'
        else:
            out: str = '{\n'
            for item in it.entries:
                out += self.ind(1)
                out += self.indent(1).serialize_hash_entry(item)
                out += ',\n'
            out += self.ind() + '}'
            return out

    @override
    def _puppet_heredoc(self, it: PuppetHeredoc) -> str:
        """
        Serialize heredoc with interpolation.

        The esacpes $, r, and t are always added and un-escaped,
        while the rest are left as is, since they work fine in the literal.
        """
        syntax: str = ''
        if it.syntax:
            syntax = f':{it.syntax}'

        # TODO find delimiter
        body = ''
        for frag in it.fragments:
            match frag:
                case PuppetString(s):
                    # \r, \t, \, $
                    e = re.sub('[\r\t\\\\$]', lambda m: {
                        '\r': r'\r',
                        '\t': r'\t',
                        }.get(m[0], '\\' + m[0]), s)
                    body += e
                case PuppetVar(x):
                    body += f'${{{x}}}'
                case p:
                    body += self.indent(2).serialize(p)

        # Check if string ends with a newline
        match it.fragments[-1]:
            case PuppetString(s) if s.endswith('\n'):
                eol_marker = ''
                body = body[:-1]
            case _:
                eol_marker = '-'

        # Aligning this to the left column is ugly, but saves us from
        # parsing newlines in the actual string
        return f'@("EOF"{syntax}/$rt)\n{body}\n|{eol_marker} EOF'

    @override
    def _puppet_literal_heredoc(self, it: PuppetLiteralHeredoc) -> str:
        syntax: str = ''
        if it.syntax:
            syntax = f':{it.syntax}'

        out: str = ''
        if not it.content:
            out += f'@(EOF{syntax})\n'
            out += self.ind() + '|- EOF'
            return out

        delimiter = find_heredoc_delimiter(it.content)

        out += f'@({delimiter}{syntax})\n'

        lines = it.content.split('\n')
        eol: bool = False
        if lines[-1] == '':
            lines = lines[:-1]  # Remove last
            eol = True

        for line in lines:
            out += self.ind(1) + line + '\n'

        out += self.ind(1) + '|'

        if not eol:
            out += '-'

        out += ' ' + delimiter

        return out

    @override
    def _puppet_var(self, it: PuppetVar) -> str:
        return f'${it.name}'

    @override
    def _puppet_lambda(self, it: PuppetLambda) -> str:
        out: str = '|'
        for item in it.params:
            out += 'TODO'
        out += '| {'
        for form in it.body:
            out += self.ind(1) + self.indent(1).serialize(form)
        out += self.ind() + '}'
        return out

    @override
    def _puppet_qn(self, it: PuppetQn) -> str:
        return it.name

    @override
    def _puppet_qr(self, it: PuppetQr) -> str:
        return it.name

    @override
    def _puppet_regex(self, it: PuppetRegex) -> str:
        return f'/{it.s}/'

    @override
    def _puppet_resource(self, it: PuppetResource) -> str:
        out = f'{self.indent(1).serialize(it.type)} {{'
        match it.bodies:
            case [(name, values)]:
                out += f' {self.indent(1).serialize(name)}:\n'
                for v in values:
                    out += self.ind(1) + self.indent(2).serialize(v) + ',\n'
            case bodies:
                out += '\n'
                for (name, values) in bodies:
                    out += f'{self.ind(1)}{self.indent(1).serialize(name)}:\n'
                    for v in values:
                        out += self.ind(2) + self.indent(3).serialize(v) + ',\n'
                    out += self.ind(2) + ';\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_define(self, it: PuppetDefine) -> str:
        out: str = f'define {it.name}'
        if params := it.params:
            out += self.format_declaration_parameters(params)

        out += ' {\n'
        for form in it.body:
            out += self.ind(1) + self.indent(1).serialize(form) + '\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_string(self, it: PuppetString) -> str:
        # TODO escaping
        return f"'{it.s}'"

    @override
    def _puppet_number(self, it: PuppetNumber) -> str:
        return str(it.x)

    @override
    def _puppet_invoke(self, it: PuppetInvoke) -> str:
        invoker = f'{self.serialize(it.func)}'
        out: str = invoker
        template: str
        if invoker == 'include':
            template = ' {}'
        else:
            template = '({})'
        out += template.format(', '.join(self.indent(1).serialize(x) for x in it.args))
        return out

    @override
    def _puppet_resource_defaults(self, it: PuppetResourceDefaults) -> str:
        out: str = f'{self.serialize(it.type)} {{\n'
        for op in it.ops:
            out += self.ind(1) + self.indent(1).serialize(op) + ',\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_resource_override(self, it: PuppetResourceOverride) -> str:
        out: str = f'{self.serialize(it.resource)} {{\n'
        for op in it.ops:
            out += self.ind(1) + self.indent(1).serialize(op) + ',\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_declaration(self, it: PuppetDeclaration) -> str:
        return f'{self.serialize(it.k)} = {self.serialize(it.v)}'

    @override
    def _puppet_selector(self, it: PuppetSelector) -> str:
        out: str = f'{self.serialize(it.resource)} ? {{\n'
        rendered_cases = [(self.indent(1).serialize(test),
                           self.indent(2).serialize(body))
                          for (test, body) in it.cases]
        case_width = max(string_width(c[0], self.__indent + 1) for c in rendered_cases)
        for (test, body) in rendered_cases:
            out += self.ind(1) + test
            out += ' ' * (case_width - string_width(test, self.__indent + 1))
            out += f' => {body},\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_block(self, it: PuppetBlock) -> str:
        return '\n'.join(self.serialize(x) for x in it.entries)

    @override
    def _puppet_node(self, it: PuppetNode) -> str:
        out: str = 'node '
        out += ', '.join(self.serialize(x) for x in it.matches)
        out += ' {\n'
        for item in it.body:
            out += self.ind(1) + self.indent(1).serialize(item) + '\n'
        out += self.ind() + '}'
        return out

    @override
    def _puppet_parenthesis(self, it: PuppetParenthesis) -> str:
        return f'({self.serialize(it.form)})'

    @override
    def _puppet_nop(self, it: PuppetNop) -> str:
        return ''
