"""Fromat Puppet AST's into something useful."""

from .base import Serializer
from muppet.puppet.ast import PuppetAST
from typing import Any, TypeVar


T = TypeVar('T')


def serialize(ast: PuppetAST, serializer: type[Serializer[T]]) -> T:
    """Run the given serializer on the given data."""
    return serializer().serialize(ast)


# TODO is this even used? Remove it?
def to_string(t: Any) -> str:
    """Turn a serialized structure into a string."""
    match t:
        case str(s):
            return s
        case [*xs]:
            return ''.join(to_string(x) for x in xs)
        case _:
            raise ValueError()
