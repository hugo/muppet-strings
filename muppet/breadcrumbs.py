"""
Page breadcrumbs.

Page breadcrumbs are the "how did we get here" at the top of web pages.

For example, the page
``/modules/example-module/example-class`` would have a breadcrumb list like

- ``/modules`` - Modules
- ``/modules/example-module`` - Example Module
- ``/modules/example-module/example-class`` Example Class
"""

import re
from dataclasses import dataclass
from typing import (
    Tuple,
)


@dataclass
class Breadcrumb:
    """
    A breadcrumb entry.

    :param ref:
        A url path. Meaning that it should contain all parents.
    :param text:
        The displayed text for this entry.
    """

    ref: str
    text: str


@dataclass
class Breadcrumbs:
    """
    A complete set of breadcrumbs.

    Basically a non-empty list, with the last item being guaranteed.
    This since the trailing item shouldn't be a link, and with this we
    have guaranteed correct data when rendering the template.
    """

    crumbs: list[Breadcrumb]
    final: str


def breadcrumbs(*items: str | Tuple[str, str]) -> Breadcrumbs:
    """
    Generate a breadcrumb trail.

    :param items:
        The parts of the trace.

        Each item should either be a single string, which is then used
        as both the name, and the url component, or a tuple, in which
        case the left value will be the displaed string, and the right
        value the url component.
    """
    url = '/'
    result = []
    for item in items[:-1]:
        if isinstance(item, str):
            url += item + '/'
            text = item
        else:
            url += item[1] + '/'
            text = item[0]
        url = re.sub('/+', '/', url)
        result.append(Breadcrumb(ref=url, text=text))

    final = items[-1]
    if isinstance(final, str):
        finaltxt = final
    else:
        finaltxt = final[0]

    return Breadcrumbs(result, finaltxt)
