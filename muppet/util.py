"""Various misc. utilities."""

from typing import (
    TypeVar,
    Callable,
    Iterable,
)

from collections.abc import (
    Sequence,
)

# from logging import LoggerAdapter
# import copy


T = TypeVar('T')
U = TypeVar('U')


def group_by(proc: Callable[[T], U], seq: Sequence[T]) -> dict[U, list[T]]:
    """
    Group elements in seq by proc.

    Return a dictionary mapping the result of proc onto lists of each
    element which evaluated to that key.
    """
    d: dict[U, list[T]] = {}
    for item in seq:
        key = proc(item)
        d[key] = (d.get(key) or []) + [item]
    return d


def partition(proc: Callable[[T], bool], seq: Sequence[T]) -> tuple[list[T], list[T]]:
    """
    Partition items into the items not matching, and matching.

    :param proc:
        Predicate function, grouping elements.

    :proc seq:
        Sequence of items to partition.

    :return:
        Two lists, with the left one being all items not matching the
        predicate, and the right one being all elements that matched.
    """
    groups = group_by(proc, seq)
    return groups.get(False, []), groups.get(True, [])


def concatenate(lstlst: Iterable[Iterable[T]]) -> list[T]:
    """Concatenate a list of lists into a flat(er) list."""
    out: list[T] = []
    for lst in lstlst:
        out += lst
    return out


# class ILoggerAdapter(LoggerAdapter):
#     """Wrapper around a logger, allowing extra keys to added once."""
#
#     def __init__(self, logger, extra):
#         super().__init__(logger, extra)
#         self.env = extra
#
#     def process(self, msg: str, kwargs):
#         """Overridden method from upstream."""
#         msg, kwargs = super().process(msg, kwargs)
#         result = copy.deepcopy(kwargs)
#         default_kwargs_key = ['exc_info', 'stack_info', 'extra']
#         custom_key = [k for k in result.keys() if k not in default_kwargs_key]
#         result['extra'].update({k: result.pop(k) for k in custom_key})
#
#         return msg, result


def string_around(string: str, point: int, back: int = 5, front: int = 5) -> str:
    """
    Return substring around the point in the given string.

    :param string:
        The source string to extract a substring from.
    :param point:
        The focal which to center the string around.
    :param back:
        Number of characters to include before the point.
    :param front:
        Number of characters to incrlude after the point.
    :return:
    """
    tmp = string[point - back:point] \
        + string[point] \
        + string[point + 1:point + 1 + front]
    return ''.join(chr(0x2400 + x if 0 <= x < 0x20
                       else 0x2421 if x == 127
                       else x) for x in tmp.encode('UTF-8'))


def len_before(string: str, point: int, back: int) -> int:
    """
    Return length of string from point back to at most point chars.

    :param string:
    :param point:
    :param back:
    """
    return len(string[max(0, point - back):point])
