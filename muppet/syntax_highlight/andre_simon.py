"""
Syntax highlighting through the highlight(1) command.

The name "Andre Simon" is choosen based of the author, and that
"highlight" by itself it really non-descriptive.
"""

import subprocess

try:
    subprocess.run(['highlight', '--version'], stdout=subprocess.DEVNULL)
    available = True
except FileNotFoundError:
    available = False


def highlight(code: str, language: str) -> str:
    """Highlight code through the ``highlight`` command."""
    # TODO line- vs pygments line_
    cmd = subprocess.run(['highlight',
                          '--out-format', 'html',
                          '--fragment',
                          '--line-numbers',
                          '--anchors',
                          '--anchor-prefix=line',
                          '--class-name=NONE',
                          '--syntax', language,
                          '--enclose-pre'],
                         capture_output=True,
                         text=True,
                         input=code,
                         check=True)
    return f"""
    <!-- Generated through highlight(1), as language {language} -->
    <div class="highlight-andre-simon">{cmd.stdout}</div>
    """
