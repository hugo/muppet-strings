"""
Syntax highlight with the best available backend.

Syntax highlight the given code by the best, available, syntax
highlighter, returning the result as HTML. The default highlighter
simply wraps the output in
``<pre><code class={language}>{code}</code></pre>``
(with escaping). This means that it can still be handled by JavaScript if so desired.
"""

from . import pygments
from . import andre_simon
from . import plain

from typing import cast


for module in [pygments, andre_simon, plain]:
    if module.available:
        backend = module
        break
else:
    # This should never happen, since ``plain`` should always be
    # available.
    raise ValueError("No applicable highlight module")


def highlight(source: str, language: str) -> str:
    """
    Highlight the given source as language, retuning HTML.

    :param source:
        Source code to highlight.
    :param language:
        Language of the source code.
    :returns:
        An HTML string.
    """
    return cast(str, backend.highlight(source, language))
