"""Syntax highlighting through pygments."""
# pyright: reportUnboundVariable=false

try:
    from pygments.formatters import HtmlFormatter
    from pygments.lexers import get_lexer_by_name
    import pygments
    available = True
except ModuleNotFoundError:  # pragma: no cover
    available = False


def highlight(code: str, language: str) -> str:
    """Highlight code through pygments."""
    # NOTE possibly propagate error from ModuleNoteFound in the imports
    assert available, "Pygmetize not available on this machine"
    out = pygments.highlight(code, get_lexer_by_name(language),
                             HtmlFormatter(cssclass='highlight-pygments',
                                           lineanchors='line',
                                           linenos='table',
                                           # linenos='inline'
                                           anchorlinenos=True,
                                           ))
    return f"""
    <!-- Generated through pygments, as {language} -->
    {out}
    """
