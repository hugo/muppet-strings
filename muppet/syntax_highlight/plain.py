"""Non-highlighter useful as a backup."""

import html

available = True


def highlight(code: str, language: str) -> str:
    """Return the code "highlighted" by wrapping it in <pre> tags."""
    out = f'<pre><code class="{language}">{html.escape(code)}</code></pre>'
    return f"""
    <!-- "Genererated" as plain output -->
    <div class"highlight-plain">{out}</div>
    """
