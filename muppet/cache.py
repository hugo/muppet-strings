"""A simple cache."""

from typing import (
    Callable,
    Optional,
    final,
)
import os.path
import pathlib
import hashlib


class AbstractCache:
    """
    Abstract base class for caches.

    Caches allows storing data under arbitrary keys, and later look up
    their values.

    These procedures use strings for the keys, since they are easy to
    manipulate in python. Individual implementations might however
    want to encode them into UTF-8 (or something else).

    Values are bytes, since anything should be storable.
    """

    def __init__(self) -> None:
        raise NotImplementedError()

    def put(self, key: str, value: bytes) -> None:
        """
        Store data into the cache.

        Multiple writes to the same key are assumed to overwrite each
        other.

        :param key:
            Name to associate with data
        :param value:
            Actuall data.
        """
        raise NotImplementedError()

    def get(self, key: str) -> Optional[bytes]:
        """
        Retrieve data from the cache.

        :param key:
            Name which was (hopefully) previously associated with some
            data.

        :return:
            The previously stored data, if available, otherwise
            nothing.
        """
        raise NotImplementedError()

    @final
    def memoize_function(
            self,
            prefix: str,
            func: Callable[[bytes], bytes]) -> Callable[[bytes], bytes]:
        """
        Return a memoized version of the given function.

        A new function is created, which will first check the cache
        for the same input, and either return that, or it will run the
        original function.

        .. code-block:: python
            :caption: How the cache key will be generated.

            key = prefix + hashlib.sha1(data).hexdigest()

        :param prefix:
            String to prefix cache keys with. Should be unique among all memoized functions

        :param func:
            The function to memoize.
        """
        def inner(data: bytes) -> bytes:
            key = prefix + hashlib.sha1(data).hexdigest()
            if value := self.get(key):
                return value
            else:
                value = func(data)
                self.put(key, value)
                return value
        return inner

    @final
    def memoize(self, prefix: str) -> Callable[[Callable[[bytes], bytes]],
                                               Callable[[bytes], bytes]]:
        """
        :func:`memoize_function`, but as a decorator.

        .. code-block:: python
            :caption: Example usage

            @memoize('my_expansive_function')
            def my_expansive_function(in: bytes) -> bytes:
                ...
        """
        return lambda func: self.memoize_function(prefix, func)


# TODO rename this to fs-cache
class Cache(AbstractCache):
    """
    A simple cache.

    This implementation is file system-backed, but its interface
    should allow any backend.
    """

    def __init__(self, path: str, *, mkdir: bool = True):
        self.base = path
        if mkdir:
            pathlib.Path(path).mkdir(parents=True, exist_ok=True)

    def put(self, key: str, value: bytes) -> None:
        """Put content into cache."""
        with open(os.path.join(self.base, key), 'wb') as f:
            f.write(value)

    def get(self, key: str) -> Optional[bytes]:
        """Get item from cache if it exists, or Nothing otherwise."""
        try:
            with open(os.path.join(self.base, key), 'rb') as f:
                return f.read()
        except FileNotFoundError:
            return None


class UnCache(AbstractCache):
    """
    A non-caching cache.

    Stores are no-ops, and lookups always fail.

    This is mostly useful for testing.
    """

    def __init__(self) -> None:
        pass

    def put(self, key: str, value: bytes) -> None:  # noqa: D102
        return None

    def get(self, key: str) -> Optional[bytes]:  # noqa: D102
        return None
