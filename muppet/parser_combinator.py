"""
YAPC - Yet Another Parser Combinator.

This module implements a simple, and probably rather bad parser
combinator.

There are two "core" parts in the system. The
:class:`ParserCombinator` class, which holds the the current state,
and :data:`Parser`, which are the rules declaring what to parse.

.. code-block:: python
   :caption: A basic example, "parsing" everything in a file

    filename = 'example.ext'
    with open(filename, 'r') as f:
        data = f.read()

    parse_context = ParserCombinator(data, file=filename)

    parser = many(char)

    parse_context.get(parser)

See :data:`Parser` for information about how parsers are constructed.

Rules
=====

Each parser MUST implement a ``run`` method, which should take an
instance of the parser object, and attempt to read characters from it.
Characters can be read from the parser instance through the ``read``
method. A couple of other usefull methods are available, see
:py:class:`ParserCombinator`.

A parser should either return a list of ``MatchObject``:s of what it
parsed, or throw a ``ParseError`` exception if the current parser
isn't applicable. These errors SHOULD be catched, since they should be
treated as non fatal. For exmaple

.. code-block:: python

    literal = integer | string

will be parsed by first trynig the integer parser, which SHOULD fail
if the input isn't a valid integer, and then try a string literal. If
neither matches, then the whole (``literal``) expression SHOULD throw.
"""
from __future__ import annotations

# import html
from dataclasses import dataclass, field
from typing import (
    Any,
    Callable,
    Optional,
    Sequence,
    TypeAlias,
    Union,
    TypeVar,
)
import logging
from muppet.util import string_around, len_before


logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class MatchCompound:
    """A matched item, similar to a regex match."""

    type: str
    matched: Any

    def __repr__(self) -> str:
        if self.type:
            return f'`{self.type}({self.matched!r})`'
        else:
            return f'MatchCompound({self.matched!r}))'


MatchObject: TypeAlias = MatchCompound | str


@dataclass
class ParseError(Exception):
    """
    Errors encountered while parsing.

    This should only appear with optional fields, since we don't know
    if the next token is the "expected" one. It should be captured
    internally by all exported procedures and methods.

    :param msg:
        Free-text message describing the error.
    :param pos:
        The position, counted in characters, from the beginning of the
        string. Zero indexed.
    :param src:
        The string which contains the position of the error.
    :param stk:
        Stack trace of previous parsers. Recommended usage is

        .. code-block:: python

            try:
                ...
            except ParseError as e:
                e.stk.append(item_used_in_block)
                raise e
    """

    msg: Optional[str] = None
    pos: Optional[int] = None
    src: Optional[str] = None

    stk: list['Parser'] = field(default_factory=list)

    def __str__(self) -> str:
        s = ''
        if self.msg:
            s += self.msg
        if self.pos:
            s += f' (pos {self.pos})\n'
            preview_back = 10
            preview_front = 10
            if self.src:
                s += "|" + string_around(self.src, self.pos, preview_back, preview_front)
                s += '\n'
                s += "|" + ' ' * len_before(self.src, self.pos, preview_back)
                s += '^'
                s += '\n'
        if self.msg or self.pos:
            s += '\n'
        s += "Traceback of called parsers:\n"
        for item in self.stk:
            s += '• '
            item = str(item)
            if len(item) > 60:
                s += item[:60] + '…'
            else:
                s += item
            s += '\n'
        return s


class ParseDirective:
    """
    Abstract base class for custom parsers.

    This is the base class for all "advanced" parsing directives. This
    can be thought of as the procedural escape hatch to the purely
    functional parser combinators.

    Each instance of (a child of) this class is a parser in its own
    right. The recommended usage is to subclass this class into
    something which looks like a regural function. For example, see
    :class:`and_`. This since these instances are used like functions
    when constructing compound parsers, and they technically being
    classes is more of an implementation detail.

    And number of operators are also available. These can be
    overridden by individual children, if care is taken. For example,
    :class:`and_` overrides ``__and__`` to merge adjacent `and`s into
    one large `and`.

    :operator &:

        .. code-block:: python

            P3 = P1 & P2

        Creates a new combinator, which matches if *P1* first matches,
        and *P2* matches wherever *P1* ends.

        Note that this is an all or nothing deal, in contrast to sequences
        from lists, which may fail partway through.

        See :class:`and_` and :ref:`sequence <sequence>`

    :operator |:

        .. code-block:: python

            P3 = P1 | P2

        Creates a new combinator, which matches either *P1* or *P2*,
        returning the relut from the one matching.

        See :class:`or_`

    :operator @:

        .. code-block:: python

            P2 = P @ HANDLER

        Matches (or fails) *P* as usuall. However, if it matches, then the
        result will be passed through the procedure *f*, and its result
        will become part of the parsed tree instead of the initial result
        of *P*.

        See :func:`fmap`

    :operator ~:

        .. code-block:: python

            P2 = ~ P

        Creates a combinator which matches the complement of *P*, meaning
        that this parser will be successfull if *P* fails.

        See :class:`not_`

    :param handler:
        Optional return value handler, see `@` above.
    """

    handler: Optional[Callable[[list[Any]], list[Any]]] = None

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:
        """
        Execute this directive.

        As a procedural escape hatch to parser combinators, the "raw"
        parser object is directly available. See the top of file,
        under "Rules" for information.

        :param parser:
            The formatter which holds the context to use.
        """
        raise NotImplementedError(f'Missing run() for {self.__class__.__name__}')

    def __and__(self, other: Any) -> 'ParseDirective':
        return and_(self, other)

    def __or__(self, other: Any) -> 'ParseDirective':
        return or_(self, other)

    def __matmul__(self, proc: Any) -> 'ParseDirective':
        self.handler = proc
        return self

    def __invert__(self) -> 'ParseDirective':
        return not_(self)


Parser: TypeAlias = Union[
    str,
    None,
    ParseDirective,
    Callable[[], 'Parser'],
    Sequence['Parser']
]
"""
The core parser type.

A parser is a rule for how a string can be parsed.

The "uninteresting" types are

- Strings; which match their contents literally
- None; which matches nothing

    .. _sequence:

- Sequences of other parsers; which matches each one in order, but
  still consumes output if it fails partway through.
  Prefer `&` chaining (see :class:`and <and_>`)
- Procedures, which are just lazy evaluation for other parsers

Then finally there is :class:`ParseDirective`, where the real fun
happens. These allow for arbitrary rules. To construct new rules,
either extend the class (not recommended), or build compound parsers
from the fundamental building blocks (recommended).
"""


@dataclass
class s(ParseDirective):
    """
    Construct a propper ParserCombinator object from a loose Parser.

    On it's own this is a no-op, but it allows for strings to be
    "promoted" into parser combinator objects, which allows combining
    operators to work on it. See :class:`ParseDirective`.
    """

    s: Parser

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        return parser.get(self.s)

    def __repr__(self) -> str:
        return repr(self.s)


@dataclass
class not_(ParseDirective):
    """Succeeds if the given directive fails."""

    form: Parser

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        snapshot = parser.snapshot()
        try:
            # if this succeeds, then we want to fail
            # but if it fails then we want to suceed
            parser.get(self.form)
        except ParseError:
            parser.restore(snapshot)
            # The parser failed, meaning that we "suceeded"
            return []
        raise parser.error()

    def __repr__(self) -> str:
        return f'~ {self.form}'


@dataclass
class name(ParseDirective):
    r"""
    Wrap a parser into a new "primitive" item.

    The resulting parser works exactly as the given parser, but in
    when instead of displaying the parsers components when printing
    it, instead only the name will be shown.

    :param name:
        New name to show
    :param form:
        Actual parser.

    .. code-block:: python

        >>> space = s(' ') | '\t' | '\n' | '\r'
        >>> ws = name('ws', many(space))
        >>> print(ws)
        [ws]
    """

    name: str
    form: 'Parser'

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        return parser.get(self.form)

    def __repr__(self) -> str:
        return f'[{self.name}]'


def optional(parser: ParseDirective) -> ParseDirective:
    """Either match the given parser, or match nothing."""
    return parser | nop


@dataclass
class and_(ParseDirective):
    """Parse a group in order."""

    items: list['Parser']

    def __init__(self, *items: 'Parser'):
        self.items = list(items)

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        snapshot = parser.snapshot()
        out = []
        try:
            # print("self.items", self.items)
            for item in self.items:
                # print("item=", item)
                result = parser.get(item)
                # print("result=", result)
                out += result
        except ParseError as e:
            parser.restore(snapshot)
            raise e
        return out

    def __and__(self, other: Any) -> ParseDirective:
        if isinstance(other, and_):
            return and_(*self.items, *other.items)
        else:
            return and_(*self.items, other)

    def __repr__(self) -> str:
        return f'({" & ".join(repr(x) for x in self.items)})'


@dataclass
class or_(ParseDirective):
    """
    A set of multiple parse forms to try in order.

    The first matched one will be used.

    (Alias Variant)
    """

    alternatives: list['Parser']

    def __init__(self, *alternatives: Parser):
        """Retrun a new OptionParse object."""
        self.alternatives = list(alternatives)

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        save = parser.snapshot()
        for alternative in self.alternatives:
            try:
                return parser.get(alternative)
            except ParseError:
                parser.restore(save)
        else:
            msg = f"No alternative suceeded, cases={self.alternatives}, seek={parser.seek}"
            raise parser.error(msg)

    def __or__(self, other: Any) -> ParseDirective:
        if isinstance(other, or_):
            return or_(*self.alternatives, *other.alternatives)
        else:
            return or_(*self.alternatives, other)

    def __repr__(self) -> str:
        # return f'or_({", ".join(repr(x) for x in self.alternatives)})'
        return f'({" | ".join(repr(x) for x in self.alternatives)})'


@dataclass
class count(ParseDirective):
    """
    "Run" a given parser between `min` and `max` times.

    Constructs a new parser applying the given parser a number of
    times. After the given parser, either a maximum count can be given
    (in which case the minimum becomes 0), an explicit minimum and
    maximum, or `min` and `max` can be passed as keyword arguments.

    .. code-block:: python

       count(parser, max)
       count(parser, min, max)
       count(parser, min=0, max=10)
    """

    min: int
    max: int
    parser: Parser

    def __init__(self,
                 parser: Parser,
                 arg: int,
                 *args: int,
                 **kwargs: int):
        min_ = 0
        max_ = 1

        match args:
            case []:
                max_ = arg
            case [v]:
                min_ = arg
                max_ = v

        if 'min' in kwargs:
            min_ = kwargs['min']
        if 'max' in kwargs:
            max_ = kwargs['max']

        self.min = min_
        self.max = max_
        self.parser = parser

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        snapshot = parser.snapshot()
        out = []
        try:
            # If any of these fails, then the whole operation was a fail.
            for _ in range(self.min):
                out += parser.get(self.parser)

            # These are optional, so an error here is "expected"
            for _ in range(self.min, self.max):
                try:
                    out += parser.get(self.parser)
                except ParseError:
                    break
        except Exception as e:
            out = []
            parser.restore(snapshot)
            raise e
        return out

    def __repr__(self) -> str:
        return f'count({repr(self.parser)}, {self.min}, {self.max})'


def discard(parser: Parser) -> ParseDirective:
    """Run parser, but discard the result."""
    return s(parser) @ (lambda _: [])


@dataclass
class _CharParser(ParseDirective):
    """Parse a single character."""

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        return [parser._get_char()]

    def __repr__(self) -> str:
        return 'char'


char = _CharParser()
"""Uncodnitionally reads (parser) a single char."""


@dataclass
class all_(ParseDirective):
    """
    Run each parser in succession from the same point, returning the final result.

    This can be used to implement and and xor between parsers.

    .. code-block:: python
       :caption: Parse any non-space character, a ⊻ b

        all_(~space, char)

    .. code-block:: python
       :caption: Roundabout way of parsing A-F, a & b

       all_(hex_digit, uppercase)

       # Note that uppercase isn't implemented here
    """

    parsers: list[Parser]

    def __init__(self, *parsers: Parser) -> None:
        self.parsers = list(parsers)

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        match self.parsers:
            case [*ps, last]:
                for p in ps:
                    snapshot = parser.snapshot()
                    parser.get(p)
                    parser.restore(snapshot)
                return parser.get(last)
            case []:
                return []
        raise ValueError(f"This shoudn't be reachable: {self!r}")


@dataclass
class _NopParser(ParseDirective):
    """Parses nothing."""

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        return []

    def __repr__(self) -> str:
        return 'nop'


nop = _NopParser()
"""
Null Parser, and always succeeds.

This is mostyl useful for different compound parsers. Either as a
'null' element to build from, or for implementing optional parsers,
and probably much more.

.. code-block:: python
   :caption: Implementation of optional

   def optional(parser):
       return parser | nop
"""


# @dataclass
# class many(ParseDirective):
#     """Many a parser as many times as possible."""
#
#     parser: 'Parser'
#
#     def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
#         out = []
#         try:
#             while True:
#                 entry = parser.get(self.parser)
#                 if not entry:
#                     raise ParseError()
#                 # logger.info("seek = %s, parser = %s",
#                 #             parser.seek, self.parser)
#                 out += entry
#         except ParseError:
#             return out
#
#     def __repr__(self) -> str:
#         return f'many({repr(self.parser)})'


# This is a "nicer" looking implementation of ``many``, but it will
# probably blow up the call stack.


def many(parser: Parser) -> ParseDirective:
    """
    Match a parser as many times as possible.

    Consume tokens until the given parser doesn't match any more, this
    means 0 or more.
    """
    return (s(parser) & name('<rest>', lambda: many(parser))) @ (lambda xs: [xs[0], *xs[1:]]) \
        | nop


def many1(parser: ParseDirective) -> ParseDirective:
    """Like ``many``, but at least one. Equivalent to ``P & many(P)``."""
    return parser & many(parser)


@dataclass
class complement(ParseDirective):
    """
    Parse any character not in the given set.

    .. code-block:: python
       :caption: A simple string parser

       str_parser = s('"') & many(complement('"')) & '"'

    :param chars:
        A list of characters which should not match.
    """

    chars: str

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        match parser.peek(char):
            case [str(c)]:
                if c not in self.chars:
                    return parser.get(char)
                else:
                    raise parser.error(f"{c} in {self.chars}")
            case it:
                raise parser.error(f"Parsed item wasn't a char: {it!r}")

    def __repr__(self) -> str:
        return f'complement({repr(self.chars)})'


@dataclass
class tag(ParseDirective):
    """
    Tag value returned by parser.

    This returns a new parser which works exactly like the given
    parser, but wraps the given parsers result in a MatchCompound
    object, attaching `tag` as metadata.

    This allows the user to know what type of object was matched, for
    example

    .. code-block:: python

        # Parses the word "if"
        p1 = "if"

        # Also parses the word "if", but marks it as a keyword for
        # handling later.
        p2 = tag("keyword", "if")

    This is NOT to be confused with :class:`name`, which just changes
    the debug output of objects. It's recommended to use `name`
    outside `tag` to reduce clutter in debug prints.

    :param tag:
        String to tag the parser's result with.
    :param parser:
        The actual parser.
    """

    tag: str
    parser: 'Parser'

    def run(self, parser: 'ParserCombinator') -> list[MatchObject]:  # noqa: D102
        result = parser.get(self.parser)
        return [MatchCompound(type=self.tag, matched=result)]

    def __repr__(self) -> str:
        return f'{self.tag}({self.parser!r})'


def delimited(delim: Parser, parser: Parser) -> ParseDirective:
    """
    Read an infix delimited list of items.

    .. code-block:: python
       :caption: Usage, with optional trailing comma

        (delimited(ws & ',' & ws,  # The delimiter
                   ITEM)           # The item to parse
           & optional(ws, ',')     # Trailing comma
    """
    return s(parser) & many(s(delim) & s(parser))


digit = name('digit', or_(*(chr(x + ord('0')) for x in range(0, 10))))
"""Parser for a single base 10 digit."""

hexdig = name('hexdig',
              digit
              | or_(*(chr(x + ord('A')) for x in range(0, 6)))
              | or_(*(chr(x + ord('a')) for x in range(0, 6))))
"""
Parser for a single hexadecimal digit.

Both upper and lower case are supported
"""

space = name('space', s(' ') | '\t' | '\n' | '\r')
"""
Parses a single whitespace token.

A whitespace token here is defined as space, tab, newline, or carriage return.
"""

ws = name('ws', many(space))
"""
Parses a whitespace sequence.

A whitespace sequence is any (including 0) number of var:`space`
characters after one another.
"""


def line_comment(start: str) -> ParseDirective:
    """Match a line comment, from start delim to end of line."""
    return tag('line-comment', and_(start, many(complement('\n')), '\n'))


class ParserCombinatorSnapshot:
    """
    An opaque object storing the state of the ParserCombinator.

    This allows undoing any number of operations. Each parser snapshot
    can only be used with the parser formatter which created it.

    A snapshot can be restored multiple times.

    See :func:`ParserCombinator.snapshot` and
    :func:`ParserCombinator.restore` for usage.
    """

    _seek: int
    _creator: 'ParserCombinator'

    @classmethod
    def _construct(cls, seek: int, creator: 'ParserCombinator') -> ParserCombinatorSnapshot:
        """
        Private constructor.

        Python doesn't support private constructors, but this one
        should be, so we do it ourself.
        """
        self = cls()
        self._seek = seek
        self._creator = creator
        return self


class ParserCombinator:
    """
    The state container for running a parser.

    This class represents the current state of a running parser, with
    each call to `get` modifying the state.

    As an aside, the only actual piece of state inside this class is
    the offset into the string, meaning that a (syntax supported)
    state monad would trivially allow us to be non-mutating.

    :param source:
        The string which should be parsed.
    :param file:
        Optional name of the file being parsed. Only used for debug purposes.
    """

    def __init__(self, source: str, file: Optional[str] = None):
        self.__source = source
        # TODO possibly hide this, since it REALLY shouldn't be
        # manually editable
        self.seek = 0
        self.file = file

    def peek(self, item: Parser) -> list[MatchObject]:
        """Run the parser without updating the state."""
        # TODO on error?
        snapshot = self.snapshot()
        result = self.get(item)
        self.restore(snapshot)
        return result

    def _get_char(self) -> str:
        """
        Primitive for reading one character.

        This method reads one character from the buffer and returns
        it, or raises an IndexError if we are at the end of the
        buffer.

        This procedure shouldn't be called directly, but instead the
        parser ``char`` should be invoked.
        """
        try:
            out = self.__source[self.seek]
        except IndexError:
            raise self.error("End of string")
        self.seek += 1
        return out

    def remaining(self) -> str:
        """Return remaining, unparsed, string."""
        return self.__source[self.seek:]

    def get(self, item: Parser) -> list[Any]:
        """
        Try parsing the next item in stream as the passed on parser.

        :param item:
            Parser to run next.

        :returns:
            If the parsing suceeded, the list of matched objects are
            returned, and the interal stream is updated.

            The reason for this type being `Any` is that any rule can
            modify its own return type through :func:`fmap`, meaning
            that we have no idea what we get. However, if no item does
            that, then the return type will be a a list of
            :class:`MatchCompound` objects, and strings.

        :throws ParseError:
            If parsing failed, then a ParseError is thrown. However,
            if any tokens had been parsed that far then the internal
            stream is still upadted, and the matched objects are lost.
        """
        # logger.debug("item: %a", item)
        out: list[Any] = []
        # Step 1: decode our parser, and run it
        # logger.debug("Running parser %a (%a:%a)", item, self.file, self.seek)
        try:
            match item:
                case [*entries]:
                    for entry in entries:
                        out += self.get(entry)

                case str(s):
                    substr = self.__source[self.seek:][:len(s)]
                    # Always case fold when matching
                    if substr.lower() == s.lower():
                        self.seek += len(s)
                        out += [s]
                    else:
                        raise self.error(f'Expected {item!r}, got {substr!r}')

                case None:
                    pass

                case ParseDirective():
                    out += item.run(self)

                case other:
                    if callable(item):
                        out += self.get(item())
                    else:
                        raise ValueError(f"Unexpected item: {other}")

        except ParseError as e:
            e.stk.append(item)
            raise e

        out = self.__merge_matches(out)

        # Parse directives can have a handler attached to them.
        # This handler is run on the matched result, and should return
        # a new result.
        if isinstance(item, ParseDirective) and (f := item.handler):
            # print('Running handler:', f, merged)
            result = f(out)
            # print("Result:", repr(result))
            out = result

        # if isinstance(merged, MatchLiteral):
        #     merged = merged.matched

        # if not isinstance(merged, list):
        #     return [merged]
        # else:
        return out

    def __merge_matches(self, entries: list[Any]) -> list[Any]:
        """
        Attempt to merge similar match nodes into larger nodes.

        For example, a list of matched strings could look like

        .. code-block:: python

            ['H', 'e', 'll', 'o']

        This would merge it into

        .. code-block:: python

            ['Hello']

        If any token which can't be merged appears in the list, simply
        return the original list.
        """
        fixed_entries = []
        s = ''
        for entry in entries:
            match entry:
                case str(c):
                    s += c
                case _:
                    if s != '':
                        fixed_entries.append(s)
                    fixed_entries.append(entry)
                    s = ''
        if s != '':
            fixed_entries.append(s)

        return fixed_entries

    def snapshot(self) -> ParserCombinatorSnapshot:
        """Create a snapshot of the parsers current state."""
        return ParserCombinatorSnapshot._construct(
                seek=self.seek,
                creator=self)

    def restore(self, snapshot: ParserCombinatorSnapshot) -> None:
        """Restore a snapshot, altering the parsers state."""
        assert snapshot._creator == self
        self.seek = snapshot._seek

    def peek_string(self, max_len: int) -> str:
        """
        Return the upcomming string, for debugging purposes.

        :param max_len:
            Maximum length to return, may be shorter if there isn't
            enough characters left
        """
        return self.__source[self.seek:][:max_len]

    def error(self, msg: Optional[str] = None) -> ParseError:
        """
        Return a fresh ParseError.

        This factory method exists since most ParseErrors should have
        their ``pos`` and ``src`` parameters set, but setting them
        manually is cumbersome.

        :param msg:
            Message passed to the ParseError.
        :return:
            A new ParseError, suitable to be directly raised.
        """
        return ParseError(msg=msg, pos=self.seek, src=self.__source)


T = TypeVar('T')


# The infix operation is the "primitive" version, due to the
# ParseDirective needing to own the parser due to how Python is
# structured. This is however an implementation detail.
def fmap(proc: Callable[[Any], Any], parser: ParseDirective) -> ParseDirective:
    """
    Apply proc to the eventual result of parser.

    This returns a new parser, which behaves just as the given parser,
    but it's result will be passed through proc, and that will be the
    result of the new parser.
    """
    return parser @ proc


def const(x: T) -> T:
    """
    Return the value directly.

    Useful for handlers.
    """
    return x
