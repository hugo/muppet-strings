"""
Procedures for generating a table of contents.

This is both used for the main page, containing everything, including
summary of them. As well as the sidebar summary which only contains
(hyperlinked) names.

A table of contents here is defined as a list of :class:`TocHeader`
objects, where each one is a top level entry.
Each entry may then contain subheading, or :class:`TocEntry` entries,
which represent concrete data which may have summaries (in our cases,
puppet classes, functions, ...)
"""

from dataclasses import dataclass, field
from typing import (
    Any,
    TypeAlias,
    Union,
    cast,
)
import html
import os.path

from muppet.puppet.strings import (
    DataTypeAlias,
    DefinedType,
    PuppetClass,
    ResourceType,
    isprivate,
)
from muppet.util import group_by
from muppet.markdown import markdown

from jinja2 import (
    Environment,
    FileSystemLoader,
)


jinja = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=False,
)


"""
---
- name: Classes
  children:
    - name: Public classes
      children:
        - name: apt
          url: apt/manifests/init
          summary: "Main class, includes all other classes."
- name: Resource Types
  children:
    - name: apt_key
      url: lib/puppet/type/apt_key
      summary: "A summary of apt_key would have gone here"
      children:
        - name: apt_key
          url: lib/puppet/provider/apt_key/apt_key
          summary: "Summary of this specific provider"

"""


def html_tag(tag: str, content: str, **attributes: str) -> str:
    """
    Generate the string of an HTML tag.

    .. code-block:: python
        :caption: Example invocation and output

        >>> html_tag('a', 'A link', href='example.com')
        '<a href="example.com">A link</a>'


    *NOTE* none of the arguments will be escaped.

    :param tag:
        The containing tag.

    :param content:
        The body of the HTML. Note that this is NOT escaped, to allow
        nesting. Use ``html.escape`` if escaping is wanted.

    :param attributes:
        Remaining key-value attributes are passed along as HTML
        attributes.
    """
    out = f'<{tag}'
    for key, value in attributes.items():
        out += f' {key}="{value}"'
    out += '>'
    out += content
    out += f'</{tag}>'
    return out


Toc: TypeAlias = Union['TocHeader', 'TocEntry']
TocList: TypeAlias = list['TocHeader'] | list['TocEntry']


@dataclass
class TocHeader:
    """
    A header entry in a TOC list.

    This is an entry which which will in "rich" form be generated with
    a header tag. Either way, it can't contain further data, except
    for children.

    :param name:
        The header of the entry. Think of this as the ``<h[2-6]>`` tag.

    :param children:
        A list of either only other TocHeader objects, or only
        TocEntry objects, the distinction since I don't think it makes
        sense to allow both at the same level.
    """

    name: str
    # children: list['Toc'] = field(default_factory=list)
    children: TocList = field(default_factory=lambda: cast(TocList, []))

    def to_html(self, *, path_base: str, level: int, **args: Any) -> str:
        """Generate an HTML representation suitable for a standalone toc page."""
        out = []
        out.append(html_tag(f'h{level}', html.escape(self.name)))
        if ch := self.children:
            if isinstance(ch[0], TocHeader):
                out += [c.to_html(path_base=path_base, level=level + 1, **args) for c in ch]
            elif isinstance(ch[0], TocEntry):
                out.append(html_tag(
                    'dl', ''.join(c.to_html(path_base=path_base, **args) for c in ch)))
        return ''.join(out)

    def to_html_list(self, path_base: str) -> str:
        """Generate an HTML representation suitable for a compact list."""
        out = '<li>'
        out += html.escape(self.name)
        if ch := self.children:
            out += html_tag('ul', ''.join(c.to_html_list(path_base=path_base) for c in ch))
        out += '</li>'
        return out


@dataclass
class TocEntry:
    """
    An entry in a TOC list which may contain data.

    In Puppet's case, this is anything which actually links to a file
    (or similar).

    :param name:
        Name of the object, should be something like a class name.

    :param url:
        Relative URL to where the documentation for that object will be located.

    :param summary:
        An (optional) short summary of the object. Will most likely be
        extracted from an ``@summary`` tag.

    :param children:
        Even these can have children. Mostly used by resource types for their providers.
    """

    name: str
    url: str
    summary: str = ''
    children: list['TocEntry'] = field(default_factory=list)

    def to_html(self, *, path_base: str, **_: Any) -> str:
        """
        Generate an HTML representation suitable for a standalone toc page.

        Varargs are taken to be compatible with other implementations
        actually taking arguments.
        """
        out = []
        out.append(html_tag('dt',
                            html_tag('a', html.escape(self.name),
                                     href=f'{path_base}/{self.url}')))
        out.append('<dd>')
        out.append(self.summary)
        if ch := self.children:
            out.append(html_tag(
                'dl', ''.join(c.to_html(path_base=path_base) for c in ch)))
        out.append('</dd>')
        return ''.join(out)

    def to_html_list(self, *, path_base: str) -> str:
        """Generate an HTML representation suitable for a compact list."""
        out = '<li>'
        out += html_tag('a', html.escape(self.name), href=f'{path_base}/{self.url}')
        # Summary intentionally ignored
        if ch := self.children:
            out += html_tag('ul', ''.join(c.to_html_list(path_base=path_base) for c in ch))
        out += '</li>'
        return out


def type_aliases_index(module_name: str, alias_list: list[DataTypeAlias]) -> TocHeader:
    """
    Prepare type alias index list.

    A concrete type, on an index page.

    This will be something like a class or resource type.

    Subheading on index page.

    Will most likely be 'Public' or 'Private' objects for the given
    top heading.

    Top level heading in index.

    This should be something like 'Classes', 'Types', ...
    Each entry contains a set of subentries, which can either be
    distinct entries, or sometimes subcategories (such as 'Public' and
    'Private').

    :param name:
        Name of the resource or similar
    :param file:
        Relative path to the resource.
    :param summary:
        One line summary of the resource, will be displayed in the UI.
    """
    groups = group_by(isprivate, alias_list)
    lst: list[TocHeader] = []
    if publics := groups.get(False):
        lst.append(TocHeader(
            name='Public Type Aliases',
            children=[TocEntry(name=i.name,
                               url=f"{module_name}/{os.path.splitext(i.file)[0]}")
                      for i in publics],
            ))

    if privates := groups.get(True):
        lst.append(TocHeader(
            name='Private Type Aliases',
            children=[TocEntry(name=i.name,
                               url=f"{module_name}/{os.path.splitext(i.file)[0]}")
                      for i in privates],
            ))

    return TocHeader(
        name='Type Aliases',
        children=lst,
    )


def resource_type_index(resource_types: list[ResourceType],
                        module_name: str) -> list[TocEntry]:
    """Generate index of all known resource types."""
    lst: list[TocEntry] = []

    for resource_type in resource_types:
        # resource_type['file']
        # resource_type['docstring']

        items: list[TocEntry] = []
        if providers := resource_type.providers:
            for provider in providers:
                # TODO summary tag?
                # TODO, instead, render markdown and take first paragraph
                documentation = provider.docstring.text.split('\n')[0]
                items.append(TocEntry(
                    name=provider.name,
                    url=provider.file,
                    summary=documentation))

        lst.append(TocEntry(name=resource_type.name,
                            url='#TODO',
                            children=items))

    return lst


def index_item(module_name: str, obj: PuppetClass | DefinedType) -> TocEntry:
    """
    Format a puppet type declaration into an index entry.

    :param obj:
        A dictionary at least containing the keys 'name' and 'file',
        and optionally containing 'docstring'. If docstring is present
        then a summary tag is searched for, and added to the resulting
        object.
    """
    name = obj.name

    out: TocEntry = TocEntry(
        name=name,
        url=f'{module_name}/{os.path.splitext(obj.file)[0]}',
    )

    for tag in obj.docstring.tags:
        if tag.tag_name == 'summary':
            out.summary = markdown(tag.text)
            break

    return out
