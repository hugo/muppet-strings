"""Generate output for Puppet Source code."""

import html
import logging
from typing import Sequence

from muppet.parser_combinator import (
    ParserCombinator,
    MatchCompound,
    MatchObject,
)
from muppet.puppet.ast import build_ast
from muppet.puppet.parser import puppet_parser
from muppet.puppet.format.parser import ParserFormatter

from .util import inner_text


logger = logging.getLogger(__name__)


_puppet_doc_base = 'https://www.puppet.com/docs/puppet/7'
_lang_facts_builtin_variables = (f'{_puppet_doc_base}/lang_facts_builtin_variables'
                                 '#lang_facts_builtin_variables')
_server_variables = f'{_lang_facts_builtin_variables}-server-variables'
_compiler_variables = f'{_lang_facts_builtin_variables}-compiler-variables'
_trusted_facts = f'{_lang_facts_builtin_variables}-trusted-facts'
_server_facts = f'{_lang_facts_builtin_variables}-server-facts'

_built_in_variables = {
    'facts': 'https://google.com',
    # clientcert, clientversion, puppetversion, clientnoop,
    # agent_specified_environment:
    # https://www.puppet.com/docs/puppet/7/lang_facts_builtin_variables#lang_facts_builtin_variables-agent-facts
    'trusted': _trusted_facts,
    'server_facts': _server_facts,
    'environment': _server_variables,
    'servername': _server_variables,
    'serverip': _server_variables,
    'serverversion': _server_variables,
    'module_name': _compiler_variables,
    'caller_module_name': _compiler_variables,

    # Also note the special variable $title and $name
    # https://www.puppet.com/docs/puppet/7/lang_defined_types#lang_defined_types-title-and-name
}


# https://www.puppet.com/docs/puppet/7/cheatsheet_core_types.html
# https://www.puppet.com/docs/puppet/7/types/file.html
# ...
_built_in_types = {
    'package',
    'file',
    'service',
    'notify',
    'exec',
    'user',
    'group',
}

# https://www.puppet.com/docs/puppet/7/function.html#{}
_built_in_functions = {
    'abs',
    'alert',
    'all',
    'annotate',
    'any',
    'assert_type',
    'binary_file',
    'break',
    'call',
    'camelcase',
    'capitalize',
    'ceiling',
    'chomp',
    'chop',
    'compare',
    'contain',
    'convert_to',
    'create_resources',
    'crit',
    'debug',
    'defined',
    'dig',
    'digest',
    'downcase',
    'each',
    'emerg',
    'empty',
    'epp',
    'err',
    'eyaml_lookup_key',
    'fail',
    'file',
    'filter',
    'find_file',
    'find_template',
    'flatten',
    'floor',
    'fqdn_rand',
    'generate',
    'get',
    'getvar',
    'group_by',
    'hiera',
    'hiera_array',
    'hiera_hash',
    'hiera_include',
    'hocon_data',
    'import',
    'include',
    'index',
    'info',
    'inline_epp',
    'inline_template',
    'join',
    'json_data',
    'keys',
    'length',
    'lest',
    'lookup',
    'lstrip',
    'map',
    'match',
    'max',
    'md5',
    'min',
    'module_directory',
    'new',
    'next',
    'notice',
    'partition',
    'realize',
    'reduce',
    'regsubst',
    'require',
    'return',
    'reverse_each',
    'round',
    'rstrip',
    'scanf',
    'sha1',
    'sha256',
    'shellquote',
    'size',
    'slice',
    'sort',
    'split',
    'sprintf',
    'step',
    'strftime',
    'strip',
    'tag',
    'tagged',
    'template',
    'then',
    'tree_each',
    'type',
    'unique',
    'unwrap',
    'upcase',
    'values',
    'versioncmp',
    'warning',
    'with',
    'yaml_data',
}


def _find_declarations(objs: list[MatchObject]) -> list[str]:
    """
    Find all local variable declarations.

    Searches the code for all local variable declarations, returing a
    list of variable names.

    Note that the same variable might appear multiple times, for example:

    .. code-block:: puppet
        :caption: The same variable being declared twice

        if $something {
            $x = 10
        } else {
            $x = 20
        }
    """
    declarations = []
    for obj in objs:
        match obj:
            case MatchCompound(type='declaration', matched=xs):
                for x in xs:
                    match x:
                        case MatchCompound(type='var', matched=ys):
                            declarations.append(inner_text(ys))
    return declarations


class _PuppetReserializer:
    """
    Reserializes parsed puppet code back into puppet code.

    This allows syntax highlighting, and hyperlinking to be added to the code.

    :param local_vars:
        Variables declared within this file. Used when resolving
        hyperlinks.
    """

    def __init__(self, local_vars: list[str]):
        self.local_vars: list[str] = local_vars

    def reserialize(self, obj: MatchObject | Sequence[MatchObject]) -> str:
        """
        Reconstruct puppet code after parsing it.

        After building the parser, and parsing the puppet code into a tree
        of MatchObjects; this procedure returns it into puppet code.
        Difference being that we now have metadata, meaning that syntax
        highlighting and variable hyperlinks can be inserted.

        :param obj:
            Should be assumed to be a list of MatchObject's, or something similar.

            MatchCompound objects are serialized as

            .. code-block:: html

                <span class="{type}">{body}</span>

            esrings as themselves, and lists have reserialize mapped over them.

        """
        out: list[str] = []
        # logger.info("obj = %a", obj)

        # TODO hyperlink functions.
        # The problem is that a function can either be implemented in
        # Puppet, or in Ruby. And Ruby functions' names aren't bound
        # by the directory layout.
        match obj:
            case str(s):
                out.append(html.escape(s))

            case MatchCompound(type='resource-name', matched=xs):
                name = inner_text(xs)
                url, cls = name_to_url(name)
                if url:
                    out.append(f'<a href="{url}" class="resource-name {cls}">{name}</a>')
                else:
                    # TODO this is class, but the class name should
                    # also be hyperlinked
                    out.append(f'<span class="resource-name {cls}">{name}</span>')

            case MatchCompound(type='invoke', matched=xs):
                function = None
                for x in xs:
                    match x:
                        case MatchCompound(type='qn', matched=ys):
                            if function is None:
                                function = inner_text(ys)
                                if function in _built_in_functions:
                                    # class="qn"
                                    url = f"https://www.puppet.com/docs/puppet/7/function.html#{function}"  # noqa: E501
                                    tag = f'<a href="{url}" class="puppet-doc">{self.reserialize(ys)}</a>'  # noqa: E501
                                    out.append(tag)
                                else:
                                    # TODO function to url
                                    out.append(f'<span class="qn">{self.reserialize(ys)}</span>')
                            else:
                                if function == 'include':
                                    url, cls = name_to_url(inner_text(ys))
                                    # class="qn"
                                    tag = f'<a href="{url}" class="{cls}">{self.reserialize(ys)}</a>'  # noqa: E501
                                    out.append(tag)
                                else:
                                    out.append(self.reserialize(ys))
                        case _:
                            out.append(self.reserialize(x))

            case MatchCompound(type='declaration', matched=xs):
                for x in xs:
                    match x:
                        case MatchCompound(type='var', matched=ys):
                            inner = ''.join(self.reserialize(y) for y in ys)
                            out.append(f'<span id="{inner_text(ys)}">{inner}</span>')
                        case _:
                            out.append(self.reserialize(x))

            case MatchCompound(type='var', matched=xs):
                out.append(self.var_to_url(inner_text(xs)))

            case MatchCompound(type=type, matched=xs):
                body = ''.join(self.reserialize(x) for x in xs)
                out.append(f'<span class="{type}">{body}</span>')

            case [*xs]:
                out.extend(self.reserialize(x) for x in xs)

            case rest:
                logger.error("Unknown type: %a", rest)

        return ''.join(out)

    def var_to_url(self, var: str) -> str:
        """
        Format variable, adding hyperlink to its definition.

        TODO these can refer to both defined types (`manifests/*.pp`),
        as well as resource types (`lib/puppet/provider/*/*.rb` /
        `lib/tpuppet/type/*.rb`)

        Same goes for functions (`functions/*.pp`),
        (`lib/puppet/functions.rb`).

        :param var:
            Name of the variable.

        :return:
            An HTML anchor element.
        """
        match var.split('::'):
            case [name]:
                # Either a local or global variable
                # https://www.puppet.com/docs/puppet/7/lang_facts_and_builtin_vars.html

                href = None
                cls = ''
                if name in self.local_vars:
                    href = f'#{html.escape(var)}'
                elif name in _built_in_variables:
                    href = html.escape(_built_in_variables[name])
                    cls = 'puppet-doc'

                if href:
                    return f'<a class="var {cls}" href="{href}">{var}</a>'
                else:
                    # `name` refers to a global fact.
                    return f'<span class="var">{var}</span>'

            case ['', name]:
                # A global variable
                if name in _built_in_variables:
                    href = html.escape(_built_in_variables[name])
                    img = '<img src="/code/muppet-strings/output/static/favicon.ico" />'
                    return f'<a class="var" href="{href}">{var}{img}</a>'
                else:
                    return f'<span class="var">{var}</span>'

            # Note the "special module" 'settings',
            # https://www.puppet.com/docs/puppet/7/lang_facts_builtin_variables#lang_facts_builtin_variables-server-variables
            case ['', module, *items, name]:
                s = '/code/muppet-strings/output/' \
                    + '/'.join([module, 'manifests', *(items if items else ['init'])])
                s += f'#{name}'
                return f'<a class="var" href="{s}">{var}</a>'
            case [module, *items, name]:
                s = '/code/muppet-strings/output/' \
                    + '/'.join([module, 'manifests', *(items if items else ['init'])])
                s += f'#{name}'
                return f'<a class="var" href="{s}">{var}</a>'
            case _:
                raise ValueError()


def hyperlink_puppet_source(source: str, file: str, in_parameters: list[str]) -> str:
    """
    Parse and syntax highlight the given puppet source.

    :returns: An HTML string
    """
    # Run the upstream puppet parser,
    # then masage the tree into a usable form.
    ast = build_ast(puppet_parser(source))

    # From the ast, build a parser combinator parser.
    # This parser will attach sufficient metadata to allow syntax
    # highlighting and hyperlinking
    parser = ParserFormatter().serialize(ast)

    # Run the generated parser, giving us a list of match objects.
    match_objects = ParserCombinator(source, file).get(parser)

    # Reserialize the matched data back into puppet code, realizing
    # the syntax highlighting and hyperlinks.
    return _PuppetReserializer(_find_declarations(match_objects) + (in_parameters)) \
        .reserialize(match_objects)


def name_to_url(name: str) -> tuple[str | None, str]:
    """
    Resolve a class or resource name into an url.

    :param name:
        The name of a class or resource, surch as "example::resource".
    :return:
        A tuple consisting of

        - One of
          - An internal link to the definition of that type
          - A link to the official puppet documentation
          - ``None``, if `name` is "class"
        - A string indicating extra HTML classes for this url.
          This is mostly so external references can be marked properly.
    """
    if name in _built_in_types:
        return (f'https://www.puppet.com/docs/puppet/7/types/{name}.html', 'puppet-doc')
    elif name == 'class':
        return (None, '')
    else:
        # TODO special cases for puppet's built in types.
        # https://www.puppet.com/docs/puppet/7/cheatsheet_core_types.html
        module, *items = name.lstrip(':').split('::')
        # TODO get prefix from the command line/config file
        return ('/code/muppet-strings/output/'
                + '/'.join([module, 'manifests', *(items if items else ['init'])]),
                '')
