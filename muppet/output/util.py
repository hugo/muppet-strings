"""
Misc utilities for the final output.

These don't really belong to any sub-system, even though some are more
useful than other.

The aim is to only have pure functions here.
"""

from typing import Any, Protocol
from muppet.parser_combinator import (
    MatchCompound,
    MatchObject,
)


def inner_text(obj: MatchObject | list[MatchObject]) -> str:
    """
    Extract the text content from a set of MatchObjects.

    This is really similar to HTML's inner_text.

    Empty whitespace tags are expanded into nothing, non-empty
    whitespace tags becomes a single space (note that this discards
    commets).

    This only works properly if no function was mapped over the parser
    return values in tree, see :func:`muppet.parser_combinator.fmap`.

    :param obj:
        Match Objects to search.
    """
    match obj:
        case str(s):
            return s
        case MatchCompound(type='ws', matched=[]):
            return ''
        case MatchCompound(type='ws'):
            return ' '
        case MatchCompound(matched=xs):
            return ''.join(inner_text(x) for x in xs)
        case [*xs]:
            return ''.join(inner_text(x) for x in xs)
        case _:
            raise ValueError('How did we get here')


class HtmlSerializable(Protocol):
    """Classes which can be serialized as HTML."""

    def to_html(self, **_: Any) -> str:  # pragma: no cover
        """Return HTML string."""
        ...

    def to_html_list(self, **_: Any) -> str:  # pragma: no cover
        """Return HTML suitable for a list."""
        ...
