"""
Generate output for Puppet Docstrings.

Docstrings are the functions preceeding any top level puppet
declaration (such as classes, rosource definitions, ...). These have a
number of "magic" tags for attaching metadata, along with usually
being Markdown formatted. This module assumes that they all are
Markdown formatted, which unfortunately leads to some (minor) errors.

(The final output also contains the original source, allowing these
errors to be overlooked).
"""

from dataclasses import dataclass, field
import html
import re
from typing import cast
from muppet.markdown import markdown
from muppet.puppet.strings import (
    DocString,
    DocStringApiTag,
    DocStringAuthorTag,
    DocStringExampleTag,
    DocStringOptionTag,
    DocStringOverloadTag,
    DocStringParamTag,
    DocStringRaiseTag,
    DocStringReturnTag,
    DocStringSeeTag,
    DocStringSinceTag,
    DocStringSummaryTag,
    DocStringTag,
)


@dataclass
class GroupedTags:
    """
    All tags from a class (or similar) docstring.

    Most fields are simply lists of tags. The reason for trailing
    underscores on each entry is since some tag names collide with
    python keywords (e.g. ``raise``).
    """

    param_:     list[DocStringParamTag]             = field(default_factory=list)  # noqa: E221
    example_:   list[DocStringExampleTag]           = field(default_factory=list)  # noqa: E221
    overload_:  list[DocStringOverloadTag]          = field(default_factory=list)  # noqa: E221
    option_:    dict[str, list[DocStringOptionTag]] = field(default_factory=dict)  # noqa: E221
    """
    Options document Hash parameters valid values.

    Each key is the corresponding parameter, and the value is the list
    of registered options for that hash.
    """

    author_:    list[DocStringAuthorTag]            = field(default_factory=list)  # noqa: E221
    api_:       list[DocStringApiTag]               = field(default_factory=list)  # noqa: E221
    raise_:     list[DocStringRaiseTag]             = field(default_factory=list)  # noqa: E221
    return_:    list[DocStringReturnTag]            = field(default_factory=list)  # noqa: E221
    since_:     list[DocStringSinceTag]             = field(default_factory=list)  # noqa: E221
    summary_:   list[DocStringSummaryTag]           = field(default_factory=list)  # noqa: E221
    see_:       list[DocStringSeeTag]               = field(default_factory=list)  # noqa: E221
    other_:     list[DocStringTag]                  = field(default_factory=list)  # noqa: E221
    """All tags of unknown type."""

    @classmethod
    def from_taglist(cls, tags: list[DocStringTag]) -> 'GroupedTags':
        """Group a list of tags."""
        grouped_tags = cls()
        for tag in tags:
            if tag.tag_name == 'option':
                tag = cast(DocStringOptionTag, tag)
                grouped_tags.option_.setdefault(tag.parent, []).append(tag)
            elif tag.tag_name in {'param', 'example', 'overload', 'author', 'api',
                                  'raise', 'return', 'since', 'summary', 'see'}:
                getattr(grouped_tags, tag.tag_name + '_').append(tag)
            else:
                grouped_tags.other_.append(tag)
        return grouped_tags


def parse_author(author: str) -> str:
    """
    Format author tags' content.

    :param author:
        The contents of the author tag. If the string is on the
        regular "author" format of ``"Firstname Lastname
        <first.last@example.com>"`` then the email will be formatted
        and hyperlinked. Otherwise the string is returned verbatim.
    :return:
        An HTML safe string, possibly including tags.
    """
    m = re.match(r'(?P<author>.*) (<(?P<email>.*)>)|(?P<any>.*)', author)
    assert m, "The above regex can't fail"
    if m['author'] and m['email']:
        author = html.escape(m['author'])
        email = html.escape(m['email'])
        return f'{author} <a class="email" href="mailto:{email}">&lt;{email}&gt</a>;'
    else:
        return html.escape(m['any'])


def format_docstring(name: str, docstring: DocString) -> str:
    """
    Format docstrings as they appear in some puppet types.

    Those types being:

    * puppet_classes,
    * puppet_type_aliases, and
    * defined_types
    """
    # The api tag is ignored, since it instead is shown from context

    out = ''

    grouped_tags = GroupedTags.from_taglist(docstring.tags)

    # --------------------------------------------------

    out += '<a href="#code">Jump to Code</a><br/>'

    if tags := grouped_tags.summary_:
        out += '<em class="summary">'
        for tag in tags:
            out += html.escape(tag.text)
        out += '</em>'

    out += '<div class="description">'
    # TODO "TODO" highlighting
    out += markdown(docstring.text)
    out += '</div>'

    # TODO proper handling of multiple @see tags
    if sees := grouped_tags.see_:
        out += '<b>See</b> '
        for see in sees:
            link: str
            m = re.match(r'((?P<url>https?://.*)|(?P<man>.*\([0-9]\))|(?P<other>.*))', see.name)
            assert m, "Regex always matched"
            if m['url']:
                link = f'<a href="{see.name}">{see.name}</a>'
                out += link
            elif m['man']:
                page = see.name[:-3]
                section = see.name[-2]
                # TODO man providers
                link = f"https://manned.org/man/{page}.{section}"
                out += link
            else:
                if '::' in m['other']:
                    # TODO
                    pass
                else:
                    # TODO
                    # link = see
                    pass
                out += m['other']
            out += ' ' + see.text

    if authors := grouped_tags.author_:
        out += '<div class="author">'
        out += "<em>Written by </em>"
        if len(authors) == 1:
            out += parse_author(authors[0].text)
        else:
            out += '<ul>'
            for author in authors:
                out += f'<li>{parse_author(author.text)}</li>'
            out += '</ul>'
        out += '</div>'

    out += '<hr/>'

    t: DocStringTag

    for t in grouped_tags .example_:
        out += '<div class="code-example">'

        if name := t.name:
            # TODO markup for title
            out += f'<div class="code-example-header">{html.escape(name)}</div>\n'
        # TODO highlight?
        # Problem is that we don't know what language the example
        # is in. Pygemntize however seems to do a reasonable job
        # treating anything as puppet code
        text = html.escape(t.text)
        out += f'<pre><code class="puppet">{text}</code></pre>\n'
        out += '</div>'

    out += '<hr/>'

    out += '<dl>'
    for t in grouped_tags.param_:
        name = html.escape(t.name)
        out += f'<dt><span id="{name}" class="variable">{name}</span>'
        match t.types:
            case [x]:
                # TODO highlight type?
                out += f': <code>{html.escape(x)}</code>'
            case [_, *_]:
                raise ValueError("How did you get multiple types onto a parameter?")

        # TODO Fetch default values from puppet strings output
        # Then in javascript query Hiera to get the true "default"
        # values for a given machine (somewhere have a setting for
        # selecting machine).
        out += '</dt>'

        if text := t.text:
            text = re.sub(r'(NOTE|TODO)',
                          r'<mark>\1</mark>',
                          markdown(text))

            if options := grouped_tags.option_.get(t.name):
                text += '<dl>'
                for option in options:
                    text += '<dt>'
                    text += html.escape(option.opt_name)
                    match option.opt_types:
                        case [x]:
                            text += f' [<code>{html.escape(x)}</code>]'
                        case [_, *_]:
                            raise ValueError("How did you get multiple types onto an option?")
                    text += '</dt>'
                    text += '<dd>'
                    if option.opt_text:
                        text += re.sub(r'(NOTE|TODO)',
                                       r'<mark>\1</mark>',
                                       markdown(option.opt_text))
                    text += '</dd>'
                text += '</dl>'

            out += f"<dd>{text}</dd>"
        else:
            out += '<dd><em>Undocumented</em></dd>'
    out += '</dl>'

    # TODO remaining tags
    # "overload"
    # raise
    # return
    # since
    # _other

    return out
