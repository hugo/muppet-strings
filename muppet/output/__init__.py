"""
Generate all output files.

The primary entry point of this module is the class
:class:`PuppetEnvironment`.
"""
import html
import logging
import os.path
import pathlib
import re
import json
from glob import glob
from typing import (
    Optional,
    Sequence,
    cast,
)

from muppet.syntax_highlight import highlight
from muppet.puppet.strings import (
    DataTypeAlias,
    DefinedType,
    DocString,
    DocStringParamTag,
    Function,
    PuppetClass,
    PuppetStrings,
    ResourceType,
    isprivate,
    puppet_strings_cached,
)
from muppet.parser_combinator import (
    ParseError,
)
from muppet.markdown import markdown
from muppet.breadcrumbs import Breadcrumbs, breadcrumbs
from muppet.util import group_by, partition
from muppet.cache import AbstractCache

from jinja2 import (
    Environment,
    FileSystemLoader,
)

from .docstring import format_docstring
from .puppet_source import hyperlink_puppet_source
from .toc import (
    Toc,
    # TocEntry,
    TocHeader,

    type_aliases_index,
    resource_type_index,
    index_item,

)


logger = logging.getLogger(__name__)


class Templates:
    """
    Namespace for templates.

    Almost all of these methods take these values:

    :path breadcrumbs:
        Breadcrumb to current page.
    """

    def __init__(self, path_base: str) -> None:
        """
        Construct a new instance.

        :param path_base:
            Prefix added to all links within output
        """
        self.jinja = Environment(
            loader=FileSystemLoader('templates'),
            autoescape=False)
        self.path_base = path_base

    def code_page(self, *,
                  title: str,
                  content: str,
                  breadcrumbs: Optional[Breadcrumbs] = None,
                  left_sidebar: Optional[str] = None,
                  right_sidebar: Optional[str] = None,
                  ) -> str:  # pragma: no cover
        """
        Template for a page containing puppet code.

        :param content:
            Top level h1 tag of page.

        :param content:
            Free form string content, being the main body of the page.
        """
        template = self.jinja.get_template('code_page.html')
        return template.render(
                title=title,
                content=content,
                path_base=self.path_base,
                left_sidebar=left_sidebar,
                right_sidebar=right_sidebar,
                breadcrumbs=breadcrumbs)

    def content(self, *,
                content: str,
                breadcrumbs: Optional[Breadcrumbs] = None
                ) -> str:  # pragma: no cover
        """
        Template for a page with arbitrary content.

        :param content:
            Arbitrary content
        """
        template = self.jinja.get_template('content.html')
        return template.render(
                content=content,
                path_base=self.path_base,
                breadcrumbs=breadcrumbs)

    def index(self, *,
              modules: list['PuppetModule'],
              breadcrumbs: Optional[Breadcrumbs] = None
              ) -> str:  # pragma: no cover
        """
        Environment index file.

        :param modules:
            List of PuppetModules to include in the environment index.
        """
        template = self.jinja.get_template('index.html')
        return template.render(
                path_base=self.path_base,
                modules=modules,
                breadcrumbs=breadcrumbs)

    def module_index(self, *,
                     # content: list[],  # something with to_html_list and to_html
                     content: Sequence[Toc],
                     module_author: Optional[str],
                     module_name: str,
                     doc_files: list[tuple[str, str]],
                     breadcrumbs: Optional[Breadcrumbs] = None,
                     left_sidebar: Optional[str] = None,
                     right_sidebar: Optional[str] = None,
                     ) -> str:  # pragma: no cover
        """
        Index for a single module.

        :param content:
            Main content of the page.
        :param module_author:
            Author of the module, as it appears in the metadata.json file.
        :param module_name:
            Name of the module, without the author component.
        :param doc_files:
            The free-form documentation files bundled with the module.

            Each element should be a pair of
            - The idealized name of the file
            - The relative path to the document inside the output
              (the HTML generated version, e.g. "{module}/{filename}/")

        :param left_sidebar:
            Free form content of the left sidebar.

            This is assumed to be a list of modules in the environment.
        :param right_sidebar:
            Free form content of the right sidebar.

            This is assumed to be a table of contents of the module,
            really similar to the actual body contents.
        """
        template = self.jinja.get_template('module_index.html')
        return template.render(
                content=content,
                module_author=module_author,
                module_name=module_name,
                doc_files=doc_files,
                path_base=self.path_base,
                breadcrumbs=breadcrumbs,
                left_sidebar=left_sidebar,
                right_sidebar=right_sidebar)


class PuppetModule:
    """
    A representation of an entire Puppet module.

    :param name:
        Name the module, without the author information.
        This is also used as the published named.
    :param path:
    :param strings_output:
    :param metadata:
    :param doc_files:
    """

    # name: str
    # path: str
    # strings_output: PuppetStrings
    # metadata: dict[str, Any]
    # doc_files: list[str]

    def __init__(self, *,
                 path: str,
                 output_prefix: str,
                 puppet_strings: PuppetStrings,
                 templates: Templates):
        """
        Construct a new instance.

        :param path:
            Path to the source of this module.

        :param output_prefix:
            Which web path the pages should be outputed as.

        :param puppet_strings:
            (Re-formatted) output of ``puppet strings``. Taken as a
            paärameter to enable caching outside.
        """
        self.path = path
        self.name = os.path.basename(path)
        self.strings_output = puppet_strings
        self.toc = self._build_module_toc()
        self.output_prefix = output_prefix
        self.templates = templates

        abspath = os.path.abspath(self.path)
        self.doc_files: list[str] = \
            glob(os.path.join(abspath, '*.md')) + \
            glob(os.path.join(abspath, 'LICENSE'))

        try:
            with open(os.path.join(self.path, 'metadata.json')) as f:
                self.metadata = json.load(f)
        except FileNotFoundError:
            self.metadata = {}

        self.module_toc: str = ''.join([
            '<ul class="toc">',
            *(e.to_html_list(path_base=self.output_prefix) for e in self._build_module_toc()),
            '</ul>'])

    def _build_module_toc(self) -> Sequence[Toc]:
        """
        Build the TOC of the module.

        This method looks into the strings output of the current
        object, and creates a TOC tree which more or less has the
        strings output keys as top level entries, and all
        resources/classes/... of that type as child entries.

        :returns:
            A list of categories.
        """
        content: list[Toc] = []

        if puppet_classes := self.strings_output.puppet_classes:
            content.append(class_index(self.name, puppet_classes))

        # data_types
        if _ := self.strings_output.data_types:
            content.append(TocHeader(
                name='Data types not yet implmented'))

        if data_type_aliases := self.strings_output.data_type_aliases:
            content.append(type_aliases_index(self.name, data_type_aliases))

        if defined_types := self.strings_output.defined_types:
            content.append(defined_types_index(self.name, defined_types))

        if resource_types := self.strings_output.resource_types:
            content.append(TocHeader(
                name='Resource Types',
                children=resource_type_index(
                    resource_types,
                    self.name)))

        # providers
        if _ := self.strings_output.providers:
            content.append(TocHeader(
                name='Providers not yet implmented'))

        # puppet_functions
        if _ := self.strings_output.puppet_functions:
            content.append(TocHeader(
                name='Puppet Functions not yet implmented'))

        # templates/
        # files/
        # examples or tests/
        # (spec)/
        # lib/puppet_x/
        # lib/facter/
        # facts.d/
        # data/
        # hiera.yaml

        # puppet_tasks
        if _ := self.strings_output.puppet_tasks:
            content.append(TocHeader(
                name='Puppet Tasks not yet implmented'))

        # puppet_plans
        if _ := self.strings_output.puppet_plans:
            content.append(TocHeader(
                name='Puppet Plans not yet implmented'))

        return content

    def file(self, path: str) -> str:
        """Return the absolute path of a path inside the module."""
        return os.path.join(self.path, path)

    def index_page(self, destination: str) -> None:
        """
        Generate the index file for a specific module.

        :param destination:
            Path which this module should be created in, such as
            '/var/www/muppet/{env}/{module_name}.
        """
        crumbs = breadcrumbs(
                ('Environment', ''),
                self.name,
                )

        # TODO left sidebar should contain list of modules in
        # environment.

        toc = self._build_module_toc()

        doc_files_toc = [(os.path.splitext(k)[0],
                          os.path.join(self.name, os.path.join(os.path.splitext(k)[0])))
                         for (k, v) in read_doc_files(self.doc_files).items()]

        with open(os.path.join(destination, 'index.html'), 'w') as f:
            f.write(self.templates.module_index(
                module_name=self.name,
                module_author=self.metadata.get('author'),
                breadcrumbs=crumbs,
                content=toc,
                doc_files=doc_files_toc,
                # left_sidebar=(),
                right_sidebar=self.module_toc))

    def _generate_classes(self, destination: str) -> None:
        for puppet_class in self.strings_output.puppet_classes \
                + self.strings_output.defined_types:
            logger.info('Formamting %s', puppet_class.name)
            # localpath = puppet_class['name'].split('::')
            localpath, _ = os.path.splitext(puppet_class.file)
            dir = os.path.join(destination, localpath)
            pathlib.Path(dir).mkdir(parents=True, exist_ok=True)
            # puppet_class['docstring']
            # puppet_class['defaults']

            # TODO option to add .txt extension (for web serverse which
            # treat .pp as application/binary)
            with open(os.path.join(dir, 'source.pp.txt'), 'wb') as f:
                with open(self.file(puppet_class.file), 'rb') as g:
                    f.write(g.read())

            crumbs = breadcrumbs(
                    ('Environment', ''),
                    self.name,
                    (puppet_class.name,
                     'manifests/' + '/'.join(puppet_class.name.split('::')[1:])),
                    'This',
                    )

            with open(os.path.join(dir, 'source.pp.html'), 'w') as f:
                with open(self.file(puppet_class.file), 'r') as g:
                    f.write(self.templates.code_page(
                        # TODO handle title?
                        title='',
                        left_sidebar=self.module_toc,
                        content=highlight(g.read(), 'puppet'),
                        breadcrumbs=crumbs))

            crumbs = breadcrumbs(
                    ('Environment', ''),
                    self.name,
                    (puppet_class.name,
                     'manifests/' + '/'.join(puppet_class.name.split('::')[1:])),
                    )

            title, body = format_class(puppet_class)
            with open(os.path.join(dir, 'index.html'), 'w') as f:
                f.write(self.templates.code_page(
                    title=title,
                    content=body,
                    left_sidebar=self.module_toc,
                    breadcrumbs=crumbs))

            # puppet_class['file']
            # puppet_class['line']

    def _generate_type_aliases(self, destination: str) -> None:
        for type_alias in self.strings_output.data_type_aliases:
            logger.debug('Formamting %s', type_alias.name)

            localpath, _ = os.path.splitext(type_alias.file)
            dir = os.path.join(destination, localpath)
            pathlib.Path(dir).mkdir(parents=True, exist_ok=True)

            with open(os.path.join(dir, 'source.pp.txt'), 'w') as f:
                f.write(type_alias.alias_of)

            title, body = format_type_alias(type_alias, type_alias.name)
            with open(os.path.join(dir, 'index.html'), 'w') as f:
                f.write(self.templates.code_page(
                    title=title,
                    content=body))

    def _output_doc_files(self, destination: str, files: dict[str, str]) -> None:
        """
        Generate and output each documentation file.

        :param destination:
            Directory where the files should end up.

            This should be the root output directory of the module, e.g.

                /var/www/muppet/{environment}/{module}/

        :param files:
            A set of files as returned by :func:`read_doc_files`.

            Each key should be the basename of the file, each value should
            be an absolute path to an (existing) file on disk.
        """
        doc_files: dict[str, str] = {}
        for filename, filepath in files.items():
            # logger.debug('Formamting %s', filename)

            name, _ = os.path.splitext(filename)
            with open(filepath) as f:
                raw_content = f.read()

            pathlib.Path(os.path.join(destination, name)).mkdir(exist_ok=True)
            out_path = os.path.join(destination, name, 'index.html')

            if filename.endswith('.md'):
                content = markdown(raw_content)
            else:
                content = '<pre>' + html.escape(raw_content) + '</pre>'

            crumbs = breadcrumbs(('Environment', ''),
                                 self.name,
                                 name)

            with open(out_path, 'w') as f:
                f.write(self.templates.content(
                    content=content,
                    breadcrumbs=crumbs))

            doc_files[name] = os.path.join(self.name, name, 'index.html')

    def output(self, destination: str) -> None:
        """
        Generate output for module.

        :param dest:
            Where the content should end up. This should NOT include
            the module name.

            example: '/var/www/muppet/{environment}/'
        """
        destination = os.path.join(destination, self.name)
        pathlib.Path(destination).mkdir(exist_ok=True)
        self.index_page(destination)

        self._output_doc_files(destination, read_doc_files(self.doc_files))
        self._generate_classes(destination)
        self._generate_type_aliases(destination)
        # data_type_aliases
        # defined_types
        # resource_types


class PuppetEnvironment:
    """
    Representation of a complete puppet environment.

    This module holds all information about a parser environment,
    along with procedures for generating documentation output from it.

    See `The official docs <https://www.puppet.com/docs/puppet/7/environments_creating.html>`_

    Environments are usually found in
    ``/etc/puppetlabs/code/environments/{name}``, where each module
    may contain any of the following files:

    - modules/
    - manifests/
    - hiera.yaml
    - environment.conf

    Along with any other files.

    :param modules:
        List of all modules in the environment. This will be a list of
        :class:`PuppetModule` objects.

    :param name:
        Name of the environment. 'production' is a common choice.

    :param source_path:
        Absolute path to the environments source code.

    :param cache:
        A cache object.

    :param output_prefix:
        Prefixes which will be added to all links in output.
    """

    def __init__(self, source_path: str, output_prefix: str, cache: AbstractCache):
        """
        Construct a new instance.

        :param source_path:
            Absolute source directory where the environment exists,

            .. code-block:: python

                >>> f'/etc/puppetlabs/code/environments/{self.name}'

        :param output_prefix:
            Prefixes which will be added to all links in output.

        :param cache:
            Cache instance.
        """
        # TODO an alternative constructor could take environment name,
        # and then expands with the default path.
        self.source_path = source_path
        self.name = os.path.basename(source_path)
        self.output_prefix = output_prefix

        self.templates = Templates(output_prefix)

        self.modules: list[PuppetModule] = []
        dir = os.path.join(self.source_path, 'modules')

        for entry in sorted(list(os.scandir(dir)),
                            key=lambda d: d.name):
            module_path = entry.path
            # entry_ = entry.name
            # module_path = os.path.join(dir, entry_)
            # print(f"entry = {entry_}, module_path = {module_path}, dir={dir}")
            self.modules.append(
                    PuppetModule(path=module_path,
                                 output_prefix=output_prefix,
                                 puppet_strings=puppet_strings_cached(module_path,
                                                                      cache),
                                 templates=self.templates))

    def output(self, destination: str) -> None:
        """
        Generate all output files.

        :param destination:
            /var/www/muppet/

            self.name will be appended to it
        """
        destination = os.path.join(destination, self.name)
        pathlib.Path(destination).mkdir(exist_ok=True)

        with open(os.path.join(destination, 'index.html'), 'w') as f:
            # TODO breadcrumbs
            f.write(self.templates.index(modules=self.modules))

        for module in self.modules:
            module.output(destination)


def class_index(module_name: str, class_list: list[PuppetClass]) -> Toc:
    """Prepage class index list."""
    publics, privates = partition(isprivate, class_list)

    lst: list[TocHeader] = []

    if publics:
        lst.append(TocHeader(
            name='Public Classes',
            children=[index_item(module_name, i) for i in publics]))

    if privates:
        lst.append(TocHeader(
            name='Private Classes',
            children=[index_item(module_name, i) for i in privates]))

    return TocHeader(
        name='Classes',
        children=lst
    )


def defined_types_index(module_name: str, defined_list: list[DefinedType]) -> TocHeader:
    """
    Prepare defined types index list.

    These are puppet types introduces by puppet code.
    Each only has one implemenattion.
    """
    groups = group_by(isprivate, defined_list)

    lst: list[TocHeader] = []

    if publics := groups.get(False):
        lst.append(TocHeader(
            name='Public Defined Types',
            children=[index_item(module_name, i) for i in publics],
        ))

    if privates := groups.get(True):
        lst.append(TocHeader(
            name='Private Defined Types',
            children=[index_item(module_name, i) for i in privates],
        ))

    return TocHeader(
        name='Defined Types',
        children=lst
    )


def format_class(d_type: DefinedType | PuppetClass) -> tuple[str, str]:
    """Format Puppet class."""
    out = ''
    logger.info("Formatting class %s", d_type.name)
    # print(name, file=sys.stderr)
    body = format_docstring(d_type.name, d_type.docstring)
    out += body

    # ------ Old ---------------------------------------
    # t = hyperlink_puppet_source(d_type.source)
    # data = parse(t, 0, ['root'])
    # renderer = HTMLRenderer(build_param_dict(d_type.docstring))
    # out += render(renderer, data)
    # ------ New ---------------------------------------
    out += '<hr/>'
    out += '<a id="code"></a>'

    in_parameters: list[str] = []
    for tag in d_type.docstring.tags:
        if tag.tag_name == 'param':
            in_parameters.append(cast(DocStringParamTag, tag).name)

    try:
        # Calculation beforehand, for "atomic" formatting
        result = hyperlink_puppet_source(d_type.source, d_type.name, in_parameters)
        out += '<pre class="highlight-muppet"><code class="puppet">'
        out += result
        out += '</code></pre>'
    except ParseError as e:
        logger.error("Parsing %(name)s failed: %(err)s",
                     {'name': d_type.name, 'err': e})
        out += f'<div class="error">{html.escape(str(e))}</div>'
        out += '<pre><code class="puppet">'
        if e.pos:
            out += d_type.source[:e.pos]
            out += '<span class="error">'
            out += d_type.source[e.pos]
            out += '</span>'
            out += d_type.source[e.pos+1:]
        else:
            out += d_type.source
        out += '</code></pre>'
    return d_type.name, out


def build_param_dict(docstring: DocString) -> dict[str, str]:
    """
    Extract all parameter documentation from a docstring dict.

    :param docstring:
        The object present under 'docstring' in the information about
        a single object (class, resource, ...) in the output of
        `puppet strings`.

    :returns:
        A dictionary where the keys are the variables which have
        documentation, and the value is the (formatted) documentation
        for that key. Undocumented keys (even those with the tag, but
        no text) are ommitted from the resulting dictionary.
    """
    obj = {}
    for t in docstring.tags:
        if isinstance(t, DocStringParamTag):
            obj[t.name] = re.sub(r'(NOTE|TODO)',
                                 r'<mark>\1</mark>',
                                 markdown(t.text))
    return obj


def format_type() -> str:
    """Format Puppet type."""
    return 'TODO format_type not implemented'


def format_type_alias(d_type: DataTypeAlias, file: str) -> tuple[str, str]:
    """Format Puppet type alias."""
    out = ''
    name = d_type.name
    logger.info("Formatting type alias %s", name)
    # print(name, file=sys.stderr)
    body = format_docstring(name, d_type.docstring)
    out += body
    out += '\n'
    out += '<pre class="highlight-muppet"><code class="puppet">'
    try:
        out += hyperlink_puppet_source(d_type.alias_of, file, [])
    except ParseError as e:
        logger.error("Parsing %(name)s failed: %(err)s",
                     {'name': d_type.alias_of, 'err': e})
    out += '</code></pre>\n'
    return d_type.name, out


def format_defined_type(d_type: DefinedType, file: str) -> tuple[str, str]:
    """Format Puppet defined type."""
    # renderer = HTMLRenderer(build_param_dict(d_type.docstring))
    out = ''
    name = d_type.name
    logger.info("Formatting defined type %s", name)
    # print(name, file=sys.stderr)
    body = format_docstring(name, d_type.docstring)
    out += body

    out += '<pre class="highlight-muppet"><code class="puppet">'
    try:
        out += hyperlink_puppet_source(d_type.source, file, [])
    except ParseError as e:
        logger.error("Parsing %(name)s failed: %(err)s",
                     {'name': d_type.source, 'err': e})
    out += '</code></pre>\n'
    return d_type.name, out


def format_resource_type(r_type: ResourceType) -> str:
    """Format Puppet resource type."""
    name = r_type.name
    logger.info("Formatting resource type %s", name)
    out = ''
    out += f'<h2>{name}</h2>\n'
    out += str(r_type.docstring)

    out += '<h3>Properties</h3>\n'
    if props := r_type.properties:
        out += '<ul>\n'
        for property in props:
            out += f'<li>{property.name}</li>\n'
            # description, values, default
        out += '</ul>\n'
    else:
        out += '<em>No providers</em>'

    out += '<h3>Parameters</h3>\n'
    out += '<ul>\n'
    for parameter in r_type.parameters:
        out += f'<li>{parameter.name}</li>\n'
        # description
        # Optional[isnamevar]
    out += '</ul>\n'

    out += '<h3>Providers</h3>\n'
    if providers := r_type.providers:
        for provider in providers:
            out += f'<h4>{provider.name}</h4>\n'
            # TODO
    else:
        print('<em>No providers</em>')

    return out


def format_puppet_function(function: Function, file: str) -> str:
    """Format Puppet function."""
    out = ''
    name = function.name
    logger.info("Formatting puppet function %s", name)
    out += f'<h2>{name}</h2>\n'
    t = function.type
    # docstring = function.docstring
    for signature in function.signatures:
        signature.signature
        signature.docstring
    if t in ['ruby3x', 'ruby4x']:
        # TODO syntax highlighting
        s = '<pre class="highlight-muppet"><code class="ruby">'
        s += function.source
        s += '</code></pre>\n'
        out += s
    elif t == 'puppet':
        out += '<pre class="highlight-muppet"><code class="puppet">'
        try:
            out += hyperlink_puppet_source(function.source, file, [])
        except ParseError as e:
            logger.error("Parsing %(name)s failed: %(err)s",
                         {'name': function.source, 'err': e})

        out += '</code></pre>\n'
    else:
        # TODO do something
        pass

    return out


def format_puppet_task() -> str:
    """Format Puppet task."""
    return 'TODO format_puppet_task not implemented'


def format_puppet_plan() -> str:
    """Format Puppet plan."""
    return 'TODO format_puppet_plan not implemented'


def read_doc_files(files: list[str]) -> dict[str, str]:
    """
    Locate the source path of all given files.

    :param files:
        A list of absolute filenames, locating the documentation files
        for a given directory.

        Files named 'REFERENCE.md' might get skipped, if they are the
        output from ``puppet strings``.

    :return:
        A dictionary mapping the base filename of each item in
        ``files`` onto it's absolute path.
    """
    GENERATED_MESSAGE = '<!-- DO NOT EDIT: This document was generated by Puppet Strings -->\n'
    """
    REFERENCE.md files generated by Puppet Strings include this string on
    their third line. We use this to ignore auto-generated files, since we
    replace that output.
    """

    _files: dict[str, str] = {}
    # TODO is this set?
    for file in files:
        # logger.debug('Formamting %s', file)

        basename = os.path.basename(file)
        if basename == 'REFERENCE.md':
            with open(file) as f:
                f.readline()
                f.readline()
                line3 = f.readline()
                if line3 == GENERATED_MESSAGE:
                    continue
        _files[basename] = file

    return _files
