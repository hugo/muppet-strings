Gathering information
=====================

For each module (``get_module``)

run ``puppet-strings`` (todo hyperlink) on the module, using the hash
of ``metadata.json`` as the cache key


Manual documentation files
--------------------------

``*.md`` + ``LICENSE``


Parsing Puppet Code
-------------------

``puppet strings --format json`` is run, generating a json
representation of the Puppet code. However, this format is overly
verbose. So it's passed to ``muppet.puppet.parser.puppet_parser``
(TODO link) which transforms it into a much simpler json tree.

This is then passed to ``muppet.puppet.ast.build_ast`` (TODO link)
which creates an AST in python objects.

These modes can be tested with

.. code-block:: sh

    python -m muppet.puppet {parser,ast,serialize}
