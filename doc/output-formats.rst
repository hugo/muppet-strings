Output Formats
==============

TODO something here about how output can be generated (see
muppet.puppet.format)


Output is handled module per module.

- index.html
- *REFERENCE*/index.html
- ...
- manifests/
  - *classname*
    - index.html
    - source.pp.html
    - source.pp.txt
  - ...

For each *class* (or *resource*, *type-alias*, ...) a directory is
generated. The index.html contains the documentation for the class,
followed by the source code, but syntax highlighted and hyperlinked.

TODO link to parser combinator usage here.

TODO source.pp.html isn't needed since "rendered" now contains the
original source code.

source.pp.txt contains the unaltered source, as it appeared on disk.
