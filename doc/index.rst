.. Muppet Strings documentation master file, created by
   sphinx-quickstart on Sat Jun  3 20:13:35 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Muppet Strings's documentation!
==========================================

Muppet Strings is a tool for documenting entire Puppet environments.

This tool makes heavy use of ``puppet strings``. `Puppet Strings
Manual <https://www.puppet.com/docs/puppet/7/puppet_strings_style>`_
is an expected pre-requisite to understand why this program works as
it does.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   doc-gather
   output-formats
   syntax-highlighting
   parser-combinator
   muppet

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

TODO
====
.. todolist::
