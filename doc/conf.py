# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
# from __future__ import annotations

import os
import sys

sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Muppet Strings'
copyright = '2023, Hugo Hörnquist'
author = 'Hugo Hörnquist'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# Coverage not included, since flake8 already captures that

extensions = [
    'sphinx.ext.autodoc',   # Automatically extract source from pypthon files
    'sphinx.ext.viewcode',  # Adds source viewer to output
    'sphinx.ext.todo',      # Add todo-lists to output
]

# Actually enable the todo list
todo_include_todos = True

# Fancy type aliases.
# For this to work, each module has to individually run
#   from __future__ import annotations
# Which will prevent type aliases from being eagerly evaluated.

autodoc_type_aliases = {
    'Markup': 'muppet.data.Markup',
}

# Add type signatures in parameter list of description, instead of in
# signature. This reduces the clutter quite a bit.
autodoc_typehints = 'description'

templates_path = ['_templates']
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'pyramid'
html_static_path = ['_static']
