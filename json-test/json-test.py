"""Unit tests for JSON parser written in my parser combinator."""


import json
from json2 import (
    json_value,
    json_string,
    _json_keyword,
    json_number,
)

from muppet.parser_combinator import ParserCombinator


def test_string():  # noqa: D103
    assert ParserCombinator('"Hello"').get(json_string) == "Hello"


def test_number_int():  # noqa: D103
    assert ParserCombinator("1").get(json_number) == [1]


def test_number_decimal():  # noqa: D103
    assert ParserCombinator("1.1").get(json_number) == [1.1]


def test_number_exp():  # noqa: D103
    assert ParserCombinator("1e-10").get(json_number) == [1e-10]


def test_number_full():  # noqa: D103
    assert ParserCombinator("-1.1e10").get(json_number) == [-1.1e10]


def test_keyword():  # noqa: D103
    assert ParserCombinator("true").get(_json_keyword) == [True]
    assert ParserCombinator("false").get(_json_keyword) == [False]
    assert ParserCombinator("null").get(_json_keyword) == [None]


tests = {
    'integer': 10,
    'negative': -10,
    'decimal': 1.2,
    'float': 1e+20,
    'string': "Hello, World",
    'object': {'a': 10, 'b': 20},
    'array': [1, 2, 3, 4],
    'nested': [{}, {'a': 10}],
    'keyword': True,
}


def test_good():  # noqa: D103
    for key, value in tests.items():
        serialized = json.dumps(value)
        deserialized = ParserCombinator(serialized).get(json_value)
        assert repr(deserialized[0])


def test_pre_serialized():  # noqa: D103
    pre_serialized = {
        'escaped': r'"Hello \u0041 World"',
    }

    for key, value in pre_serialized.items():
        print(key)
        deserialized = ParserCombinator(value).get(json_string)
        assert deserialized[0]
