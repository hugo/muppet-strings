.PHONY: all output check test sphinx-apidoc documentation \
	check_style check_type check_pyright

all: output

DOC_OUTPUT = doc.rendered

OUTPUT_FLAGS = --path-base /code/muppet-strings/output \
			   --env ~/puppet/generated-environments/production/

SCSS = scss

output: static-src/style.css
	python -m muppet $(OUTPUT_FLAGS)

check_style:
	flake8 muppet

check_type:
	mypy -p muppet

check_pyright:
	pyright $(shell find muppet -type f -name '*.py')

check: check_style check_type check_pyright

# TODO only require coverage if `python -m pytest -VV | grep pytest-cov` return true
PYTEST_FLAGS = --cov=muppet --cov-branch --cov-report=html

test:
	python -m pytest $(PYTEST_FLAGS) $(if $(VERBOSE),--verbose) tests

sphinx-apidoc:
	sphinx-apidoc --separate --force -o doc muppet

$(DOC_OUTPUT)/index.html: sphinx-apidoc
	sphinx-build -b dirhtml doc $(DOC_OUTPUT)

documentation: $(DOC_OUTPUT)/index.html

clean:
	-rm -r output

static-src/style.css:
	$(MAKE) -C $(dir $@) $(notdir $@)

tags: $(shell find muppet -type f -name '*.py')
	ctags $^
